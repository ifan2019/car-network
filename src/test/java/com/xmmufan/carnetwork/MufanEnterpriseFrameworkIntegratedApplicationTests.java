package com.xmmufan.carnetwork;

import com.xmmufan.carnetwork.constant.hardware.alarm.Alarm;
import com.xmmufan.carnetwork.entity.monitor.CarInfo;
import com.xmmufan.carnetwork.entity.monitor.ImgUrl;
import com.xmmufan.carnetwork.entity.monitor.UserC;
import com.xmmufan.carnetwork.repository.jpa.CarInfoRepository;
import com.xmmufan.carnetwork.service.CarInfoService;
import com.xmmufan.carnetwork.util.FileUploadUtil;
import com.xmmufan.carnetwork.util.IPFSUtil;
import com.xmmufan.carnetwork.util.RedisUtil;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.activemq.command.ActiveMQTopic;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import javax.jms.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MufanEnterpriseFrameworkIntegratedApplicationTests {
//    private Topic topic = new ActiveMQTopic("testTopic");
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    private MongoTemplate mongoTemplate;
    @Test
    public void contextLoads() throws JSONException, JMSException {

        jmsTemplate.convertAndSend("1111_query","hello world");
        System.out.println("success");
//        jmsTemplate.setDefaultDestinationName("1111_query");
//        jmsTemplate.setReceiveTimeout(1000);
//        Message msg = jmsTemplate.sendAndReceive(new MessageCreator() {
//            @Override
//            public Message createMessage(Session session) throws JMSException {
//                TextMessage txtMsg = session.createTextMessage("msg");
//                return txtMsg;
//            }
//        });
//        System.out.println(msg);
//        long l = System.currentTimeMillis();
//        //1575545182894
//        System.out.println("l="+l);
//        Alarm alarm = new Alarm();
//        alarm.setTime(time);
//        alarm.setDescription("askjdhaskjdhaskjd");
//        Alarm save = mongoTemplate.save(alarm);
//        String str = "5de38e43";
//        long l = Long.parseLong(str,16) * 1000;
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(new Date(l));
////        Alarm a = new Alarm();
////        a.setEquipmentName("combi");
////        a.setTime(calendar.getTime());
////        a.setDescription("水箱过低");
////        a.setSort("0");
////        a.setType("1");
////        mongoTemplate.save(a);
//        System.out.println(calendar.getTime());
//        Criteria criteria = new Criteria();
////        calendar.add(Calendar.HOUR,-8);
//        System.out.println(calendar.getTime());
//        criteria.and("time").is(calendar.getTime()).and("equipmentName").is("combi");
//        Query query = new Query(criteria);
//        List<Alarm> alarms = mongoTemplate.find(query, Alarm.class);
//
//        System.out.println(alarms.size());
//
//        System.out.println(mongoTemplate.findAll(Alarm.class));
    }
}
