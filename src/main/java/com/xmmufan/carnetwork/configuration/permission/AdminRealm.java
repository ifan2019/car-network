package com.xmmufan.carnetwork.configuration.permission;

import com.xmmufan.carnetwork.constant.permission.AccountState;
import com.xmmufan.carnetwork.entity.monitor.Admin;
import com.xmmufan.carnetwork.repository.jpa.AdminRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
public class AdminRealm extends AuthorizingRealm {


    @Autowired
    private AdminRepository adminRepository;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("doGetAuthorizationInfo");
        if (principalCollection == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }
        String username = (String) principalCollection.getPrimaryPrincipal();
        Set<String> roles = new HashSet<>();
        Set<String> permissions = new HashSet<>();
        try {
            System.out.println(username);
            List<Admin> adminList = adminRepository.findByUserName(username);
            System.out.println(adminList.size());
            Admin targetUser = adminList.isEmpty()?null:adminList.get(0);
            assert targetUser != null;
            targetUser.getRoleList().forEach(role -> {
                roles.add(role.getRole());
                role.getPermissionList().forEach(sysPermission -> permissions.add(sysPermission.getPermission()));
            });
        } catch (NullPointerException e) {
            final String message = "No account found for user '" + username +"'";
            throw new UnknownAccountException(message, e);
        }
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roles);
        info.setStringPermissions(permissions);
        System.out.println(Arrays.toString(permissions.toArray()));
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        String username = token.getUsername();
        if(StringUtils.isEmpty(username)){
            throw new AccountException("null name can not be login");
        }
        List<Admin> admins = adminRepository.findByUserName(username);
        Admin admin = admins.isEmpty()?null:admins.get(0);
        String password = "";
        String salt = "";
        if(admin!=null){
            //判断用户是否锁定
            if (admin.getState() == AccountState.LOCKED){
                throw new LockedAccountException("user: "+username+" this account has been locked. ");
            }
            password = admin.getPassword();
            salt = admin.getSalt();
        }
        return new SimpleAuthenticationInfo(username, password, ByteSource.Util.bytes(salt), this.getName());
    }

    @Override
    public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("MD5");
        hashedCredentialsMatcher.setHashIterations(8);
        super.setCredentialsMatcher(hashedCredentialsMatcher);
    }
}
