package com.xmmufan.carnetwork.configuration.permission;


import com.xmmufan.carnetwork.constant.permission.AccountState;
import com.xmmufan.carnetwork.entity.monitor.UserB;
import com.xmmufan.carnetwork.service.UserBService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.StringUtils;

@Slf4j
public class UserBRealm extends AuthorizingRealm {

    @Autowired
    @Lazy
    private UserBService userBService;


    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        CustomizedToken token = (CustomizedToken) authenticationToken;
        String username = token.getUsername();
        if(StringUtils.isEmpty(username)){
            throw new AccountException("null name can not be login");
        }
        UserB user = userBService.findByUsername(username);
        log.info("user=" + user.getUserName());
        //判断用户是否锁定
        if (user.getState() == AccountState.LOCKED){
            throw new LockedAccountException("userB: "+username+" this account has been locked. ");
        }


        String password = user.getPassword();
        String salt = user.getSalt();
        return new SimpleAuthenticationInfo(username, password, ByteSource.Util.bytes(salt), this.getName());
    }

    @Override
    public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("MD5");
        hashedCredentialsMatcher.setHashIterations(8);
        super.setCredentialsMatcher(hashedCredentialsMatcher);
    }
}
