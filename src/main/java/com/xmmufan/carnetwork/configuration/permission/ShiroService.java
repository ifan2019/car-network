package com.xmmufan.carnetwork.configuration.permission;

import com.xmmufan.carnetwork.service.SysPermissionService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Mr.ifan| 詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 * 清空权限缓存并重新从数据库读取
 * </p>
 */
@Service
@Slf4j
public class ShiroService {

    private final ShiroFilterFactoryBean shiroFilterFactoryBean;

    private final SysPermissionService sysPermissionService;

    @Autowired
    public ShiroService(ShiroFilterFactoryBean shiroFilterFactoryBean, SysPermissionService sysPermissionService) {
        this.shiroFilterFactoryBean = shiroFilterFactoryBean;
        this.sysPermissionService = sysPermissionService;
    }

    /**
     * 数据库读取权限配置，封装map
     * @return java.util.Map<java.lang.String ,java.lang.String>
     * @author Mr.ifan
     * @date 2019/5/19
     */
    private Map<String, String> loadFilterChainDefinitions() {
        Map<String, String> filterChainDefinitionMap = new HashMap<>(30);
        filterChainDefinitionMap.put("/logout", "logout");
        filterChainDefinitionMap.put("/user/subLogin", "anon");
//        sysPermissionService.findAllPermissions().forEach(sysPermission -> filterChainDefinitionMap.put(sysPermission.getUrl(),
//                "authc, perms[" + sysPermission.getPermission() + "]"));
        filterChainDefinitionMap.put("/**", "authc");
        return filterChainDefinitionMap;
    }

    /**
     * 清空权限缓存，并且重新重新从数据库读取
     * @author Mr.ifan
     * @date 2019/5/19
     */
    public void updatePermission() {
        synchronized (shiroFilterFactoryBean) {

            try {
                AbstractShiroFilter shiroFilter = (AbstractShiroFilter) shiroFilterFactoryBean.getObject();
                PathMatchingFilterChainResolver filterChainResolver =
                        (PathMatchingFilterChainResolver) Objects.requireNonNull(shiroFilter).getFilterChainResolver();
                DefaultFilterChainManager manager = (DefaultFilterChainManager) filterChainResolver
                        .getFilterChainManager();

                manager.getFilterChains().clear();
                shiroFilterFactoryBean.getFilterChainDefinitionMap().clear();
                shiroFilterFactoryBean.setFilterChainDefinitionMap(loadFilterChainDefinitions());

                Map<String, String> chains = shiroFilterFactoryBean.getFilterChainDefinitionMap();
                chains.forEach(manager::createChain);
            } catch (Exception e) {
                log.error(e.getMessage());
                throw new RuntimeException("Dynamic access permission failed. ");
            }

        }
    }
}
