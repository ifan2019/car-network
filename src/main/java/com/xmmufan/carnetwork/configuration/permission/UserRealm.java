package com.xmmufan.carnetwork.configuration.permission;


import com.xmmufan.carnetwork.constant.permission.AccountState;
import com.xmmufan.carnetwork.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * shiro 用户认证授权realm
 * @author Mr ifan/詹奕凡
 * @date 2019/5/19
 * @version 1.0
 */
public class UserRealm extends AuthorizingRealm {

    @Autowired
    @Lazy
    private UserService userService;


    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("doGetAuthorizationInfo");
        if (principalCollection == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }
        String username = (String) principalCollection.getPrimaryPrincipal();
        Set<String> roles = new HashSet<>();
        Set<String> permissions = new HashSet<>();
//        try {
//            User targetUser = userService.findByUsername(username);
//            targetUser.getRoleList().forEach(sysRole -> {
//                roles.add(sysRole.getRole());
//                sysRole.getPermissions().forEach(sysPermission -> permissions.add(sysPermission.getPermission()));
//            });
//        } catch (NullPointerException e) {
//            final String message = "No account found for user '" + username +"'";
//            throw new UnknownAccountException(message, e);
//        }
//        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roles);
//        info.setStringPermissions(permissions);
//        System.out.println(Arrays.toString(permissions.toArray()));
        return null;
    }

    /**
     *认证方法
     * @param authenticationToken authenticationToken
     * @return AuthenticationInfo
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("认证function");
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        String username = token.getUsername();
        if(StringUtils.isEmpty(username)){
            throw new AccountException("null name can not be login");
        }
//        User user = userService.findByUsername(username);
//        //判断用户是否锁定
//        if (user.getState() == AccountState.LOCKED){
//            throw new LockedAccountException("user: "+username+" this account has been locked. ");
//        }
//        String password = user.getPassword();
//        String salt = user.getSalt();
//        return new SimpleAuthenticationInfo(username, password, ByteSource.Util.bytes(salt), this.getName());
        return null;
    }

    @Override
    public void setCredentialsMatcher(CredentialsMatcher credentialsMatcher) {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("MD5");
        hashedCredentialsMatcher.setHashIterations(8);
        super.setCredentialsMatcher(hashedCredentialsMatcher);
    }
}
