package com.xmmufan.carnetwork.entity.mongodb;

import lombok.Data;

/**
 * 逆变器数据 mongodb
 * @author ifan
 * @version 1.0
 * @date 2019/12/2
 */
@Data
public class Inverter {

    /**
     * 输入电压
     * 当已检测到市电接入时，实时显示市电输入电压；
     * 当“无市电接入”或“有市电输入且逆变器通讯失联”时，则显示“---”
     */
    private String inputVoltage;
    /**
     * 带载功率 当逆变器处于“市电或逆变”状态且系统正常时，
     * 实时显示当前带载功率，当逆变器未开机或“已开机带载且逆变器通讯失联”时，则显示“---”
     * 查询 000301107A
     */
    private String carryPower;
    /**
     * 连接市电的状态
     */
    private boolean electricityState;
    /**
     * 设备状态 0代表正常打开 1代表关闭 2代表故障
     */
    private Integer switchFaultState;
    /**
     * 逆变器状态
     */
    private String inverterState;
    /**
     * 输出功率
     */
    private String outputPower;
    /**
     * 输出功率百分比
     */
    private String outputPowerPercentage;
    /**
     * 输入电流
     */
    private String inputCurrent;
    /**
     * 输入限流
     */
    private String inputLimitCurrent;


}
