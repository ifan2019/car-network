package com.xmmufan.carnetwork.entity.mongodb;

import lombok.Data;

/**
 * 发电状态
 * @author ifan
 * @version 1.0
 * @date 2019/12/2
 */
@Data
public class Electric {
    private Long id;
    /**
     * 判断此设备状态 0代表正常打开 1代表关闭 2代表故障
     */
    private Integer switchFaultState;
    /**
     * 发电功率 当发电类设备已开机时，该处实时显示当前发电功率；
     * 发电类设备未对外通讯（包括通讯失联、关机）时，则显示“---”
     */
    private String electricPower;
    /**
     * 发电总电流 当发电类设备已开机时，
     * 该处实时显示当前发电总电流；发电类设备未对外通讯（包括通讯失联、关机）时，则显示“---”
     */
    private String electricAlli;
    /**
     * 输入电压
     */
    private Double inputVoltage;
    /**
     * 充电电流
     */
    private Long inputI;
    /**
     * 单日发电量
     */
    private Long dayNum;
    /**
     * 累计发电量
     */
    private Long allNum;

}
