package com.xmmufan.carnetwork.entity.mongodb;

import lombok.Data;

import java.util.Date;

/**
 * 水箱数据 mongodb 实体类
 * @author ifan
 * @version 1.0
 * @date 2019/12/2
 */
@Data
public class Tank {
    /**
     * 清水箱
     */
    private String solutionTank;
    /**
     * 灰水箱
     */
    private String greyTank;
    /**
     * 黑水箱
     */
    private String blackTank;
    /**
     * 水箱设备状态
     */
    private Integer switchFaultState;

    private Date time;

    private String carVin;
}
