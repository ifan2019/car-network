package com.xmmufan.carnetwork.entity.mongodb;

import lombok.Data;

/**
 * 灯光
 * @author ifan
 * @version 1.0
 * @date 2019/12/2
 */
@Data
public class Light {
    private Long id;
    /**
     * 灯光等级
     * 等级 0 1 2对应亮度
     */
    private Long mainLight;
}
