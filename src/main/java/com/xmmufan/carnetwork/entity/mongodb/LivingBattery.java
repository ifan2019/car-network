package com.xmmufan.carnetwork.entity.mongodb;

import lombok.Data;

/**
 * 电池 mongodb
 * @author ifan
 * @version 1.0
 * @date 2019/12/2
 */
@Data
public class LivingBattery {

    private Long id;
    /**
     * 电池充电还是放电
     */
    private String batteryType;
    /**
     * 电流
     * 放电电流为负数,充电为正数
     */
    private String batteryTypeNum;
    /**
     *电池剩余容量
     */
    private Integer capacity;
    /**
     * 显示电池类型的充放电时间
     *若在充电,则是离充满的时间,
     * 若不在充电,则是剩余可放电时间.
     * 若故障,则---
     */
    private String time;
    /**
     * 电池电压,若失联为0
     */
    private Integer batteryVoltage;

}
