//package com.xmmufan.carnetwork.entity.monitor;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//
//@Data
//@Entity
//@NoArgsConstructor
//@AllArgsConstructor
//@Table(name = "modified_report")
//public class ModifiedReport {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    @ManyToOne(targetEntity = Car.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH})
//    @JoinColumn(name="car_id")
//    private Car car;
//
//    @ManyToOne(targetEntity = Device.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH})
//    @JoinColumn(name="device_id")
//    private Device device;
//
//}
