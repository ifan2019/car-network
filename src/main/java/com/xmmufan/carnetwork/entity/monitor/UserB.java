package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="user_b")
@Entity
public class UserB implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 用户名
     */
    @Column(name="user_name")
    private String userName;

    @Column(name = "avatar")
    private String avatar;

    /**
     * 密码
     */
    @Column(name="password")
    private String password;

    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name="create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;

    /**
     * 盐值
     */
    private String salt;

    /**
     * 用户状态,0:创建未认证（比如没有激活，没有输入验证码等等）--等待验证的用户 , 1:正常状态,2：用户被锁定.
     */
    private int state;

    /**
     * 品牌
     */
    @Column(name = "brand")
    private String brand;


    /**
     * 性别
     */
    @Column(name="gender")
    private String gender;

    /**
     * 企业名称
     */
    @Column(name="enterprise")
    private String enterprise;

    /**
     * 法人代表
     */
    @Column(name="name")
    private String name;

    /**
     * 身份证
     */
    @Column(name="idcard")
    private String idCard;

    /**
     * 邮箱
     */
    @Column(name="mailbox")
    private String mailbox;

    /**
     * 电话
     */
    @Column(name="phone")
    private String phone;

    /**
     * 营业执照
     */
    @Column(name="license")
    private String license;

    @JsonIgnore
    @OneToMany(mappedBy = "userB",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    List<OperationRecord> operationRecordList;

    @JsonIgnore
    @OneToMany(mappedBy = "userB",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<CarInfo> carInfoList;

}
