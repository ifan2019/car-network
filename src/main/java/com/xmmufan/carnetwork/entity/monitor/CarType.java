package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 陈丽
 * @date   2019-5-31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="car_type")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@Entity
public class CarType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name="create_time")
    private Date createTime;


    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;

    @Column(name="car_type_name")
    private String carTypeName;

    /**
     * 车辆型号
     */
    @Column(name="car_model")
    private String carModel;

    @OneToMany(mappedBy = "carType1")
    @JsonIgnore
    private List<Car> carList;
}
