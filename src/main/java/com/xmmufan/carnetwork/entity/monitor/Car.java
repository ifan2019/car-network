package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * //mybatis通用接口mapper依赖JPA实体类采用JPA
 * @author /陈丽
 * @date 2019/5/20
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@Table(name="car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //主键

    private Long id;

    /**
     * 车辆类型
     */
    @Column(name="car_type")
    private  String carType;

    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name="create_time")
    private Date createTime;


    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;


    @ManyToOne(targetEntity = CarType.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH},fetch = FetchType.LAZY)
    @JoinColumn(name="cartype_id")
    private CarType carType1;
}
