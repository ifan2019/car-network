package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author  chenli
 *@date      2019-8-3
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="img_url")
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class ImgUrl implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="img_id")
    private Long imgId;

    @Column(name="img_url")
    private String imgUrl;


    /**
     * 创建时间
     */
    @Column(name="create_time")
    @CreatedDate
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;

    @ManyToOne(targetEntity = CarTypePrice.class,fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.REFRESH})
    @JoinColumn(name="cartype_price_id")
    @JsonIgnore
    private CarTypePrice carTypePrice;

    /**
     * 0 轮播图
     * 1 详细图
     */
    private String imgType;

//    @ManyToOne(targetEntity = Device.class,fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.REFRESH})
//    @JoinColumn(name="device_id")
//    @JsonIgnore
//    private Device device;
}
