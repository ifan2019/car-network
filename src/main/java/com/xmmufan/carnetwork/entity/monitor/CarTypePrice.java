package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.xmmufan.carnetwork.entity.vo.CarTypePriceVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="car_type_price")
@Entity
public class CarTypePrice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * 车辆类型id
     */
    @Column(name="car_type_id")
    private Long carTypeId;

    /**
     * 车辆设备类型id
     */
    @Column(name="carDeviceIds")
    private String carDeviceIds;

    /**
     * 车辆类型name
     */
    @Column(name = "type_name")
    private String typeName;
    /**
     * 车辆日租金
     */
    @Column(name="price")
    private Double price;

    /**
     * 车辆押金
     */
    @Column(name = "deposit")
    private Double deposit;

    /**
     * 品牌
     */
    @Column(name = "brand")
    private String brand;

    @ManyToOne(targetEntity = UserF.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH},fetch = FetchType.LAZY)
    @JoinColumn(name = "userf_id")
    private UserF userF;

    @ManyToOne(targetEntity = City.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH},fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    private City city;

    @OneToMany(mappedBy = "carTypePrice",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JsonIgnore
    private List<ImgUrl> imgUrls;

    public CarTypePrice (CarTypePriceVo carTypePriceVo){
        this.id = carTypePriceVo.getId();
        this.carTypeId = carTypePriceVo.getCarTypeId();
        this.carDeviceIds = carTypePriceVo.getCarDeviceIds();
        this.typeName = carTypePriceVo.getTypeName();
        this.price = 0.0;
        this.deposit = 0.0;
        this.brand = carTypePriceVo.getBrand();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CarTypePrice that = (CarTypePrice) o;
        return Objects.equals(carTypeId, that.carTypeId) &&
                Objects.equals(carDeviceIds, that.carDeviceIds) &&
                Objects.equals(typeName, that.typeName) &&
                Objects.equals(brand, that.brand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carTypeId,carDeviceIds,typeName,brand);
    }
}
