//package com.xmmufan.carnetwork.entity.monitor;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//
///**
// * @author  陈丽
// * @date    2019-7-28
// */
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//@Table(name="interactive_information")
//@Entity
//public class InteractiveInformation {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name="info_id")
//    private Long infoId;
//
//    @Column(name="info")
//    private String info;
//
//    @Column(name="carSequenceSign")
//    private String carSequenceSign;
//}
