package com.xmmufan.carnetwork.entity.monitor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/8/12
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tb_order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="order_id")
    private Long id;

    /**
     * 订单编号
     */
    @Column(name="order_number")
    private String orderNumber;

    /**
     * i订单状态
     */
    @Column(name="order_status")
    private String orderStatus;

    /**
     * 订单时间
     */
    @CreatedDate
    @Column(name="order_time")
    private Date  orderTime;

    /**
     * 订单类型
     */
    @Column(name="order_type")
    private String orderType;

    /**
     * 取车时间
     */
    @Column(name="pickup_car_time")
    private Date pickUpCarTime;

    /**
     * 还车时间
     */
    @Column(name="pickout_car_time")
    private Date pickOutCarTime;

    /**
     * 取车城市
     */
    @Column(name="pickup_car_city")
    private String pickUpCarCity;


    /**
     * 还车城市
     */
    @Column(name="pickout_car_city")
    private String pickOutCarCity;

    /**
     * 帮其他人下单驾驶证
     */
    @Column(name="license")
    private String license;


    /**
     * 帮其他人下单身份证
     */
    @Column(name="idcard")
    private String idCard;


    /**
     * 第一联系人
     */
    @Column(name="first_relationer")
    private String firstRelationer;

    /**
     * 第二联系人
     */
    @Column(name="second_relationer")
    private  String secondRelationer;

    /**
     * 订单总金额
     */
    @Column(name="order_totalprice")
    private Double orderTotalPrice;


    /**
     * 押金
     */
    @Column(name="order_deposit")
    private Double orderDeposit;


    /**
     * 赔偿金
     */
    @Column(name="order_damages")
    private Double orderDamages;


    /**
     * 订单最晚支付时间
     */
    @Column(name="order_last_payment")
    private Date orderLastPayment;

    /**
     * 实际提车时间
     */
    @Column(name="actual_carriage_time")
    private Date actualCarriageTime;

    /**
     * 实际还车时间
     */
    @Column(name="actual_return_time")
    private Date actualReturnTime;
    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name="create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;


    @ManyToOne(targetEntity = CarInfo.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH},fetch = FetchType.LAZY)
    @JoinColumn(name="car_info_id")
    private CarInfo carInfo;

    @ManyToOne(targetEntity = UserC.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH},fetch = FetchType.LAZY)
    @JoinColumn(name="userc_id")
    private UserC userC;
}
