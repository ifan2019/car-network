package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.experimental.Accessors;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author  chenli
 *@date      2019-8-3
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="car_info")
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class CarInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long  id;

    /**
     * 车辆VIN
     */
    @Column(name="car_vin")
    private String carVin;

    /**
     * 底盘信息
     */
    @Column(name="chassis_info")
    private String chassisInfo;

    /**
     * 车牌
     */
    @Column(name="licentse_plate")
    private String licentsePlate;

    /**
     * 品牌
     */
    @Column(name="brand")
    private String brand;

    /**
     * 租赁状态
     */
    @Column(name="lease_status")
    private String leaseStatus;

    /**
     * 车辆价格
     */
    @Column(name="price")
    private double price;

    /**
     * 车辆押金
     */
    @Column(name="car_deposit")
    private double carDeposit;

    /**
     * 车辆状态
     */
    @Column(name="car_status")
    private String carStatus;

    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name="create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;

    /**
     * 车辆设备标识
     */
    @Column(name="sequence_sign")
    private String carSequenceSign;

    @OneToOne(targetEntity = Car.class,fetch = FetchType.LAZY)
    @JoinColumn(name="car_id")
    private Car car;

    @ManyToMany(mappedBy = "carInfoList")
    @JsonIgnore
    private List<Device> deviceList;

//    @OneToMany(mappedBy = "carInfo",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
//    private List<ImgUrl> imgUrls;

    @ManyToOne(targetEntity = Garage.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH},fetch = FetchType.LAZY)
    @JoinColumn(name = "garage_id")
    @JsonIgnore
    private Garage garage;

    @OneToMany(mappedBy = "carInfo",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Order> orderList;

    /**
     * 多辆车归属一个车队
     */
    @ManyToOne(targetEntity = UserB.class,fetch = FetchType.LAZY,
            cascade = {CascadeType.MERGE,CascadeType.REFRESH})
    private UserB userB;

    @ManyToOne(targetEntity = UserF.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH},fetch = FetchType.LAZY)
    @JoinColumn(name="userf_id")
    private UserF userF;


    @ManyToOne(targetEntity = Store.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH},fetch = FetchType.LAZY)
    @JoinColumn(name="store_id")
    @JsonIgnore
    private Store store;

    @OneToMany(mappedBy = "carInfo")
    @JsonIgnore
    private List<CarDevice> carDeviceList;
}



