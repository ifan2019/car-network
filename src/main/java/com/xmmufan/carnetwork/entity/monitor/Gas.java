//package com.xmmufan.carnetwork.entity.monitor;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import org.springframework.data.annotation.CreatedDate;
//import org.springframework.data.annotation.LastModifiedDate;
//
//import javax.persistence.*;
//import java.util.Date;
//
///**
// * @author 陈丽
// * @date 2019/5/19
// * @version 1.0
// * //初审通过
// */
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//@Table(name="gas")
//@Entity
//public class Gas {
//    @Id
//    @GeneratedValue(strategy= GenerationType.IDENTITY)
//    private Long id;
//
//    /**
//     * 煤气桶数据
//     */
//    @Column(name="gas_barrel")
//    private Double gasBarrel;
//
//    /**
//     * 发电机数据
//     */
//    @Column(name="dynamo")
//    private Double dynamo;
//
//    /**
//     * 电冰箱数据
//     */
//    @Column(name="fridge")
//    private Double fridge;
//
//    /**
//     * 空调数据
//     */
//    @Column(name="air_conditioner")
//    private Double airConditioner;
//
//    /**
//     * 锂电池循环数据
//     */
//    @Column(name="lithium_battery_cycle")
//    private Double lithiumBatteryCycle;
//
//    /**
//     * 逆变器数据
//     */
//    @Column(name="inverter")
//    private Double inverter;
//
//    /**
//     * 创建时间
//     */
//    @CreatedDate
//    @Column(name="create_time")
//    private Date createTime;
//
//
//    /**
//     * 修改时间
//     */
//    @LastModifiedDate
//    @Column(name="update_time")
//    private Date updateTime;
//
//
//    @OneToOne(targetEntity = Car.class,fetch = FetchType.LAZY)
//    @JoinColumn(name="car_id")
//    private Car car;
//}
//
