package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="user_c")
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
/**
 * 用户—————C端
 */
public class UserC implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="user_id")
    private Integer id;

    /**
     * 用户名
     */
    @Column(name="user_name")
    private String userName;

    /**
     * 身份证
     */
    @Column(name="user_idcard")
    private String userIdCard;

    /**
     * 实名状态
     */
    @Column(name="real_name_status")
    private int realNameStatus;

    /**
     * 用户电话
     */
    @Column(name="user_tel")
    private String userTel;

    /**
     * 用户头像
     */
    @Column(name="user_img")
    private String userImg;

    /**
     * 用户驾驶证
     */
    @Column(name="user_license")
    private String userLicense;

    /**
     * 用户钱包余额
     */
    @Column(name="user_money")
    private double userMoney;

    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name="create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;

    @OneToMany(mappedBy = "userC",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Order> orderList;
}
