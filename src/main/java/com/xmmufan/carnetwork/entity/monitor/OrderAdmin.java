package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author  chenli
 * @date      2019-8-3
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="order_admin")
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
@Accessors(chain = true)
public class OrderAdmin {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="id")
        private Long id;

        /**
         * 用户名
         */
        @Column(name="user_name")
        private String userName;

        /**
         * 管理员真实姓名
         */
        @Column(name = "name")
        private String name;

        /**
         * 密码
         */
        @Column(name="password")
        private String password;

        /**
         * 管理员类型
         */
        @Column(name="type")
        private String type;

        @Column(name = "gender")
        private String gender;

        @Column(name = "mailbox")
        private String mailbox;

        @Column(name = "phone")
        private String phone;
        /**
         * 创建时间
         */
        @Column(name="create_time")
        @CreatedDate
        private Date createTime;

        /**
         * 修改时间
         */
        @LastModifiedDate
        @Column(name="update_time")
        private Date updateTime;

        /**
         * 盐值
         */
        @Column(name="salt")
        private String salt;

        /**
         * 状态
         */
        @Column(name="state")
        private int state;

        @ManyToOne(targetEntity = UserF.class,fetch = FetchType.LAZY)
        @JoinColumn(name="userf_id")
        private UserF userF;

        @ManyToOne(targetEntity = City.class,fetch = FetchType.LAZY)
        @JoinColumn(name="city_id")
        private City city;
}
