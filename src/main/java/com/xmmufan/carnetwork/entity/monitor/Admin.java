package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author  chenli
 * @date      2019-8-3
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="admin")
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class Admin implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 用户名
     */
    @Column(name="user_name")
    private String userName;

    /**
     * 管理员姓名
     */
    @Column(name="name")
    private String name;

    /**
     * 密码
     */
    @Column(name="password")
    private String password;

    /**
     * 创建时间
     */
    @Column(name="create_time")
    @CreatedDate
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;

    /**
     * 盐值
     */
    @Column(name="salt")
    private String salt;

    /**
     * 状态
     */
    @Column(name="state")
    private int state;

    /**
     * 性别
     */
    @Column(name="gender")
    private String gender;

    /**
     * 邮箱
     */
    @Column(name="mailbox")
    private String mailbox;

    /**
     * 电话
     */
    @Column(name="phone")
    private String phone;

    @ManyToOne(targetEntity = UserF.class)
    @JoinColumn(name = "userf_id")
    private UserF userF;

    @ManyToOne(targetEntity = City.class)
    @JoinColumn(name = "city_id")
    private City city;

    @ManyToMany(mappedBy = "adminList")
    @JsonIgnore
    private List<Role> roleList;

    @OneToMany(mappedBy = "admin")
    @JsonIgnore
    private List<AdminRole> adminRoleList;

}
