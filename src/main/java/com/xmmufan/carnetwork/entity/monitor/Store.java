package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author   chenli
 * @date       2019-8-3
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="store")
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class Store implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    /**
     * 门店名称
     */
    @Column(name="store_name")
    private String storeName;

    /**
     * 门店所在地
     */
    @Column(name="store_location")
    private String storeLocation;

    /**
     * 门店运营时间
     */
    @Column(name="store_operating_time")
    private String storeOperatingTime;

    /**
     *门店联系方式
     */
    @Column(name="store_tel")
    private String storeTel;


    /**
     * 创建时间
     */
    @Column(name="create_time")
    @CreatedDate
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;

    @ManyToOne(targetEntity =UserF.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH},fetch = FetchType.LAZY)
    @JoinColumn(name="userf_id")
    private  UserF userF;

    @OneToMany(mappedBy = "store")
    @JsonIgnore
    private List<CarInfo> carInfoList;

    @ManyToOne(targetEntity =City.class,cascade = {CascadeType.MERGE,CascadeType.REFRESH},fetch = FetchType.LAZY)
    @JoinColumn(name="city_id")
    private City city;
}
