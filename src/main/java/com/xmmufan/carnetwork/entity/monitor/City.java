package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="city")
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@Accessors(chain = true)
@EntityListeners(AuditingEntityListener.class)
public class City implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="city_id")
    private Long cityId;

    /**
     * 城市名称
     */
    @Column(name="city_name")
    private String cityName;

    /**
     * 城市编码
     */
    @Column(name="city_code")
    private String cityCode;

    /**
     * 创建时间
     */
    @Column(name="create_time")
    @CreatedDate
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;

    @ManyToMany(mappedBy = "cityList")
    @JsonIgnore
    private List<UserF> userFList;

//    @OneToMany(mappedBy ="city")
//    @JsonIgnore
//    private List<UserF_City> userFCityList;
//
//    @OneToMany(mappedBy = "city")
//    private List<Store> storeList;
//
//    @OneToMany(mappedBy = "city")
//    @JsonIgnore
//    private List<CityAdmin> cityAdminList;
//
//    @OneToMany(mappedBy = "city")
//    @JsonIgnore
//    private List<OrderAndCarAdmin> orderAndCarAdminList;
}
