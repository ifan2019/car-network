package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="device")
@Entity
@Accessors(chain = true)
public class Device {
    /**
     * 设备类型id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "device_id")
    private Long deviceId;


    /**
     * 设备类型名称
     */
    @Column(name = "device_name")
    private String deviceName;

    /**
     * 设备类型图标
     */
    @Column(name = "device_icon")
    private String deviceIcon;

    @ManyToMany
    @JoinTable(name = "car_device", joinColumns = @JoinColumn(name = "device_id"), inverseJoinColumns = @JoinColumn(name = "car_info_id"))
    @JsonIgnore
    private List<CarInfo> carInfoList;

}
