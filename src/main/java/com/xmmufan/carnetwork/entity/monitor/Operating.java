package com.xmmufan.carnetwork.entity.monitor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/8/12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="operator")
@Entity
public class Operating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 状态名称
     */
     @Column(name="status_name")
     private String statusName;

    /**
     * 当前状态
     */
    @Column(name="current_status")
    private String currentStatus;

    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH},optional = false)
    @JoinColumn(name="car_id")
    private Car car;

    @ManyToOne(targetEntity = OperatingType.class,fetch = FetchType.LAZY)
    @JoinColumn(name="type_id")
    private OperatingType operatingType;
    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name="create_time")
    private Date createTime;


    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;


}
