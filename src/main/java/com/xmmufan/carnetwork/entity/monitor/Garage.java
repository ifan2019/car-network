package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/8/12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="garage")
@Entity
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class Garage implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="garage_id")
    private Long garageId;

    @Column(name="car_plate")
    private String carPlate;

    /**
     * 创建时间
     */
    @Column(name="create_time")
    @CreatedDate
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;

    @OneToMany(mappedBy = "garage",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JsonIgnore
    private List<CarInfo> carInfoList;

    @OneToOne(targetEntity = UserC.class,fetch = FetchType.LAZY)
    private UserC userC;

}
