package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.List;

/**
 * 角色表
 * @author yifan
 * @version 1.0
 * @date 2019/11/15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="role")
@Entity
@Accessors(chain = true)
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "role")
    private String role;
    @Column(name = "description")
    @JsonIgnore
    private String description;

    @ManyToMany
    @JoinTable(name="sys_admin_role",joinColumns={@JoinColumn(name="role_id")},inverseJoinColumns={@JoinColumn(name=
            "admin_id")})
    @JsonIgnore
    private List<Admin> adminList;

    @OneToMany(mappedBy = "role")
    @JsonIgnore
    private List<AdminRole> roleList;

    @ManyToMany(mappedBy = "roleList")
    @JsonIgnore
    private List<Permission> permissionList;

}
