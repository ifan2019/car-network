package com.xmmufan.carnetwork.entity.monitor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="user_f")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
@Entity
public class UserF implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 用户名
     */
    @Column(name="user_name")
    private String userName;

    /**
     * 密码
     */
    @Column(name="password")
    private String password;

    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name="create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @Column(name="update_time")
    private Date updateTime;

    /**
     *企业名称
     */
    @Column(name="enterprise")
    private String enterprise;

    /**
     * 法人名称
     */
    @Column(name="deputy")
    private String deputy;


    /**
     * 邮箱
     */
    @Column(name="mailbox")
    private String mailbox;

    /**
     * 电话
     */
    @Column(name="phone")
    private String phone;

    /**
     * 营业执照
     */
    @Column(name="license")
    private String license;

    /**
     * idCard
     */
    @Column(name="idcard")
    private String idCard;

    /**
     * 性别
     */
    @Column(name="gender")
    private String  gender;

    /**
     * 盐值
     */
    private String salt;

    /**
     * 用户状态,0:创建未认证（比如没有激活，没有输入验证码等等）--等待验证的用户 , 1:正常状态,2：用户被锁定.
     */
    private byte state;

    @OneToMany(mappedBy = "userF",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JsonIgnore
    private List<OperationRecord> operationRecordList;

    @OneToMany(mappedBy = "userF",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JsonIgnore
    private List<CarInfo> carInfoList;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name="userf_city",joinColumns = @JoinColumn(name="userf_id"),inverseJoinColumns = @JoinColumn(name="city_id"))
    private List<City> cityList;
}

