//package com.xmmufan.carnetwork.entity.permission;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//import java.util.List;
//
///**
// * 系统权限 domain
// * @author Mr ifan/詹奕凡
// * @date 2019/5/18
// */
//
//@Data
//@Entity
//@Table(name = "sys_permission")
//@NoArgsConstructor
//@AllArgsConstructor
//public class SysPermission {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Integer id;
//
//    private String name;
//
//    /**
//     * 资源类型，[menu|button]
//     */
//    @Column(columnDefinition="enum('menu','button')")
//    private String resourceType;
//
//    /**
//     * 资源路径
//     */
//    private String url;
//
//    /**
//     * 权限字符串,menu例子：role:*，button例子：role:create,role:update,role:delete,role:view
//     */
//    private String permission;
//
//    /**
//     * 父编号
//     */
//    private Long parentId;
//
//    /**
//     * 父编号列表
//     */
//    private String parentIds;
//
//    private Boolean available = Boolean.FALSE;
//
//    @ManyToMany
//    @JoinTable(name="SysRolePermission",joinColumns={@JoinColumn(name="permissionId")},inverseJoinColumns={@JoinColumn(name="roleId")})
//    private List<SysRole> roles;
//}
