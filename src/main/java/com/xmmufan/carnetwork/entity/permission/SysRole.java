//package com.xmmufan.carnetwork.entity.permission;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//import java.util.List;
//
///**
// * 系统角色domain
// * @author Mr ifan/詹奕凡
// * @date 2019/5/18
// * @version 1.0
// */
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//@Entity
//@Table(name = "sys_role")
//public class SysRole {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Integer id;
//    @Column(name = "role")
//    private String role;
//    @Column(name = "description")
//    private String description;
//    @Column(name = "available")
//    private Boolean available = Boolean.FALSE;
//    @ManyToMany(fetch= FetchType.EAGER)
//    @JoinTable(name="SysRolePermission",joinColumns={@JoinColumn(name="roleId")},inverseJoinColumns={@JoinColumn(name="permissionId")})
//    private List<SysPermission> permissions;
//
//    @ManyToMany
//    @JoinTable(name="SysUserRole",joinColumns={@JoinColumn(name="roleId")},inverseJoinColumns={@JoinColumn(name=
//            "user_id")})
//    private List<User> users;
//}
