//package com.xmmufan.carnetwork.entity.permission;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.util.List;
//
///**
// * //mybatis通用接口mapper依赖JPA实体类采用JPA
// * @author Mr ifan/詹奕凡
// * @date 2019/5/19
// * @version 1.0
// */
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//@Table(name = "user")
//@Entity
//public class User implements Serializable {
//    @Id
//    private Long id;
//
//    /**
//     * 名称
//     */
//    @Column(unique = true)
//    private String username;
//
//    /**
//     * 密码
//     */
//    private String password;
//
//    /**
//     * 盐值
//     */
//    private String salt;
//
//    /**
//     * 用户状态,0:创建未认证（比如没有激活，没有输入验证码等等）--等待验证的用户 , 1:正常状态,2：用户被锁定.
//     */
//    private byte state;
//
//    /**
//     * 一个用户具有多个角色
//     */
//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "SysUserRole", joinColumns = {@JoinColumn(name = "user_id")},
//            inverseJoinColumns = {@JoinColumn(name = "roleId")})
//    private List<SysRole> roleList;
//}