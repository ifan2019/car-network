package com.xmmufan.carnetwork.entity.vo;

import com.xmmufan.carnetwork.entity.monitor.CarTypePrice;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/9/28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarTypePriceVo {
    private Long id;

    /**
     * 车辆类型id
     */
    private Long carTypeId;

    /**
     * 车辆设备类型id
     */
    private String carDeviceIds;

    /**
     * 车辆类型name
     */
    private String typeName;
    /**
     * 车辆日租金
     */
    private Double price;

    /**
     * 车辆押金
     */
    private Double deposit;

    /**
     * 品牌
     */
    private String brand;

    private Long currentAdminId;

    public CarTypePriceVo (CarTypePrice carTypePrice){
        this.id = carTypePrice.getId();
        this.typeName = carTypePrice.getTypeName();
        this.carTypeId = carTypePrice.getCarTypeId();
        this.carDeviceIds = carTypePrice.getCarDeviceIds();
        this.price = carTypePrice.getPrice();
        this.deposit = carTypePrice.getDeposit();
        this.brand = carTypePrice.getBrand();
    }

}
