package com.xmmufan.carnetwork.entity.vo;

import com.xmmufan.carnetwork.entity.monitor.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarInfoVo {
    private Long id;
    private String carVin;
    private String chassisInfo;
    private String licentsePlate;
    private String brand;
    private String leaseStatus;
    private Double price;
    private Double carDeposit;
    private String carSequenceSign;
    private String carStatus;
    private Long carTypeId;
    private String carTypeName;
    private List<ImgUrl> imgUrls;
    private Long userBId;
    private String userBName;
    private Long cityId;
    private String cityName;
    private List<Long> devices;
    private List<String> devicesName;
    public CarInfoVo(CarInfo carInfo){
        this.id = carInfo.getId();
        this.carVin = carInfo.getCarVin();
        this.chassisInfo = carInfo.getChassisInfo();
        this.licentsePlate = carInfo.getLicentsePlate();
        this.brand = carInfo.getBrand();
        this.price =carInfo.getPrice();
        this.carDeposit = carInfo.getCarDeposit();
        this.carSequenceSign = carInfo.getCarSequenceSign();
        this.carStatus = this.getCarStatus();
        CarType carType = carInfo.getCar().getCarType1();
        this.carTypeId = carType.getId();
        this.carTypeName = carType.getCarTypeName() + carType.getCarModel();
//        this.imgUrls = carInfo.getImgUrls();
        UserB userB = carInfo.getUserB();
        this.userBId = userB.getId();
        this.userBName = userB.getEnterprise();
        List<Device> deviceList = carInfo.getDeviceList();
        this.devices = new ArrayList<>();
        this.devicesName = new ArrayList<>();
        for(Device device: deviceList){
            this.devices.add(device.getDeviceId());
            this.devicesName.add(device.getDeviceName());
        }
        City city = carInfo.getStore().getCity();
        this.cityId = city.getCityId();
        this.cityName = city.getCityName();
    }
}
