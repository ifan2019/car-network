package com.xmmufan.carnetwork.entity.vo;

import com.xmmufan.carnetwork.entity.monitor.UserB;
import lombok.Data;

import java.util.Set;

@Data
public class UserBInfoVo {


    private Long id;

    private String username;

    private String enterprise;

    public UserBInfoVo() {
    }

    public UserBInfoVo(UserB user) {
        this.id = user.getId();
        this.username = user.getUserName();
        this.enterprise = user.getEnterprise();
//        Set<RoleInfoVo> roleInfoVos = new HashSet<>();
//        Set<PermissionVo> permissionVos = new HashSet<>();
//        user.getRoleList().forEach(sysRole -> {
//            roleInfoVos.add(new RoleInfoVo(sysRole));
//            sysRole.getPermissions().forEach(sysPermission -> permissionVos.add(new PermissionVo(sysPermission)));
//        });
//        this.setPermissionSet(permissionVos);
//        this.setRoleSet(roleInfoVos);
    }
}
