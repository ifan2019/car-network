package com.xmmufan.carnetwork.entity.vo;

import com.xmmufan.carnetwork.entity.monitor.UserF;

import java.util.Set;

public class UserFInfoVo {



    private Long id;

    private String username;


//    private String avatar;

    private byte state;

    private Set<RoleInfoVo> roleSet;

    private Set<PermissionVo> permissionSet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public byte getState() {
        return state;
    }

    public void setState(byte state) {
        this.state = state;
    }

    public Set<RoleInfoVo> getRoleSet() {
        return roleSet;
    }

    public void setRoleSet(Set<RoleInfoVo> roleSet) {
        this.roleSet = roleSet;
    }

    public Set<PermissionVo> getPermissionSet() {
        return permissionSet;
    }

    public void setPermissionSet(Set<PermissionVo> permissionSet) {
        this.permissionSet = permissionSet;
    }

    public UserFInfoVo() {
    }

    public UserFInfoVo(UserF user) {
        this.id = user.getId();
        this.username = user.getUserName();
//        this.avatar = user.getAvatar();
        this.state = user.getState();
//        Set<RoleInfoVo> roleInfoVos = new HashSet<>();
//        Set<PermissionVo> permissionVos = new HashSet<>();
//        user.getRoleList().forEach(sysRole -> {
//            roleInfoVos.add(new RoleInfoVo(sysRole));
//            sysRole.getPermissions().forEach(sysPermission -> permissionVos.add(new PermissionVo(sysPermission)));
//        });
//        this.setPermissionSet(permissionVos);
//        this.setRoleSet(roleInfoVos);
    }
}
