package com.xmmufan.carnetwork.entity.vo;

import com.xmmufan.carnetwork.entity.monitor.Admin;
import lombok.Data;

@Data
public class CarAdminVo {

    private Long id;

    private String username;

    private String name;

    private int state;

    private String enterprise;

    public CarAdminVo(Admin carAdmin){
        this.id = carAdmin.getId();
        this.username = carAdmin.getUserName();
        this.name = carAdmin.getName();
        this.state = carAdmin.getState();
        this.enterprise = carAdmin.getUserF().getEnterprise();
    }
}
