package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.service.CarStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @author 陈丽
 * @date   2019/5/24
 */
@RestController
@RequestMapping("/carStatus")
public class CarStatusController {
    @Autowired
    private CarStatusService carStatusServiceImpl;

    /**
     * 获取车辆状态及行车安全信息
     * @param carId
     * @return
     */
    @GetMapping("")
    public ResponseModel getCarStatus(int carId){
        ResponseModel model =new ResponseModel();
        HashMap<String ,Object> data=carStatusServiceImpl.getCarStatus(carId);
        model.setStatusCode(data!=null? HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"操作成功":"操作失败");
        model.setData(data);
        return model;
    }

    /**
     * 获取车辆状态及行车安全信息
     * @return
     */
    @GetMapping("/getCarStatusAndSafe")
    public ResponseModel getCarStatusAndSafe(){
        ResponseModel model =new ResponseModel();
        HashMap<String ,Object> data=carStatusServiceImpl.getCarStatusAndSafe();
        model.setStatusCode(data!=null? HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"操作成功":"操作失败");
        model.setData(data);
        return model;
    }


}
