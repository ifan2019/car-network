package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.service.CarTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author  陈丽
 * @date    2019-6-2
 */
@RestController
@RequestMapping("/carType")
public class CarTypeController {

    @Autowired
    private CarTypeService carTypeServiceImpl;

    /**
     * 获取房车类型
     * @return
     */
    @GetMapping("getAllCarType")
    public ResponseModel getCarType(){
        ResponseModel model=new ResponseModel();
        Map<String,Object> data=carTypeServiceImpl.getListCarType();
        model.setStatusCode(data!=null? HttpStatusCode.OK :HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"获取成功":"获取失败");
        model.setData(data);
        return model;
    }
}

