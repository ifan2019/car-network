package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.service.RentService;
import com.xmmufan.carnetwork.service.impl.RentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author   陈丽
 * @date     2019-5-26
 */
@RestController
@RequestMapping("/rent")
public class RentController {

    @Autowired
    private RentService rentServiceImpl;

    /**
     * 车辆租赁信息
     * @return
     */
    @GetMapping("/listRentInfo")
    public ResponseModel getRentInfoList(Long currentAdminId){
        ResponseModel model=new ResponseModel();
        Map<String,Object> data=rentServiceImpl.getRentInfoList(currentAdminId);
        model.setStatusCode(data!=null? HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"获取成功":"获取失败");
        model.setData(data);
        return model;
    }

    /**
     * 通过message查询租赁信息
     * @param id
     * @param message
     * @return
     */
    @GetMapping("getRentInfoByCondition")
    public ResponseModel getCarInfoByVin(Long id,String message){
        ResponseModel model=new ResponseModel();
        Map<String,Object> data= rentServiceImpl.getRentInfoByCondition(id,message);
        model.setStatusCode(data!=null? HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"获取成功":"获取失败");
        model.setData(data);
        return model;
    }
}
