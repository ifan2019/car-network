package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.entity.monitor.Device;
import com.xmmufan.carnetwork.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/8/13
 */
@RestController
public class DeviceController {
    @Autowired
    private DeviceService deviceService;

    @GetMapping("/getAllDevice")
    public ResponseModel getAllDevice(){
        ResponseModel responseModel = new ResponseModel();
        List<Device> device = deviceService.getAllDevice();
        responseModel.setStatusCode(device!=null? HttpStatusCode.OK :HttpStatusCode.INTERNAL_SERVER_ERROR);
        responseModel.setMessage(device!=null?"获取成功":"获取失败!");
        responseModel.setData(device);
        return responseModel;
    }
}
