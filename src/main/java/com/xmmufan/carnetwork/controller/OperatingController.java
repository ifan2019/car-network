package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.entity.monitor.Operating;
import com.xmmufan.carnetwork.service.OperatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author  陈丽
 * @date    2019-5-25
 */
@RestController
@RequestMapping("/operating")
public class OperatingController {
    @Autowired
    private OperatingService operatingServiceImpl;

    /**
     * 获取智能操作信息
     * @param carId
     * @return ResponseModel
     */
    @GetMapping("/getOperatingInfo")
    public ResponseModel getOperatingInfo(int carId){
        System.out.print("id="+carId);
        ResponseModel model=new ResponseModel();
        Map<String, Object> data=operatingServiceImpl.getOperatingInfo(carId);
        model.setStatusCode(data!=null? HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"操作成功":"操作失败");
        model.setData(data);
        return model;
    }

    /**
     * 改变智能操控按钮转态
     * @return
     */
    @PostMapping("/updateOperatingStatus")
    public ResponseModel updateOperatingStatus(Operating operating){
        System.out.print("operating="+operating);
        ResponseModel model=new ResponseModel();
        Map<String,Object> data=operatingServiceImpl.updateOperating(operating.getCurrentStatus(),operating.getId());
        model.setStatusCode(data!=null?HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"操作成功":"操作失败");
        model.setData(data);
        return model;
    }

    /**
     * 用户安全及智能控制
     * @return
     */
    @GetMapping("/getUserSafeOrOperating")
    public ResponseModel getUserSafeOrOperating(){
        ResponseModel model=new ResponseModel();
        Map<String,Object> data=operatingServiceImpl.getUserSafeOrOperating();
        model.setStatusCode(data!=null?HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"操作成功":"操作失败");
        model.setData(data);
        return model;
    }


}
