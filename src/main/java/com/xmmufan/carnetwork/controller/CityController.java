package com.xmmufan.carnetwork.controller;


import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.entity.monitor.City;
import com.xmmufan.carnetwork.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

/**
 *
 */
@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    private CityService cityService;

    @GetMapping("/getCityByUserF")
    public ResponseModel getCityByCarAdminId(Long currentAdminId){
        ResponseModel responseModel = new ResponseModel();
        Set<City> cityByCarAdminId = cityService.getCityByCarAdminId(currentAdminId);
        responseModel.setStatusCode(cityByCarAdminId!=null? HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        responseModel.setMessage(cityByCarAdminId!=null?"获取成功!":"获取失败!");
        responseModel.setData(cityByCarAdminId);
        return responseModel;
    }
}
