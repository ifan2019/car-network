package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author  陈丽
 * @date    2015-5-26
 */
@RestController
@RequestMapping("/car")
public class CarController {
    @Autowired
    private CarService carServiceImpl;
    /**
     * 获取地图信息
     */
    @GetMapping("/getMapInfo")
    public ResponseModel getMaoInfo(int carId){
        ResponseModel model=new ResponseModel();
        Map<String,Object> data=carServiceImpl.getMapInfo(carId);
        model.setStatusCode(data!=null? HttpStatusCode.OK :HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"获取成功":"获取失败");
        model.setData(data);
        return model;
    }

    @GetMapping("/getCarInfoNumberByCarType")
    public ResponseModel getCarInfoNumberByCarType(Long currentAdminId){
        ResponseModel model=new ResponseModel();
        Map<String,Object> data=carServiceImpl.getCarInfoNumberByCarType(currentAdminId);
        model.setMessage(data!=null?"获取成功":"获取失败");
        model.setStatusCode(data!=null?HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setData(data);
        return model;
    }
}
