package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.entity.monitor.CarInfo;
import com.xmmufan.carnetwork.service.GasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author 陈丽
 * @Date   2019/5/24
 *
 */
@RestController
public class GasController {
    @Autowired
    private GasService gasServiceImpl;

    /**
     * app_C通过车辆Id获取电器系统参数
     */
    @GetMapping("/app_C/getApp_CEletricParameters")
    public ResponseModel getAppCEletricParameters(int carId){
        ResponseModel model =new ResponseModel();
        Map<String,Object> data= gasServiceImpl.getAppCeletricparam(carId);
        model.setStatusCode(data!=null? HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"操作成功":"操作失败");
        model.setData(data);
        return model;
    }

    /**
     * Web_B通过车辆vin获取电气系统参数
     */
    @GetMapping("/web_B/getWeb_BEletricParameters")
    public ResponseModel getWebBEletricParameters(CarInfo carInfo){
        ResponseModel model=new ResponseModel();
        Map<String,Object> data=gasServiceImpl.getWebBeletricparam(carInfo);
        model.setStatusCode(data!=null?HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"操作成功":"VIN不存在");
        model.setData(data);
        return model;
    }

    /**
     * 电器系统及相关功能性系统
     * @return
     */
    @GetMapping("/getElectricSystem")
    public ResponseModel getElectricSystem(){
        ResponseModel model=new ResponseModel();
        HashMap<String,Object> data=gasServiceImpl.getEletricSystem();
        model.setStatusCode(data!=null?HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data!=null?"操作成功":"操作失败");
        model.setData(data);
        return model;
    }
}
