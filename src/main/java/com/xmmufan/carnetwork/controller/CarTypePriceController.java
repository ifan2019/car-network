package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.entity.monitor.CarTypePrice;
import com.xmmufan.carnetwork.entity.vo.CarTypePriceVo;
import com.xmmufan.carnetwork.service.CarTypePriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/9/28
 */
@RestController
@RequestMapping("/carTypePrice")
public class CarTypePriceController {
    @Autowired
    private CarTypePriceService carTypePriceService;

    /**
     * 添加和修改车辆类型
     * @param carTypePriceVo carTypePriceVo
     * @return ResponseModel;
     */
    @PostMapping("/addOrUpdateCarTypePrice")
    public ResponseModel addOrUpdateCarTypePrice(@RequestBody CarTypePriceVo carTypePriceVo){
        ResponseModel responseModel = new ResponseModel();
        boolean b = carTypePriceService.addOrUpdateCarTypePrice(carTypePriceVo);
        responseModel.setStatusCode(b? HttpStatusCode.OK :HttpStatusCode.INTERNAL_SERVER_ERROR);
        responseModel.setMessage(b? "添加/修改成功":"添加/修改失败!");
        responseModel.setData(b);
        return responseModel;
    }

    /**
     * 车辆管理员获取所有的车辆类型列表
     * @param currentAdminId
     * @return
     */
    @GetMapping("/getCarTypePriceList")
    public ResponseModel getCarTypePriceList(Long currentAdminId){
        ResponseModel model = new ResponseModel();
        Set<CarTypePriceVo> carTypePriceVos = carTypePriceService.getCarTypePriceList(currentAdminId);
        model.setStatusCode(carTypePriceVos!=null?HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(carTypePriceVos!=null?"获取成功":"获取失败");
        model.setData(carTypePriceVos);
        return model;
    }

    @PostMapping("/deleteById")
    public ResponseModel deleteCarTypePriceById(Long id){
        ResponseModel model = new ResponseModel();
        boolean result = carTypePriceService.deleteById(id);
        model.setStatusCode(result?HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(result?"获取成功":"获取失败");
        model.setData(result);
        return model;
    }

}
