package com.xmmufan.carnetwork.controller;
import com.xmmufan.carnetwork.constant.exception.AccountException;
import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.HostUnauthorizedException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 异常处理类，发生异常会执行相对应的方法
 * @author Mr ifan/詹奕凡
 * @version 1.0
 * @date 2019/5/19
 */
@ControllerAdvice
public class BaseRestController {
    /**
     * shiro抛出未授权或授权异常时，返回未授权状态码403
     * @return com.xmmufan.project.constant.http.ResponseModel
     * @author Mr ifan
     * @date 2019/5/19
     */
    @ExceptionHandler({UnauthenticatedException.class, AuthenticationException.class, HostUnauthorizedException.class})
    protected ResponseModel authenticationException() {
        ResponseModel model = new ResponseModel();
        model.setStatusCode(HttpStatusCode.UNAUTHORIZED);
        model.setMessage("未登录不允许访问该地址");
        return model;

    }

    /**
     * shiro抛出权限异常时，返回授权不足状态码403
     * @return com.xmmufan.project.constant.http.ResponseModel
     * @author Mr.ifan
     * @date 2019/5/19
     */
    @ExceptionHandler({UnauthorizedException.class})
    protected ResponseModel permissionAuthenticationException(){
        ResponseModel model = new ResponseModel();
        model.setStatusCode(HttpStatusCode.FORBIDDEN);
        model.setMessage("用户权限不足，无法访问该地址");
        return model;
    }
    @ExceptionHandler({AuthorizationException.class})
    protected  ResponseModel permission(){
        ResponseModel model = new ResponseModel();
        model.setStatusCode(HttpStatusCode.FORBIDDEN);
        model.setMessage("用户权限不足，不能执行此操作");
        return model;
    }

    /**
     * 捕获密码加密时的异常，返回异常信息
     * @return com.xmmufan.project.constant.http.ResponseModel
     * @author Mr.ifan
     * @date 2019/5/19
     */
    @ExceptionHandler({AccountException.class})
    protected ResponseModel accountException(){
        ResponseModel model = new ResponseModel();
        model.setStatusCode(HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage("传入参数不符合条件");
        return model;
    }

}
