package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.entity.monitor.Order;
import com.xmmufan.carnetwork.service.UserCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/userC")
public class UserCController {

    @Autowired
    private UserCService userCService;

    @PostMapping("/login")
    public ResponseModel userCLogin(@RequestParam("telephone") String telephone,@RequestParam("code") String code){
        ResponseModel responseModel = new ResponseModel();
        Order userRentInfo = userCService.getUserRentInfo(telephone);
        HashMap<String, Integer> map = new HashMap<>(0);
        if (userRentInfo!=null) {
            map.put("status",1);
        }else{
            map.put("status",0);
        }
        responseModel.setData(map);
        return responseModel;
    }
}
