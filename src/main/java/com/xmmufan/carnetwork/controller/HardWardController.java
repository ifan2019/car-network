package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.service.HardWareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author ifan
 * @date 2019/7/29
 * @version 1.0
 */
@RestController
@RequestMapping("/hardWard")
public class HardWardController {

    @Autowired
    private HardWareService hardWareService;

    @PostMapping("/control")
    public ResponseModel control(String address,String commandStatus,String carVin){
        ResponseModel model=new ResponseModel();

        int data=hardWareService.control(address,"0040",commandStatus,carVin);
        model.setStatusCode(data!=0? HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        if(data == 0){
            model.setMessage("操作成功!");
        }else if(data == -2){
            model.setMessage("硬件响应超时!");
        } else{
            model.setMessage("操作失败!");
        }
        model.setData(data);
        return model;
    }

    @GetMapping("/query")
    public ResponseModel query(String startAddress,String endAddress,String commandType,String data,String carVin){
        ResponseModel responseModel = new ResponseModel();
        List<Map> query = hardWareService.query(startAddress, endAddress, commandType, data, carVin);
        responseModel.setStatusCode(query!=null?HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        responseModel.setMessage(query!=null?"查询成功!":"查询失败!请检查硬件");
        responseModel.setData(query);
        return responseModel;
    }

    public ResponseModel config(String address,String data,String carVin){
        ResponseModel model = new ResponseModel();
        int flag = hardWareService.config(address,data,carVin);
        model.setStatusCode(flag!=0? HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        if(flag == 0){
            model.setMessage("操作成功!");
        }else if(flag == -1){
            model.setMessage("操作失败!");
        } else{
            model.setMessage("硬件响应超时!");
        }
        model.setData(data);
        return model;
    }

    @GetMapping("/getAlarm")
    public ResponseModel getAlarm(String carVin){
        ResponseModel responseModel = new ResponseModel();
//        List<Map<String,String>> result = hardWareService.getAlarm(carVin);
//        responseModel.setStatusCode(result!=null?HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
//        responseModel.setMessage(result!=null?"获取成功!":"获取失败!");
//        responseModel.setData(result);
        return responseModel;
    }

    public ResponseModel getPageData(String carVin){
        Map<String, Object> pageData = hardWareService.getPageData(carVin);
        System.out.println(pageData);
        return new ResponseModel();
    }



}
