package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.aspect.EncryptedPasswordAndSalt;
import com.xmmufan.carnetwork.configuration.permission.CustomizedToken;
import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.entity.monitor.UserB;
import com.xmmufan.carnetwork.entity.vo.UserBInfoVo;
import com.xmmufan.carnetwork.service.UserBService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Mr.ifan/詹奕凡
 * @date 2019/6/13
 * @version 1.0
 */
@RequestMapping("/userB")
@RestController
public class UserBController {

    @Autowired
    private UserBService userBService;


    @PostMapping("/login")
    public ResponseModel userLogin(UserB userB){
        System.out.println("userB=" + userB);
        Subject subject = SecurityUtils.getSubject();
        try{
            if(!subject.isAuthenticated()){
                subject.login(new CustomizedToken(userB.getUserName(),userB.getPassword(),"UserB"));
            }
        }catch(LockedAccountException e){
            return new ResponseModel(HttpStatusCode.OK,"用户被锁定",null);
        }catch (AuthenticationException e){
            return new ResponseModel(HttpStatusCode.OK,"用户名密码错误",null);
        } catch(Exception e){
            e.printStackTrace();
            return new ResponseModel(HttpStatusCode.OK,"登录失败",null);
        }
        UserBInfoVo userBInfoVo = new UserBInfoVo(userBService.findByUsername(userB.getUserName()));
        return new ResponseModel(HttpStatusCode.OK,"登录成功",userBInfoVo);
    }

    @EncryptedPasswordAndSalt
    @PostMapping("/register")
    public String userRegister(UserB userB){
        System.out.println(userB);
        return "success";
    }

    /**
     * 获取所有房车制造商信息
     * @return
     */
    @GetMapping("/getAllUserB")
    public ResponseModel getAllUserB(){
        ResponseModel model = new ResponseModel();
        List<UserBInfoVo> userBList = userBService.getAllUserB();
        model.setStatusCode(userBList!=null?HttpStatusCode.OK:HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(userBList!=null?"获取成功":"获取失败");
        model.setData(userBList);
        return model;
    }
}
