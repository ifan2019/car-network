package com.xmmufan.carnetwork.controller;
import com.xmmufan.carnetwork.aspect.EncryptedPasswordAndSalt;
import com.xmmufan.carnetwork.configuration.permission.CustomizedToken;
import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.entity.vo.UserInfoVo;
import com.xmmufan.carnetwork.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户表相关的处理器方法
 * @author Mr ifan/詹奕凡
 * @version 1.0
 * @date 2019/5/19
 */
@RestController
public class UserController {

    @Autowired
    private UserService userServiceImpl;


//    @GetMapping("/get/{id}")
//    public User get(@PathVariable int id){
//        return userService.get(id);
//    }


    @GetMapping("/user/hello")
    public ResponseModel hello(){
        return new ResponseModel(HttpStatusCode.OK,"hello, project",null);
    }


//    @RequiresPermissions("user:read")
    @GetMapping("/list")
    public ResponseModel list(int offset, int limit){
        ResponseModel model = new ResponseModel();
//        List<UserInfoVo> list = userServiceImpl.list(offset, limit);
//        model.setStatusCode(list!=null? HttpStatusCode.OK :HttpStatusCode.INTERNAL_SERVER_ERROR);
//        model.setMessage(list!=null?"获取成功":"获取失败");
//        model.setData(list);
        return model;
    }

//    @RequiresPermissions("user:add")
//    @PostMapping("/user/add")
//    @EncryptedPasswordAndSalt
//    public User addUser(User user){
//        System.out.println(user);
//        return user;
//    }

//    @PostMapping("/user/login")
//    public ResponseModel userLogin(User user){
//        Subject subject = SecurityUtils.getSubject();
//        try{
//            if(!subject.isAuthenticated()){
//                subject.login(new CustomizedToken(user.getUsername(),user.getPassword(),"User"));
//            }
//        }catch(LockedAccountException e){
//            return new ResponseModel(HttpStatusCode.OK,"用户被锁定",null);
//        }catch (AuthenticationException e){
//            return new ResponseModel(HttpStatusCode.OK,"用户名密码错误",null);
//        } catch(Exception e){
//            e.printStackTrace();
//            return new ResponseModel(HttpStatusCode.OK,"登录失败",null);
//        }
//        return new ResponseModel(HttpStatusCode.OK,"登录成功",null);
//    }
}
