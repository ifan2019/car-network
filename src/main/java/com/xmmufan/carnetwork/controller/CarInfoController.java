package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.entity.monitor.CarInfo;
import com.xmmufan.carnetwork.entity.monitor.UserB;
import com.xmmufan.carnetwork.entity.vo.CarInfoVo;
import com.xmmufan.carnetwork.service.CarInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * carInfo相关的处理器方法
 *
 * @author 陈丽
 * @version 1.0
 * @date 2019/5/23
 */
@RestController
@RequestMapping("/carInfo")
public class CarInfoController {

    @Autowired
    private CarInfoService carInfoServiceImpl;

    /**
     * 获取车辆基本信息
     */
    @GetMapping("/list")
    @ResponseBody
    public ResponseModel getCarInfo(UserB userB) {
        ResponseModel model = new ResponseModel();
        List<Map> carInfoList = carInfoServiceImpl.getCarInfo(userB);
        model.setStatusCode(carInfoList != null ? HttpStatusCode.OK : HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(carInfoList != null ? "获取成功" : "获取失败");
        model.setData(carInfoList);
        return model;
    }

    @GetMapping("/getCarInfoById")
    public ResponseModel getCarInfoById(Long id) {
        ResponseModel responseModel = new ResponseModel();
        CarInfoVo carInfo = carInfoServiceImpl.getCarInfoById(id);
        responseModel.setStatusCode(carInfo != null ? HttpStatusCode.OK : HttpStatusCode.INTERNAL_SERVER_ERROR);
        responseModel.setMessage(carInfo != null ? "获取成功" : "获取失败");
        responseModel.setData(carInfo);
        return responseModel;
    }

    /**
     * 获取车辆基本信息列表
     *
     * @return
     */
    @GetMapping("/listCarInfo")
    public ResponseModel getCarInfoList(Long currentAdminId) {
        ResponseModel model = new ResponseModel();
        Map<String, Object> carInfoList = carInfoServiceImpl.getCarInfoList(currentAdminId);
        model.setStatusCode(carInfoList != null ? HttpStatusCode.OK : HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(carInfoList != null ? "获取成功" : "获取失败");
        model.setData(carInfoList);
        return model;
    }

    /**
     * 车辆基本信息录入
     *
     * @return
     */
    @PostMapping("/addCarInfo")
    public ResponseModel addCarInfo(CarInfo carInfo,Long cityId,Long carTypePriceId, Long currentAdminId) {
        ResponseModel model = new ResponseModel();
        int result = carInfoServiceImpl.addCarInfo(carInfo, cityId,carTypePriceId, currentAdminId);
        try {
            if (result == 1) {
                model.setMessage("vin已存在");

            } else if (result == 2) {
                model.setMessage("车牌已存在");
            } else if (result == 3) {
                model.setMessage("vin和车牌都已存在");
            } else if (result == 4) {
                model.setMessage("插入成功");
            } else {
                model.setMessage("操作失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.setStatusCode(result == 4 ? HttpStatusCode.OK : HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setData(result);
        return model;
    }

    /**
     * 根据车辆Message查询车辆
     *
     * @param id,info
     * @return
     */
    @GetMapping("getCarInfoByCondition")
    public ResponseModel getCarInfoByVin(Long id, String message) {
        ResponseModel model = new ResponseModel();
        Map<String, Object> data = carInfoServiceImpl.getCarInfoByCondition(id, message);
        model.setStatusCode(data != null ? HttpStatusCode.OK : HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(data != null ? "获取成功" : "获取失败");
        model.setData(data);
        return model;
    }

    /**
     * 删除车辆信息
     *
     * @param carInfo
     * @return
     */
    @PostMapping("/deleteCarInfo")
    public ResponseModel deleteCarInfo(CarInfo carInfo) {
        ResponseModel model = new ResponseModel();
        boolean b = carInfoServiceImpl.deleteCarIfo(carInfo);
        model.setStatusCode(b ? HttpStatusCode.OK : HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(b ? "获取成功" : "获取失败");
        model.setData(b);
        return model;
    }

    /**
     * 修改车辆信息
     *
     * @param carInfo
     * @return
     */
    @PostMapping("/updateCarInfo")
    public ResponseModel uodateCarInfo(CarInfo carInfo,Long cityId,Long carTypePriceId, Long currentAdminId) {
        ResponseModel model = new ResponseModel();
        boolean b = carInfoServiceImpl.updateCarInfo(carInfo, cityId,carTypePriceId, currentAdminId);
        model.setStatusCode(b ? HttpStatusCode.OK : HttpStatusCode.INTERNAL_SERVER_ERROR);
        model.setMessage(b ? "获取成功" : "获取失败");
        model.setData(b);
        return model;
    }

    /**
     * 通过message查询
     *
     * @param id
     * @param message
     * @return
     */
    @GetMapping("/getWeb_bCarInfoByCondition")
    public ResponseModel getWeb_bCarInfoByCondition(int id, String message) {
        ResponseModel model = new ResponseModel();
        List<Map> map = carInfoServiceImpl.getWeb_bCarInfoByCondition(id, message);
        model.setMessage("获取成功!");
        model.setStatusCode(HttpStatusCode.OK);
        model.setData(map);
        return model;
    }

    @RequestMapping("/getCountByChassis")
    public ResponseModel getCountByChassis(Long currentAdminId) {
        ResponseModel model = new ResponseModel();
        List<Map<String, String>> data = carInfoServiceImpl.getCountByChassis(currentAdminId);
        model.setMessage(data != null ? "获取成功" : "获取失败");
        model.setData(data);
        model.setStatusCode(data != null ? HttpStatusCode.OK : HttpStatusCode.INTERNAL_SERVER_ERROR);
        return model;
    }

    @RequestMapping("/getByCountByLeaseStatus")
    public ResponseModel getByCountByLeaseStatus(Long currentAdminId) {
        ResponseModel model = new ResponseModel();
        Map<String, Object> data = carInfoServiceImpl.getByCountByLeaseStatus(currentAdminId);
        model.setMessage(data != null ? "获取成功" : "获取失败");
        model.setData(data);
        model.setStatusCode(data != null ? HttpStatusCode.OK : HttpStatusCode.INTERNAL_SERVER_ERROR);
        return model;
    }
}

