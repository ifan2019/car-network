package com.xmmufan.carnetwork.controller;

import com.xmmufan.carnetwork.configuration.permission.CustomizedToken;
import com.xmmufan.carnetwork.constant.http.HttpStatusCode;
import com.xmmufan.carnetwork.constant.http.ResponseModel;
import com.xmmufan.carnetwork.entity.monitor.Admin;
import com.xmmufan.carnetwork.entity.vo.CarAdminVo;
import com.xmmufan.carnetwork.service.CarAdminService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Mr.ifan/詹奕凡
 * @date 2019/6/19
 * @version 1.0
 */
@RestController
@RequestMapping("/admin")
public class CarAdminController {

    @Autowired
    @Lazy
    private CarAdminService carAdminService;

    @PostMapping("/login")
    public ResponseModel userLogin(Admin admin){
        Subject subject = SecurityUtils.getSubject();
        try{
            if(!subject.isAuthenticated()){
                subject.login(new CustomizedToken(admin.getUserName(),admin.getPassword(),"Admin"));
            }
        }catch(LockedAccountException e){
            return new ResponseModel(HttpStatusCode.OK,"用户被锁定",null);
        }catch (AuthenticationException e){
            return new ResponseModel(HttpStatusCode.OK,"用户名密码错误",null);
        } catch(Exception e){
            e.printStackTrace();
            return new ResponseModel(HttpStatusCode.OK,"登录失败",null);
        }
        CarAdminVo carAdminVo = new CarAdminVo(carAdminService.findByUsername(admin.getUserName()));
        return new ResponseModel(HttpStatusCode.OK,"登录成功",carAdminVo);
    }
}
