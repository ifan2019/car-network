package com.xmmufan.carnetwork.constant.exception;

/**
 * @author Mr ifan/詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 *     自定义用户账户信息异常类
 *     调用情景：用户名或密码缺失
 * </p>
 */
public class AccountInfoMissingException extends Exception {

    public AccountInfoMissingException(String message) {
        super(message);
    }
}
