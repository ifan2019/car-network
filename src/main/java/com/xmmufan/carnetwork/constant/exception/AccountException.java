package com.xmmufan.carnetwork.constant.exception;

/**
 * @author Mr ifan/詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 *     自定义账号异常类
 * </p>
 */
public class AccountException extends Exception{

    public AccountException(String message) {
        super(message);
    }
}
