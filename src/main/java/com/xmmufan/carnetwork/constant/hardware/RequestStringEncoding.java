package com.xmmufan.carnetwork.constant.hardware;


import com.xmmufan.carnetwork.util.ParseHelper;

import java.util.ArrayList;
import java.util.List;

public class RequestStringEncoding {
    //fe00　1f00　1202　13a3　3e04　ea00　0000　005d　1062　f200　5005　0003　0010　6b00　0300　107a　31fa　fe
    public static void main(String[] args) {
        judgeOperationType("0040","000300106b","000300107a","0102");
    }
    public static void judgeOperationType(String command,String startAddress,String endAddress,String data){
        if(command.equals("0030")){
            configRequest(command,startAddress,data);
        }
        else if(command.equals("0040")){
            controlRequest(command,startAddress,data);
        }
        else if(command.equals("0050")){
            queryRequest(command,startAddress,endAddress);
        }
        else if(command.equals("0080")){
            ipSettingRequest(command);
        }else if(command.equals("00A2")){
            queryAttributeTreeRequest(command,startAddress,endAddress);
        }
    }

    private static void queryAttributeTreeRequest(String command, String startAddress, String endAddress) {
    }

    private static String ipSettingRequest(String command) {
        StringBuilder all=new StringBuilder("");
       String  random=Integer.toHexString((int)(Math.random()*16777215));
        all.insert(0,"fe");
        all.insert(2,"xxxx");
        all.insert(6,"0012");
        while(random.length()<6){
            random="0"+random;
        }
        System.out.println("random"+random);
        all.insert(10,"00"+random);
        all.insert(18,"04ea");
        all.insert(22,"00000000");
        all.insert(30,"5d1062f2");
        all.insert(38,command);
        all.insert(42,"19216801");
        all.insert(50,"8080");
        String a=all.substring(6,54);
        System.out.println("aaa==="+a);
        List<String> strList = ParseHelper.getStrList(a, 2);
        System.out.println("strListSize==="+strList.size());
        List<Integer> crcList=new ArrayList<>();
        for (int i=0;i<strList.size();i++){
            crcList.add(Integer.parseInt(strList.get(i),16));
        }
        int [] arr = new int[]{};
        String crcData = CrcUtil.contextLoads(crcList.stream().mapToInt(Integer::valueOf).toArray());
        all.insert(54,crcData);
        String headLength=Integer.toHexString(strList.size());
        if(strList.size()+2<0x10){
            System.out.println("小于0x10");
            headLength="000"+Integer.toHexString(strList.size());
        }else if(strList.size()+2<0x100){
            System.out.println("小于0x100");
            headLength="00"+Integer.toHexString(strList.size()+2);

        }else if(strList.size()<0x1000){
            System.out.println("小于0x1000");
            headLength="0"+Integer.toHexString(strList.size()+2);
        }
        all.replace(2,6,headLength);
        all.insert(58,"fe");
        System.out.println("crc=="+crcData);
        System.out.println("all==="+all);
        List<String> result=ParseHelper.getStrList(all.toString(),4);
        System.out.println("result===="+result);
        System.out.println("crcList==="+crcList);
        System.out.println("aaa=="+a);
        System.out.println("abc==="+all);
        System.out.println("sdfa==="+ String.join(" ",result));
        return String.join(" ",result);
    }

    private static String configRequest(String command, String startAddress, String data) {
        StringBuilder all=new StringBuilder("");
        all.insert(0,"fe");
        all.insert(2,"xxxx");
        all.insert(6,"0012");
        all.insert(10,"00"+Integer.toHexString((int)(Math.random()*16777215)));
        all.insert(18,"04ea");
        all.insert(22,"00000000");
        all.insert(30,"5d1062f2");
        all.insert(38,command);
        all.insert(42,"05");
        all.insert(44,startAddress);
        List<String> datas=ParseHelper.getStrList(data,2);
        String length = Integer.toHexString(datas.size());
        if(datas.size()<0x10){
            length = "0"+Integer.toHexString(datas.size());
        }
        all.insert(54,length);
        all.insert(56,data);
        String a=all.substring(6,56+data.length());
        System.out.println("aaa==="+a);
        List<String> strList = ParseHelper.getStrList(a, 2);
        System.out.println("strListSize==="+strList.size());
        List<Integer> crcList=new ArrayList<>();
        for (int i=0;i<strList.size();i++){
            crcList.add(Integer.parseInt(strList.get(i),16));
        }
        int [] arr = new int[]{};
        String crcData = CrcUtil.contextLoads(crcList.stream().mapToInt(Integer::valueOf).toArray());
        all.insert(56+data.length(),crcData);
        String headLength=Integer.toHexString(strList.size());
        if(strList.size()+2<0x10){
            System.out.println("小于0x10");
            headLength="000"+Integer.toHexString(strList.size());
        }else if(strList.size()+2<0x100){
            System.out.println("小于0x100");
            headLength="00"+Integer.toHexString(strList.size()+2);

        }else if(strList.size()<0x1000){
            System.out.println("小于0x1000");
            headLength="0"+Integer.toHexString(strList.size()+2);
        }
        all.replace(2,6,headLength);
        all.insert(56+data.length()+4,"fe");
        System.out.println("crc=="+crcData);
        System.out.println("datas==="+datas);
        System.out.println("all==="+all);
        List<String> result=ParseHelper.getStrList(all.toString(),4);
        System.out.println("result===="+result);
        System.out.println("crcList==="+crcList);
        System.out.println("aaa=="+a);
        System.out.println("abc==="+all);
        System.out.println("sdfa==="+ String.join(" ",result));
        return String.join(" ",result);

    }

    private static void realTimeDataResponse(String command) {
    }

    private static String controlRequest(String command, String startAddress,String data) {
        StringBuilder all=new StringBuilder("");
        all.insert(0,"fe");
        all.insert(2,"xxxx");
        all.insert(6,"0012");
        all.insert(10,"00"+Integer.toHexString((int)(Math.random()*16777215)));
        all.insert(18,"04ea");
        all.insert(22,"00000000");
        all.insert(30,"5d1062f2");
        all.insert(38,command);
        all.insert(42,"05");
        all.insert(44,startAddress);
        List<String> datas=ParseHelper.getStrList(data,2);
        String length = Integer.toHexString(datas.size());
        if(datas.size()<0x10){
            length = "0"+Integer.toHexString(datas.size());
        }
        all.insert(54,length);
        all.insert(56,data);
        String a=all.substring(6,56+data.length());
        System.out.println("aaa==="+a);
        List<String> strList = ParseHelper.getStrList(a, 2);
        System.out.println("strListSize==="+strList.size());
        List<Integer> crcList=new ArrayList<>();
        for (int i=0;i<strList.size();i++){
            crcList.add(Integer.parseInt(strList.get(i),16));
        }
        int [] arr = new int[]{};
        String crcData = CrcUtil.contextLoads(crcList.stream().mapToInt(Integer::valueOf).toArray());
        all.insert(56+data.length(),crcData);
        String headLength=Integer.toHexString(strList.size());
        if(strList.size()+2<0x10){
            System.out.println("小于0x10");
            headLength="000"+Integer.toHexString(strList.size());
        }else if(strList.size()+2<0x100){
            System.out.println("小于0x100");
            headLength="00"+Integer.toHexString(strList.size()+2);

        }else if(strList.size()<0x1000){
            System.out.println("小于0x1000");
            headLength="0"+Integer.toHexString(strList.size()+2);
        }
        all.replace(2,6,headLength);
        all.insert(56+data.length()+4,"fe");
        System.out.println("crc=="+crcData);
        System.out.println("datas==="+datas);
        System.out.println("all==="+all);
        List<String> result=ParseHelper.getStrList(all.toString(),4);
        System.out.println("result===="+result);
        System.out.println("crcList==="+crcList);
        System.out.println("aaa=="+a);
        System.out.println("abc==="+all);
        System.out.println("sdfa==="+ String.join(" ",result));
        return String.join(" ",result);

    }
    //fe00　1f00　1202　13a3　3e04　ea00　0000　005d　1062　f200　5005　0003　0010　6b00　0300　107a　31fa　fe

    private static String queryRequest(String command,String startAddress,String endAddress) {
        StringBuilder all=new StringBuilder("");
        all.insert(0,"fe");
        all.insert(2,"001f");
        all.insert(6,"0012");
        all.insert(10,"00"+Integer.toHexString((int)(Math.random()*16777215)));
        all.insert(18,"04ea");
        all.insert(22,"00000000");
        all.insert(30,"5d1062f2");
        all.insert(38,command);
        all.insert(42,"05");
        all.insert(44,startAddress);
        all.insert(54,endAddress);
        String a=all.substring(6,64);
        List<String> strList = ParseHelper.getStrList(a, 2);
        List<Integer> crcList=new ArrayList<>();
        for (int i=0;i<strList.size();i++){
            crcList.add(Integer.parseInt(strList.get(i),16));
        }
        int [] arr = new int[]{};
        String crcData = CrcUtil.contextLoads(crcList.stream().mapToInt(Integer::valueOf).toArray());
        all.insert(64,crcData);
        all.insert(68,"fe");
        List<String> result=ParseHelper.getStrList(all.toString(),4);
        System.out.println("result===="+result);
        System.out.println("crcList==="+crcList);
        System.out.println("aaa=="+a);
        System.out.println("abc==="+all);
        System.out.println("sdfa==="+ String.join(" ",result));
        return String.join(" ",result);
    }
}
