package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

public class BmsFF03 {
    private static Map<String, Map<String,String>> bmsAddress=new HashMap<>(0);
    private static Map<String,Map<String,String>> bmsStatusAddress=new HashMap<>(0);
    private static Map<String,String> data;

    static {
        data=new HashMap<>(0);
        data.put("content","系统类型");
        data.put("length","4");
        bmsAddress.put("0000",data);

    }
}
