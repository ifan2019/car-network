package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

/**
 *针对00a4命令解析结果的存储
 */
public class AttributeMap {
    private static Map<String, Map<String,String>> data=new HashMap<>(0);

    public static void add(String sequenceSign,Map<String,String> content){
        data.put(sequenceSign,content);
    }

    public static Map<String,String> get(String sequenceSign){
        return data.get(sequenceSign);
    }

    public static Map<String,Map<String,String>> getData(){
        return data;
    }
}
