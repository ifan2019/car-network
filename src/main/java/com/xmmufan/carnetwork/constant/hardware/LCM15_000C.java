package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

public class LCM15_000C {
    private static Map<String,Map<String,String>> lcm15Address=new HashMap<String, Map<String, String>>(0);
    private static Map<String,Map<String,String>> lcm15StatusAddress=new HashMap<String, Map<String, String>>(0);
    private static Map<String,Map<String,String>> lcm15ControlAddress=new HashMap<>(0);
    private static Map<String,String> data;
    static{
        data=new HashMap<String, String>(0);
        data.put("content","序列号");
        data.put("length","30");
        lcm15Address.put("0000",data);

        data=new HashMap<String, String>(0);
        data.put("content","制造商");
        data.put("length","30");
        lcm15Address.put("0001",data);

        data=new HashMap<String, String>(0);
        data.put("content","型号");
        data.put("length","30");
        lcm15Address.put("0002",data);

        data=new HashMap<String, String>(0);
        data.put("content","TBB序列号");
        data.put("length","30");
        lcm15Address.put("0003",data);

        data=new HashMap<String, String>(0);
        data.put("content","TBB型号");
        data.put("length","15");
        lcm15Address.put("0004",data);

        data=new HashMap<String, String>(0);
        data.put("content","CTRL firmware版本");
        data.put("length","10");
        lcm15Address.put("0005",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","10");
        lcm15Address.put("0006",data);

        data=new HashMap<String, String>(0);
        data.put("content","UNIX时间戳");
        data.put("length","4");
        lcm15Address.put("0007",data);

        data=new HashMap<String, String>(0);
        data.put("content","协议版本");
        data.put("length","2");
        lcm15Address.put("0008",data);

        data=new HashMap<String, String>(0);
        data.put("content","RS485通讯地址");
        data.put("length","2");
        lcm15Address.put("0009",data);

        data=new HashMap<String, String>(0);
        data.put("content","校验位");
        data.put("length","2");
        lcm15Address.put("000A",data);

        data=new HashMap<String, String>(0);
        data.put("content","485波特率设置");
        data.put("length","2");
        lcm15Address.put("000B",data);

        //LCM15设备状态地址
        data=new HashMap<String, String>(0);
        data.put("content","通道0（总电源状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0100",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道1(逆变器干接点继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0101",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道2(干接点:U-SW1继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0102",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道3(干接点:U-SW2继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0103",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道4(干接点:U-SW3继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0104",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道5(干接点:U-SW4继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0105",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道6(干接点:U-SW5继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0106",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道7(干接点:U-SW6继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0107",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道8(Ceiling Light状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0108",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道9(Bedroom Light状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0109",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道10(Dining Light状态)");
        data.put("length","2");
        lcm15StatusAddress.put("010A",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道11(Outside Light状态)");
        data.put("length","2");
        lcm15StatusAddress.put("010B",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道12(TV状态)");
        data.put("length","2");
        lcm15StatusAddress.put("010C",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道13(Heater状态)");
        data.put("length","2");
        lcm15StatusAddress.put("010D",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道14(Switch A状态)");
        data.put("length","2");
        lcm15StatusAddress.put("010E",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道15(Switch B状态)");
        data.put("length","2");
        lcm15StatusAddress.put("010F",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道16(Switch C状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0110",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道17(Pump状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0111",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道18(负载11继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0112",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道19(负载12继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0113",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道20(负载13继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0114",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道21(负载14继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0115",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道22(负载15继电器状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0116",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道23(预留) ");
        data.put("length","2");
        lcm15StatusAddress.put("0117",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道24 (预留)");
        data.put("length","2");
        lcm15StatusAddress.put("0118",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道25 (预留)");
        data.put("length","2");
        lcm15StatusAddress.put("0119",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道26 (预留)");
        data.put("length","2");
        lcm15StatusAddress.put("011A",data);

        data=new HashMap<String, String>(0);
        data.put("content","夜间模式(Night Mode状态)");
        data.put("length","2");
        lcm15StatusAddress.put("011B",data);

        data=new HashMap<String, String>(0);
        data.put("content","Inverter灯 Change灯 Fault灯 Grid灯 状态");
        data.put("length","2");
        lcm15StatusAddress.put("011C",data);

        data=new HashMap<String, String>(0);
        data.put("content","低电压");
        data.put("length","2");
        lcm15StatusAddress.put("011D",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserve");
        data.put("length","2");
        lcm15StatusAddress.put("011E",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserve");
        data.put("length","2");
        lcm15StatusAddress.put("011F",data);

        data=new HashMap<String, String>(0);
        data.put("content","系统供电电压(预留)");
        data.put("length","2");
        lcm15StatusAddress.put("0120",data);

        data=new HashMap<String, String>(0);
        data.put("content","F07（预留）");
        data.put("length","2");
        lcm15StatusAddress.put("0121",data);

        data=new HashMap<String, String>(0);
        data.put("content","F08（预留）");
        data.put("length","2");
        lcm15StatusAddress.put("0122",data);

        data=new HashMap<String, String>(0);
        data.put("content","F09（预留）");
        data.put("length","2");
        lcm15StatusAddress.put("0123",data);

        data=new HashMap<String, String>(0);
        data.put("content","F10状态（预留）");
        data.put("length","2");
        lcm15StatusAddress.put("0124",data);

        data=new HashMap<String, String>(0);
        data.put("content","拨码开关状态（预留）");
        data.put("length","2");
        lcm15StatusAddress.put("0125",data);

        data=new HashMap<String, String>(0);
        data.put("content","Temp_AD1（室外温度）");
        data.put("length","2");
        lcm15StatusAddress.put("0126",data);

        data=new HashMap<String, String>(0);
        data.put("content","Temp_AD2（室内温度)");
        data.put("length","2");
        lcm15StatusAddress.put("0127",data);
/*

        data=new HashMap<String, String>(0);
        data.put("content","通道0（总电源状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0128",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道0（总电源状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0129",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道0（总电源状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0130",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道0（总电源状态)");
        data.put("length","2");
        lcm15StatusAddress.put("0131",data);*/

        data=new HashMap<String, String>(0);
        data.put("content","AD通道6（预留）");
        data.put("length","2");
        lcm15StatusAddress.put("012B",data);

        data=new HashMap<String, String>(0);
        data.put("content","AD通道7（温度电阻，模拟量输入2）");
        data.put("length","2");
        lcm15StatusAddress.put("012C",data);

        data=new HashMap<String, String>(0);
        data.put("content","AD通道8（温度电阻，模拟量输入1）");
        data.put("length","2");
        lcm15StatusAddress.put("012D",data);

        //LCM15支持的控制
        data=new HashMap<String, String>(0);
        data.put("content","通道0（总电源状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0100",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道1(逆变器干接点继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0101",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道2(干接点:U-SW1继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0102",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道3(干接点:U-SW2继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0103",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道4(干接点:U-SW3继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0104",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道5(干接点:U-SW4继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0105",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道6(干接点:U-SW5继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0106",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道7(干接点:U-SW6继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0107",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道8(Ceiling Light状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0108",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道9(Bedroom Light状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0109",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道10(Dining Light状态)");
        data.put("length","2");
        lcm15ControlAddress.put("010A",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道11(Outside Light状态)");
        data.put("length","2");
        lcm15ControlAddress.put("010B",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道12(TV状态)");
        data.put("length","2");
        lcm15ControlAddress.put("010C",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道13(Heater状态)");
        data.put("length","2");
        lcm15ControlAddress.put("010D",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道14(Switch A状态)");
        data.put("length","2");
        lcm15ControlAddress.put("010E",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道15(Switch B状态)");
        data.put("length","2");
        lcm15ControlAddress.put("010F",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道16(Switch C状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0110",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道17(Pump状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0111",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道18(负载11继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0112",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道19(负载12继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0113",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道20(负载13继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0114",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道21(负载14继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0115",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道22(负载15继电器状态)");
        data.put("length","2");
        lcm15ControlAddress.put("0116",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道23(预留) ");
        data.put("length","2");
        lcm15ControlAddress.put("0117",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道24 (预留)");
        data.put("length","2");
        lcm15ControlAddress.put("0118",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道25 (预留)");
        data.put("length","2");
        lcm15ControlAddress.put("0119",data);

        data=new HashMap<String, String>(0);
        data.put("content","通道26 (预留)");
        data.put("length","2");
        lcm15ControlAddress.put("011A",data);

        data=new HashMap<String, String>(0);
        data.put("content","夜间模式(预留)");
        data.put("length","2");
        lcm15ControlAddress.put("011B",data);

    }

    public static void main(String[] args) {
        System.out.println(getLcm15AddressName("0001"));
        System.out.println(getLcm15StatusAddressName("012C"));
    }

    public static Map<String,String> getLcm15AddressName(String name){
        return lcm15Address.get(name);
    }

    public static Map<String,String> getLcm15StatusAddressName(String name){
        return lcm15StatusAddress.get(name);
    }
}
