package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

/**
 * @author  陈丽
 * @date    2019-7-23
 */
public class Unit {
    private static Map<String, String> map = new HashMap<>(0);

    static {
        //电压单位
        map.put("0000000000000000", "1V");
        map.put("0000000000000001", "0.1V");
        map.put("0000000000000010", "0.01V");

        //电流单位
        map.put("1000000000010000", "1A");
        map.put("1000000000010001", "0.1A");
        map.put("1000000000010010", "0.01A");

        //频率单位
        map.put("0000000000100000", "1HZ");
        map.put("0000000000100001", "0.1HZ");

        //功率单位VA/W
        map.put("0000000000110000", "1VA");
        map.put("0000000000110001", "1W");
        map.put("0000000000110010", "10VA");
        map.put("0000000000110011", "10W");
        map.put("0000000000110100", "0.1VA");
        map.put("0000000000110101", "0.1W");
        map.put("0000000000110110", "100W");

        //温度单位
        map.put("0000000001000000", "1℃");
        map.put("0000000001000001", "0.1℃");

        //百分比
        map.put("0000000001010000", "1%");
        map.put("0000000001010001", "0.1%");

        //能量单位
        map.put("1000000001100000", "1WS");
        map.put("1000000001100001", "0.1WH");
        map.put("1000000001100010", "1WH");
        map.put("1000000001100011", "0.1KWH");
        map.put("1000000001100100", "1KWH");

        //电池容量单位
        map.put("0000000001110000", "1AH");

        //时间单位
        map.put("0000000010000000", "1s");
        map.put("0000000010000001", "1min");
        map.put("0000000010000010", "1H");
        map.put("0000000010000011", "1Day");
        map.put("0000000010000100", "1Month");
        map.put("0000000010000101", "1Year");
        map.put("0000000010000110", "1Week");
        map.put("0000000010000111", "0.1H");

        //字符串
        map.put("0000000011110001", "string");

        //无单位
        map.put("0111111111111111", "no unit");
    }


    public static String getUnitContent(String name){
        return map.get(name);
    }
}