package com.xmmufan.carnetwork.constant.hardware.alarm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/8/20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Alarm {
    private String description;
    private String type;
    private String sort;
    private Date time;
    private String equipmentName;

    public Alarm(String description,String type,String sort){
        this.description = description;
        this.type = type;
        this.sort = sort;
    }
}
