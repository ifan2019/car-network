package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

public class EquipmentSign {
    public static Map<String,String> equipmentSign = new HashMap<>(0);

    static{
        equipmentSign.put("0001","CCS");
        equipmentSign.put("0002","Cyber");
        equipmentSign.put("0003","COMBI");
        equipmentSign.put("0004","MEDU");
        equipmentSign.put("0005","SP Ctrl");
        equipmentSign.put("0006","SP LCD");
        equipmentSign.put("0007","Crystal");
        equipmentSign.put("0008","MP");
        equipmentSign.put("0009","LCM10");
        equipmentSign.put("000A","RS32");
        equipmentSign.put("000B","CMP");
        equipmentSign.put("000C","LCM15");
        equipmentSign.put("000D","MASTER");
        equipmentSign.put("000E","DX");
        equipmentSign.put("000F","DDX");
        equipmentSign.put("001B","Vision");
        equipmentSign.put("0030","BM");
        equipmentSign.put("0036","XCM16");
        equipmentSign.put("FF03","BMS");
        equipmentSign.put("FFFF","系统标识");
    }

    public static void main(String[] args) {
        System.out.println(getByName("0030"));
    }

    public static String getByName(String name){
        return equipmentSign.get(name);
    }
}
