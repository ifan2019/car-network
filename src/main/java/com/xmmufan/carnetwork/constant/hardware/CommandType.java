package com.xmmufan.carnetwork.constant.hardware;

public final class CommandType {

    public final static String CONNECTION_COMMAND_SEND = "0010";

    public final static String CONNECTION_COMMAND_REPLY = "0011";

    public final static String REAL_TIME_DATA_SEND = "0020";

    public final static String REAL_TIME__DATA_REPLY = "0021";

    public final static String CONFIG_INSTRUCTION_SEND = "0030";

    public final static String CONFIG_INSTRUCTION_REPLY = "0031";

    public final static String CONTROL_INSTRUCTION_SEND = "0040";

    public final static String CONTROL_INSTRUCTION_REPLY = "0041";

    public final static String SELECT_INSTRUCTION_SEND  = "0050";

    public final static String SELECT_INSTRUCTION_REPLY = "0051";

    public final static String RENEWAL_SEND = "0060";

    public final static String RENEWAL_REPLY = "0061";

    public final static String START_OR_FINISH_UPDATE_FIRMWARE_SEND = "0070";

    public final static String START_OR_FINISH_UPDATE_FIRMWARE_REPLY = "0071";

    public final static String FIRMWARE_UPDATE_SEND = "0072";

    public final static String FIRMWARE_UPDATE_REPLY = "0073";

    public final static String FIRMWARE_UPDATE_INFO_SELECT_SEND = "0074";

    public final static String FIRMWARE_UPDATE_INFO_SELECT_REPLY = "0075";

    public final static String IP_SET_COMMAND_SEND = "0080";

    public final static String IP_SET_COMMAND_REPLY = "0081";

    public final static String PAIR_COMMAND_SEND = "0090";

    public final static String PAIR_COMMAND_REPLY = "0091";

    public final static String SELECT_DATA_ATTRITUBE_SEND = "00A0";

    public final static String SELECT_DATA_ATTRITUBE_REPLY = "00A1";

    public final static String SELECT_DATA_ATTRITUBE_TREE_SEND = "00A2";

    public final static String SELECT_DATA_ATTRITUBE_TREE_REPLY = "00A3";

    public final static String DATA_ATTRITUBE_TREE_SEND = "00A4";

    public final static String DATA_ATTRITUBE_TREE_REPLY = "00A5";

    public final static String HEART_BEAT_PACKAGE_SEND = "00B0";

    public final static String HEART_BEAT_PACKAGE_REPLY = "00B1";
}
