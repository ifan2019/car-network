package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

public class Bm_0030 {
    private static Map<String, Map<String,String>> bmAddress=new HashMap<>(0);
    private static Map<String,Map<String,String>> bmStatusAddress=new HashMap<>(0);
    private static Map<String,String> data;

    static{
        data=new HashMap<String, String>(0);
        data.put("content","电池类型");
        data.put("length","2");
        bmAddress.put("0000",data);


        data=new HashMap<String, String>(0);
        data.put("content","电池容量");
        data.put("length","2");
        bmAddress.put("0001",data);


        data=new HashMap<String, String>(0);
        data.put("content","电池放电深度(DOD)");
        data.put("length","2");
        bmAddress.put("0002",data);


        data=new HashMap<String, String>(0);
        data.put("content","电池自然老化率");
        data.put("length","2");
        bmAddress.put("0003",data);


        data=new HashMap<String, String>(0);
        data.put("content","电池保护恢复量");
        data.put("length","2");
        bmAddress.put("0004",data);


        data=new HashMap<String, String>(0);
        data.put("content","蜂鸣器鸣叫使能");
        data.put("length","2");
        bmAddress.put("0005",data);


        data=new HashMap<String, String>(0);
        data.put("content","安装日期");
        data.put("length","4");
        bmAddress.put("0006",data);


        data=new HashMap<String, String>(0);
        data.put("content","安装日期");
        data.put("length","4");
        bmAddress.put("0007",data);

        data=new HashMap<String, String>(0);
        data.put("content","语言");
        data.put("length","2");
        bmAddress.put("0008",data);


        data=new HashMap<String, String>(0);
        data.put("content","RTC时间设置");
        data.put("length","8");
        bmAddress.put("0009",data);

        data=new HashMap<String, String>(0);
        data.put("content","RTC时间设置");
        data.put("length","8");
        bmAddress.put("000A",data);

        data=new HashMap<String, String>(0);
        data.put("content","RTC时间设置");
        data.put("length","8");
        bmAddress.put("000B",data);

        data=new HashMap<String, String>(0);
        data.put("content","RTC时间设置");
        data.put("length","8");
        bmAddress.put("000C",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池额定电压");
        data.put("length","2");
        bmAddress.put("000D",data);

        data=new HashMap<String, String>(0);
        data.put("content","单体电池数");
        data.put("length","2");
        bmAddress.put("000E",data);

        data=new HashMap<String, String>(0);
        data.put("content","单体电池不平衡高压保护值");
        data.put("length","2");
        bmAddress.put("000F",data);

        data=new HashMap<String, String>(0);
        data.put("content","单体电池高压保护值");
        data.put("length","2");
        bmAddress.put("0010",data);

        data=new HashMap<String, String>(0);
        data.put("content","单体电池低压保护值");
        data.put("length","2");
        bmAddress.put("0011",data);

        data=new HashMap<String, String>(0);
        data.put("content","RS485通讯地址");
        data.put("length","2");
        bmAddress.put("0012",data);

        data=new HashMap<String, String>(0);
        data.put("content","奇偶校验");
        data.put("length","2");
        bmAddress.put("0013",data);

        data=new HashMap<String, String>(0);
        data.put("content","波特率");
        data.put("length","2");
        bmAddress.put("0014",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("0015",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("0016",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("0017",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("0018",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("0019",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("001A",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("001B",data);


        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("001C",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("001C",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("001D",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("001E",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("001F",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("0020",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("0021",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("0022",data);

        data=new HashMap<String, String>(0);
        data.put("content","软件版本");
        data.put("length","30");
        bmAddress.put("0023",data);

        data=new HashMap<String, String>(0);
        data.put("content","BM安装线长度");
        data.put("length","2");
        bmAddress.put("0024",data);

        data=new HashMap<String, String>(0);
        data.put("content","恢复出厂设置");
        data.put("length","2");
        bmAddress.put("00FF",data);

        //BM设备状态地址
        data=new HashMap<String, String>(0);
        data.put("content","电池总电压");
        data.put("length","2");
        bmStatusAddress.put("0200",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池总电流");
        data.put("length","2");
        bmStatusAddress.put("0201",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池实时充电总能量");
        data.put("length","4");
        bmStatusAddress.put("0202",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池实时充电总能量");
        data.put("length","4");
        bmStatusAddress.put("0203",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池实时放电总能量");
        data.put("length","4");
        bmStatusAddress.put("0204",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池实时放电总能量");
        data.put("length","4");
        bmStatusAddress.put("0205",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池当前容量");
        data.put("length","2");
        bmStatusAddress.put("0206",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池 SOC");
        data.put("length","2");
        bmStatusAddress.put("0207",data);

        data=new HashMap<String, String>(0);
        data.put("content","剩余可用时间/所需充电时间");
        data.put("length","2");
        bmStatusAddress.put("0208",data);

        data=new HashMap<String, String>(0);
        data.put("content","充放电循环计数次数");
        data.put("length","2");
        bmStatusAddress.put("0209",data);

        data=new HashMap<String, String>(0);
        data.put("content","第1路单体锂电池电压");
        data.put("length","2");
        bmStatusAddress.put("020A",data);

        data=new HashMap<String, String>(0);
        data.put("content","第2路单体锂电池电压");
        data.put("length","2");
        bmStatusAddress.put("020B",data);

        data=new HashMap<String, String>(0);
        data.put("content","第3路单体锂电池电压");
        data.put("length","2");
        bmStatusAddress.put("020C",data);

        data=new HashMap<String, String>(0);
        data.put("content","第4路单体锂电池电压");
        data.put("length","2");
        bmStatusAddress.put("020D",data);

        data=new HashMap<String, String>(0);
        data.put("content","第5路单体锂电池电压");
        data.put("length","2");
        bmStatusAddress.put("020E",data);

        data=new HashMap<String, String>(0);
        data.put("content","第6路单体锂电池电压");
        data.put("length","2");
        bmStatusAddress.put("020F",data);

        data=new HashMap<String, String>(0);
        data.put("content","第7路单体锂电池电压");
        data.put("length","2");
        bmStatusAddress.put("0210",data);

        data=new HashMap<String, String>(0);
        data.put("content","第8路单体锂电池电压");
        data.put("length","2");
        bmStatusAddress.put("0211",data);

        data=new HashMap<String, String>(0);
        data.put("content","启动电池");
        data.put("length","2");
        bmStatusAddress.put("0212",data);

    }

    public static void main(String[] args) {
        System.out.println(getAddressName("0010"));
        System.out.println(getAddressName("0021"));
    }

    public static Map<String,String> getAddressName(String name){
        return bmAddress.get(name);
    }


    public static Map<String,String> getStatusName(String name){
        return bmStatusAddress.get(name);
    }


}
