package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

public class Combi_0003 {
    private static Map<String,Map<String, String>> combiAddress=new HashMap<>(0);
    private static Map<String,Map<String, String>> combiStatusAddress=new HashMap<>(0);
    private static Map<String,Map<String, String>> combiDescription = new HashMap<>(0);
    private static Map<String,String> data;
    static {
        data=new HashMap<>(0);
        data.put("content","电网输入低压保护值");
        data.put("length","2");
        combiAddress.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","电网输入低压恢复值");
        data.put("length","2");
        combiAddress.put("1001",data);

        data=new HashMap<>(0);
        data.put("content","电网输入高压恢复值");
        data.put("length","2");
        combiAddress.put("1002",data);

        data=new HashMap<>(0);
        data.put("content","电网输入高压保护值");
        data.put("length","2");
        combiAddress.put("1003",data);

        data=new HashMap<>(0);
        data.put("content","发电机输入低压保护值");
        data.put("length","2");
        combiAddress.put("1004",data);

        data=new HashMap<>(0);
        data.put("content","发电机输入低压恢复值");
        data.put("length","2");
        combiAddress.put("1005",data);

        data=new HashMap<>(0);
        data.put("content","发电机输入高压恢复值");
        data.put("length","2");
        combiAddress.put("1006",data);

        data=new HashMap<>(0);
        data.put("content","发电机输入高压保护值");
        data.put("length","2");
        combiAddress.put("1007",data);

        data=new HashMap<>(0);
        data.put("content","电网频率输入低保护值");
        data.put("length","2");
        combiAddress.put("1008",data);

        data=new HashMap<>(0);
        data.put("content","电网频率输入低恢复值");
        data.put("length","2");
        combiAddress.put("1009",data);

        data=new HashMap<>(0);
        data.put("content","电网频率输入高恢复值");
        data.put("length","2");
        combiAddress.put("100A",data);

        data=new HashMap<>(0);
        data.put("content","电网频率输入高保护值");
        data.put("length","2");
        combiAddress.put("100B",data);

        data=new HashMap<>(0);
        data.put("content","发电机频率输入低保护值");
        data.put("length","2");
        combiAddress.put("100C",data);

        data=new HashMap<>(0);
        data.put("content","发电机频率输入低恢复值");
        data.put("length","2");
        combiAddress.put("100D",data);

        data=new HashMap<>(0);
        data.put("content","发电机频率输入高恢复值");
        data.put("length","2");
        combiAddress.put("100E",data);

        data=new HashMap<>(0);
        data.put("content","发电机频率输入高保护值");
        data.put("length","2");
        combiAddress.put("100F",data);

        data=new HashMap<>(0);
        data.put("content","电池从CF模式进入CC充电的电压设定");
        data.put("length","2");
        combiAddress.put("1010",data);

        data=new HashMap<>(0);
        data.put("content","谷电时间区间设置");
        data.put("length","2");
        combiAddress.put("1011",data);

        data=new HashMap<>(0);
        data.put("content","二次启动电压点");
        data.put("length","2");
        combiAddress.put("1012",data);

        data=new HashMap<>(0);
        data.put("content","485协议版本");
        data.put("length","2");
        combiAddress.put("1013",data);

        data=new HashMap<>(0);
        data.put("content","电池低压关机点2");
        data.put("length","2");
        combiAddress.put("1014",data);

        data=new HashMap<>(0);
        data.put("content","交流充电器禁止");
        data.put("length","2");
        combiAddress.put("1015",data);

        data=new HashMap<>(0);
        data.put("content","静音模式时间");
        data.put("length","2");
        combiAddress.put("1016",data);

        data=new HashMap<>(0);
        data.put("content","电池可编程拨码定义");
        data.put("length","2");
        combiAddress.put("1017",data);

        data=new HashMap<>(0);
        data.put("content","额定输出电压");
        data.put("length","1");
        combiAddress.put("1018",data);

        data=new HashMap<>(0);
        data.put("content","额定输出频率");
        data.put("length","1");
        combiAddress.put("1019",data);

        data=new HashMap<>(0);
        data.put("content","工作模式");
        data.put("length","1");
        combiAddress.put("101A",data);

        data=new HashMap<>(0);
        data.put("content","直流低压关机点");
        data.put("length","2");
        combiAddress.put("101B",data);

        data=new HashMap<>(0);
        data.put("content","直流低压报警点");
        data.put("length","2");
        combiAddress.put("101C",data);

        data=new HashMap<>(0);
        data.put("content","电池类型");
        data.put("length","1");
        combiAddress.put("101D",data);

        data=new HashMap<>(0);
        data.put("content","电池AH数设置");
        data.put("length","1");
        combiAddress.put("101E",data);


        data=new HashMap<>(0);
        data.put("content","充电电压温度补偿");
        data.put("length","1");
        combiAddress.put("101F",data);


        data=new HashMap<>(0);
        data.put("content","充电电流设置(Max)");
        data.put("length","1");
        combiAddress.put("1020",data);

        data=new HashMap<>(0);
        data.put("content","aea");
        data.put("length","1");
        combiAddress.put("1021",data);

        data=new HashMap<>(0);
        data.put("content","节能模式启动");
        data.put("length","1");
        combiAddress.put("1022",data);

        data=new HashMap<>(0);
        data.put("content","节能模式进入值设置");
        data.put("length","1");
        combiAddress.put("1023",data);

        data=new HashMap<>(0);
        data.put("content","接地继电器功能使能");
        data.put("length","1");
        combiAddress.put("1024",data);

        data=new HashMap<>(0);
        data.put("content","Reserved");
        data.put("length","7");
        combiAddress.put("1025",data);

        data=new HashMap<>(0);
        data.put("content","Reserved");
        data.put("length","7");
        combiAddress.put("1026",data);

        data=new HashMap<>(0);
        data.put("content","RTC时间设置");
        data.put("length","7");
        combiAddress.put("1027",data);

        data=new HashMap<>(0);
        data.put("content","Buzzer鸣叫使能");
        data.put("length","1");
        combiAddress.put("1028",data);

        data=new HashMap<>(0);
        data.put("content","干接点触发源设置");
        data.put("length","1");
        combiAddress.put("1029",data);

        data=new HashMap<>(0);
        data.put("content","交直流主用设置");
        data.put("length","1");
        combiAddress.put("102A",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之CV电压设置");
        data.put("length","2");
        combiAddress.put("102B",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之CF电压设置");
        data.put("length","2");
        combiAddress.put("102C",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之CC最大时间设置");
        data.put("length","3");
        combiAddress.put("102D",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之CV最大时间设置");
        data.put("length","3");
        combiAddress.put("102E",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之EQ时间设置");
        data.put("length","2");
        combiAddress.put("102F",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之EQ电压设置");
        data.put("length","2");
        combiAddress.put("1030",data);

        data=new HashMap<>(0);
        data.put("content","RS485通讯地址");
        data.put("length","1");
        combiAddress.put("1031",data);

        data=new HashMap<>(0);
        data.put("content","奇偶校验");
        data.put("length","1");
        combiAddress.put("1032",data);

        data=new HashMap<>(0);
        data.put("content","485波特率设置");
        data.put("length","1");
        combiAddress.put("1033",data);

        data=new HashMap<>(0);
        data.put("content","额定功率");
        data.put("length","2");
        combiAddress.put("1034",data);

        data=new HashMap<>(0);
        data.put("content","额定直流电压");
        data.put("length","1");
        combiAddress.put("1035",data);

        data=new HashMap<>(0);
        data.put("content","LVD关机延迟时间");
        data.put("length","2");
        combiAddress.put("1036",data);

        data=new HashMap<>(0);
        data.put("content","制造商");
        data.put("length","30");
        combiAddress.put("1037",data);

        data=new HashMap<>(0);
        data.put("content","CTRL firmware版本");
        data.put("length","10");
        combiAddress.put("1038",data);

        data=new HashMap<>(0);
        data.put("content","LCD firmware版本");
        data.put("length","10");
        combiAddress.put("1039",data);

        data=new HashMap<>(0);
        data.put("content","TBB序列号");
        data.put("length","30");
        combiAddress.put("103A",data);

        data=new HashMap<>(0);
        data.put("content","TBB型号");
        data.put("length","10");
        combiAddress.put("103B",data);

        data=new HashMap<>(0);
        data.put("content","电池高压关机点");
        data.put("length","2");
        combiAddress.put("103C",data);

        data=new HashMap<>(0);
        data.put("content","发电机启动预热时间");
        data.put("length","1");
        combiAddress.put("103D",data);

        data=new HashMap<>(0);
        data.put("content","CF下从新充电时间设置");
        data.put("length","1");
        combiAddress.put("103E",data);

        data=new HashMap<>(0);
        data.put("content","充电从CV模式进入CF模式的充电电流");
        data.put("length","1");
        combiAddress.put("103F",data);

        data=new HashMap<>(0);
        data.put("content","Reserved");
        data.put("length","1");
        combiAddress.put("1040",data);

        data=new HashMap<>(0);
        data.put("content","电池容量2");
        data.put("length","2");
        combiAddress.put("1041",data);

        data=new HashMap<>(0);
        data.put("content","AEA Input limit2");
        data.put("length","2");
        combiAddress.put("1042",data);

        data=new HashMap<>(0);
        data.put("content","发电机启动条件使能");
        data.put("length","2");
        combiAddress.put("1043",data);

        data=new HashMap<>(0);
        data.put("content","发电机关闭条件使能");
        data.put("length","2");
        combiAddress.put("1044",data);

        data=new HashMap<>(0);
        data.put("content","电池电压启动发电机");
        data.put("length","2");
        combiAddress.put("1045",data);

        data=new HashMap<>(0);
        data.put("content","发电机低压启动延迟时间");
        data.put("length","2");
        combiAddress.put("1046",data);

        data=new HashMap<>(0);
        data.put("content","发电机负载启动值");
        data.put("length","2");
        combiAddress.put("1047",data);

        data=new HashMap<>(0);
        data.put("content","发电机负载启动延迟时间");
        data.put("length","2");
        combiAddress.put("1048",data);

        data=new HashMap<>(0);
        data.put("content","发电机开启时间2");
        data.put("length","2");
        combiAddress.put("1049",data);

        data=new HashMap<>(0);
        data.put("content","发电机停止时间2");
        data.put("length","2");
        combiAddress.put("104A",data);

        data=new HashMap<>(0);
        data.put("content","电池电压停止发电机");
        data.put("length","2");
        combiAddress.put("104B",data);

        data=new HashMap<>(0);
        data.put("content","CC结束关闭发电机延迟时间");
        data.put("length","2");
        combiAddress.put("104C",data);

        data=new HashMap<>(0);
        data.put("content","CV结束关闭发电机延迟时间");
        data.put("length","2");
        combiAddress.put("104D",data);

        data=new HashMap<>(0);
        data.put("content","发电机负载停止值");
        data.put("length","2");
        combiAddress.put("104E",data);

        data=new HashMap<>(0);
        data.put("content","发电机负载停止延迟时间");
        data.put("length","2");
        combiAddress.put("104F",data);

        data=new HashMap<>(0);
        data.put("content","发电机最小启动时间");
        data.put("length","2");
        combiAddress.put("1050",data);

        data=new HashMap<>(0);
        data.put("content","电池电压停止发电机延迟时间");
        data.put("length","2");
        combiAddress.put("1051",data);

        data=new HashMap<>(0);
        data.put("content","恢复默认设置");
        data.put("length","2");
        combiAddress.put("10FF",data);

        //设备状态地址
        data=new HashMap<>(0);
        data.put("content","发电机输入电压");
        data.put("length","2");
        combiStatusAddress.put("1064",data);

        data=new HashMap<>(0);
        data.put("content","发电机输入频率");
        data.put("length","2");
        combiStatusAddress.put("1065",data);

        data=new HashMap<>(0);
        data.put("content","电网输入电压");
        data.put("length","2");
        combiStatusAddress.put("1066",data);

        data=new HashMap<>(0);
        data.put("content","电网输入频率");
        data.put("length","2");
        combiStatusAddress.put("1067",data);


        data=new HashMap<>(0);
        data.put("content","输入电流");
        data.put("length","2");
        combiStatusAddress.put("1068",data);


        data=new HashMap<>(0);
        data.put("content","outputVoltage");
        data.put("length","2");
        combiStatusAddress.put("1069",data);

        data=new HashMap<>(0);
        data.put("content","输出频率");
        data.put("length","2");
        combiStatusAddress.put("106A",data);

        data=new HashMap<>(0);
        data.put("content","outputCurrent");
        data.put("length","2");
        combiStatusAddress.put("106B",data);

        data=new HashMap<>(0);
        data.put("content","outputPower");
        data.put("length","2");
        combiStatusAddress.put("106C",data);

        data=new HashMap<>(0);
        data.put("content","输出负载百分比");
        data.put("length","1");
        combiStatusAddress.put("106D",data);


        data=new HashMap<>(0);
        data.put("content","chargingCurrent");
        data.put("length","2");
        combiStatusAddress.put("106E",data);


        data=new HashMap<>(0);
        data.put("content","充电电流百分比");
        data.put("length","1");
        combiStatusAddress.put("106F",data);

        data=new HashMap<>(0);
        data.put("content","直流电压");
        data.put("length","2");
        combiStatusAddress.put("1070",data);


        data=new HashMap<>(0);
        data.put("content","散热片温度");
        data.put("length","2");
        combiStatusAddress.put("1071",data);


        data=new HashMap<>(0);
        data.put("content","机内温度");
        data.put("length","2");
        combiStatusAddress.put("1072",data);


        data=new HashMap<>(0);
        data.put("content","变压器温度");
        data.put("length","2");
        combiStatusAddress.put("1073",data);


        data=new HashMap<>(0);
        data.put("content","电池温度");
        data.put("length","2");
        combiStatusAddress.put("1074",data);

        data=new HashMap<>(0);
        data.put("content","风扇状态");
        data.put("length","1");
        combiStatusAddress.put("1075",data);


        data=new HashMap<>(0);
        data.put("content","CF工作状态");
        data.put("length","1");
        combiStatusAddress.put("1076",data);


        data=new HashMap<>(0);
        data.put("content","充电器工作状态");
        data.put("length","1");
        combiStatusAddress.put("1077",data);

        data=new HashMap<>(0);
        data.put("content","组合控制状态");
        data.put("length","1");
        combiStatusAddress.put("1078",data);

        data=new HashMap<>(0);
        data.put("content","开关信息");
        data.put("length","4");
        combiStatusAddress.put("1079",data);

        data=new HashMap<>(0);
        data.put("content","负载功率");
        data.put("length","2");
        combiStatusAddress.put("107A",data);

        data=new HashMap<>(0);
        data.put("content","invaild data");
        data.put("length","19");
        combiStatusAddress.put("107B",data);
        data = new HashMap<>(5);
        data.put("0","待机状态");
        data.put("1","旁路状态");
        data.put("2","电池逆变模式");
        data.put("3","错误模式");
        data.put("4","节能模式");
        combiDescription.put("1076",data);
        data = new HashMap<>(5);
        data.put("0","CC");
        data.put("1","CV");
        data.put("2","CF");
        data.put("3","Charger Stop");
        data.put("4","EQ");
        combiDescription.put("1077",data);
    }

    public static void main(String[] args) {
        System.out.println(Combi_0003.getCombiDescription("1076"));
    }

    public static Map<String,String> getCombiAddressName(String name){
        return combiAddress.get(name);
    }

    public static Map<String,String> getCombiStatusAddress(String name){
        return combiStatusAddress.get(name);
    }

    public static Map<String,String> getCombiDescription(String name){
        return combiDescription.get(name);
    }
}
