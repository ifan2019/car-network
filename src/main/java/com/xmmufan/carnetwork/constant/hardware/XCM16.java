package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

/**
 * XCM16 配置
 */
public class XCM16 {

    private static Map<String, Map<String,String>> address = new HashMap<>(0);
    private static Map<String,Map<String,String>> status=new HashMap<>(0);
    private static Map<String,Map<String,String>> control=new HashMap<>(0);

    static{
        String[] contents = new String[]{
            "1#PWM输出状态", "2#PWM输出状态", "3#PWM输出状态",
            "4#PWM输出状态", "5#PWM输出状态", "6#PWM输出状态",
            "indoorLight", "2#PWM输出位置", "foreheadLight",
            "outdoorLight", "backbedLight", "cassetteLight",
            "1#继电器输出状态", "2#继电器输出状态", "3#继电器输出状态",
            "4#继电器输出状态", "5#继电器输出状态", "6#继电器输出状态",
            "Vanity", "atmosphereLight", "9#继电器输出状态",
            "waterPump", "11#继电器输出状态", "12#继电器输出状态",
            "13#继电器输出状态", "14#继电器输出状态", "15#继电器输出状态", "16#继电器输出状态",
            "1#扩展继电器输出状态", "2#扩展继电器输出状态", "3#扩展继电器输出状态", "4#扩展继电器输出状态",
            "5#扩展继电器输出状态", "6#扩展继电器输出状态", "7#扩展继电器输出状态", "8#扩展继电器输出状态",
            "9#扩展继电器输出状态", "10#扩展继电器输出状态", "11#扩展继电器输出状态", "12#扩展继电器输出状态",
            "13#扩展继电器输出状态", "14#扩展继电器输出状态", "15#扩展继电器输出状态", "16#扩展继电器输出状态",
            "17#扩展继电器输出状态", "18#扩展继电器输出状态", "19#扩展继电器输出状态", "20#扩展继电器输出状态",
            "21#扩展继电器输出状态", "22#扩展继电器输出状态", "23#扩展继电器输出状态", "24#扩展继电器输出状态",
            "25#扩展继电器输出状态", "26#扩展继电器输出状态", "27#扩展继电器输出状态", "28#扩展继电器输出状态",
            "29#扩展继电器输出状态", "30#扩展继电器输出状态", "31#扩展继电器输出状态", "32#扩展继电器输出状态",
            "solutionTank", "greyTank", "blackTank",
            "室外温度", "室内温度", "启动电池电压", "后备电池电压",
            "空气质量传感器", "光强传感器", "睡眠模式", "总电源",
            "夜间模式", "低压报警", "MasterOff", "逆变器灯状态",
            "1#PWM输出关闭前位置", "2#PWM输出关闭前位置", "3#PWM输出关闭前位置", "4#PWM输出关闭前位置",
            "5#PWM输出关闭前位置", "6#PWM输出关闭前位置"
        };
        //outsideTemperature
        //insideTemperature
        String[] lengths = new String[]{
            "1","1","1","1","1","1","1","1","1","1",
            "1","1","1","1","1","1","1","1","1","1",
            "1","1","1","1","1","1","1","1","1","1",
            "1","1","1","1","1","1","1","1","1","1",
            "1","1","1","1","1","1","1","1","1","1",
            "1","1","1","1","1","1","1","1","1","1",
            "1","1","1","1","1","2","2","1","1","1",
            "1","1","1","1","1","1","1","1","1","1",
            "1"
        };
        String[] addresses = new String[]{
            "0100","0101","0102","0103","0104","0105","0106","0107","0108","0109",
            "010A","010B","010C","010D","010E","010F","0110","0111","0112","0113",
            "0114","0115","0116","0117","0118","0119","011A","011B","011C","011D",
            "011E","011F","0120","0121","0122","0123","0124","0125","0126","0127",
            "0128","0129","012A","012B","012C","012D","012E","012F","0130","0131",
            "0132","0133","0134","0135","0136","0137","0138","0139","013A","013B",
            "013C","013D","013E","013F","0140","0141","0142","0143","0144","0145",
            "0146","0147","0148","0149","014A","014B","014C","014D","014E","014F",
            "0150"
        };
        for(int i =0;i< contents.length;i++){
            Map<String,String> data = new HashMap<>(0);
            data.put("content",contents[i]);
            data.put("length", lengths[i]);
            status.put(addresses[i],data);
        }
    }

    public static void main(String[] args) {
        Map<String, String> xcmStatusByAddress = getXCMStatusByAddress("0141");

        System.out.println(xcmStatusByAddress);
    }

    public static Map<String,String> getXCMStatusByAddress(String address){
        return status.get(address);
    }
}
