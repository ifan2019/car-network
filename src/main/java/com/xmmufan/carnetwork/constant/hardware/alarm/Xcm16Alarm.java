package com.xmmufan.carnetwork.constant.hardware.alarm;

import java.util.HashMap;
import java.util.Map;

/**XCM 16报警信息  对应表
 * @author  chenli
 * @date      2019-8-18
 */
public class Xcm16Alarm {
    private static Map<String,Alarm> xcm16Alarm=new HashMap<>();
    static {
        xcm16Alarm.put("003601",new Alarm("通讯异常","Alarm","2"));
        xcm16Alarm.put("003602",new Alarm("通讯异常已恢复","Status",""));
        xcm16Alarm.put("003603",new Alarm("PWM输出回路1过流","Alarm","2"));
        xcm16Alarm.put("003604",new Alarm("PWM输出回路1过流已恢复","Status",""));
        xcm16Alarm.put("003605",new Alarm("PWM输出回路2过流","Alarm","2"));
        xcm16Alarm.put("003606",new Alarm("PWM输出回路2过流已恢复","Status",""));
        xcm16Alarm.put("003607",new Alarm("PWM输出回路3过流","Alarm","2"));
        xcm16Alarm.put("003608",new Alarm("PWM输出回路3过流已恢复","Status",""));
        xcm16Alarm.put("003609",new Alarm("PWM输出回路4过流","Alarm","2"));
        xcm16Alarm.put("00360A",new Alarm("PWM输出回路4过流已恢复","Status",""));
        xcm16Alarm.put("00360B",new Alarm("PWM输出回路5过流","Alarm","2"));
        xcm16Alarm.put("00360C",new Alarm("PWM输出回路5过流已恢复","Status",""));
        xcm16Alarm.put("00360D",new Alarm("PWM输出回路6过流","Alarm","2"));
        xcm16Alarm.put("00360E",new Alarm("PWM输出回路6过流已恢复","Status",""));
        xcm16Alarm.put("00360F",new Alarm("PWM输出回路1短路","Alarm","2"));
        xcm16Alarm.put("003610",new Alarm("PWM输出回路1短路已恢复","Status",""));
        xcm16Alarm.put("003611",new Alarm("PWM输出回路2短路","Alarm","2"));
        xcm16Alarm.put("003612",new Alarm("PWM输出回路2短路已恢复","Status",""));
        xcm16Alarm.put("003613",new Alarm("PWM输出回路3短路","Alarm","2"));
        xcm16Alarm.put("003614",new Alarm("PWM输出回路3短路已恢复","Status",""));
        xcm16Alarm.put("003615",new Alarm("PWM输出回路4短路","Alarm","2"));
        xcm16Alarm.put("003616",new Alarm("PWM输出回路4短路已恢复","Status",""));
        xcm16Alarm.put("003617",new Alarm("PWM输出回路5短路","Alarm","2"));
        xcm16Alarm.put("003618",new Alarm("PWM输出回路5短路已恢复","Status",""));
        xcm16Alarm.put("003619",new Alarm("PWM输出回路6短路","Alarm","2"));
        xcm16Alarm.put("00361A",new Alarm("PWM输出回路6短路已恢复","Status",""));
        xcm16Alarm.put("00361B",new Alarm("水位1过高","Status","NG"));
        xcm16Alarm.put("00361C",new Alarm("水位1过高已恢复","",""));
        xcm16Alarm.put("00361D",new Alarm("水位2过高","Alarm","2"));
        xcm16Alarm.put("00361E",new Alarm("水位2过高已恢复","Status",""));
        xcm16Alarm.put("00361F",new Alarm("水位3过高","Alarm","2"));
        xcm16Alarm.put("003620",new Alarm("水位3过高已恢复","Status",""));
        xcm16Alarm.put("003621",new Alarm("水位1过低","Alarm","1"));
        xcm16Alarm.put("003622",new Alarm("水位1过低已恢复","Status",""));
        xcm16Alarm.put("003623",new Alarm("水位2过低","Status","NG"));
        xcm16Alarm.put("003624",new Alarm("水位2过低已恢复","Status",""));
        xcm16Alarm.put("003625",new Alarm("水位3过低","Status","NG"));
        xcm16Alarm.put("003626",new Alarm("水位3过低已恢复","Status",""));
        xcm16Alarm.put("003627",new Alarm("水位传感器1未接","Alarm","2"));
        xcm16Alarm.put("003628",new Alarm("水位传感器1未接已恢复","Status",""));
        xcm16Alarm.put("003629",new Alarm("水位传感器2未接","Alarm","2"));
        xcm16Alarm.put("00362A",new Alarm("水位传感器2未接已恢复","Status",""));
        xcm16Alarm.put("00362B",new Alarm("水位传感器3未接","Alarm","2"));
        xcm16Alarm.put("00362C",new Alarm("水位传感器3未接已恢复","Status",""));
        xcm16Alarm.put("00362D",new Alarm("室内温度传感器未接","Alarm","1"));
        xcm16Alarm.put("00362E",new Alarm("室内温度传感器已恢复","Status",""));
        xcm16Alarm.put("00362F",new Alarm("室外温度传感器未接","Alarm","1"));
        xcm16Alarm.put("003630",new Alarm("室外温度传感器已恢复","Status",""));
        xcm16Alarm.put("003631",new Alarm("输入电压过低","Status","NG"));
        xcm16Alarm.put("003632",new Alarm("输入电压过低已恢复","Status",""));
        xcm16Alarm.put("003665",new Alarm("总电源开","Status",""));
        xcm16Alarm.put("003666",new Alarm("总电源关","Status",""));
        xcm16Alarm.put("003667",new Alarm("进入夜间模式","Status",""));
        xcm16Alarm.put("003668",new Alarm("退出夜间模式","Status",""));
        xcm16Alarm.put("003669",new Alarm("输入电压过低已恢复","Status",""));
        xcm16Alarm.put("00368D",new Alarm("LVD设置","Set",""));

    }

    public static Alarm getAlarmByName(String name){
        return xcm16Alarm.get(name);
    }
}
