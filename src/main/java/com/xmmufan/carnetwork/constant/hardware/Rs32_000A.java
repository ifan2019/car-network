package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

public class Rs32_000A {
    private static Map<String,Map<String,String>> rs32Address=new HashMap<String, Map<String, String>>(0);
    private static Map<String,Map<String,String>> rs32StatusAddress=new HashMap<String, Map<String, String>>(0);
    private static Map<String,Map<String,String>> rs32ControlAddress=new HashMap<String, Map<String, String>>(0);

    private static Map<String,String> data;
    static{
        data=new HashMap<String, String>(0);
        data.put("content","客定序列号");
        data.put("length","30");
        rs32Address.put("0000",data);

        data=new HashMap<String, String>(0);
        data.put("content","客定制造商");
        data.put("length","30");
        rs32Address.put("0001",data);

        data=new HashMap<String, String>(0);
        data.put("content","客定型号");
        data.put("length","30");
        rs32Address.put("0002",data);

        data=new HashMap<String, String>(0);
        data.put("content","TBB序列号");
        data.put("length","30");
        rs32Address.put("0003",data);

        data=new HashMap<String, String>(0);
        data.put("content","TBB型号");
        data.put("length","15");
        rs32Address.put("0004",data);

        data=new HashMap<String, String>(0);
        data.put("content","CTRL firmware版本");
        data.put("length","10");
        rs32Address.put("0005",data);

        data=new HashMap<String, String>(0);
        data.put("content","485协议版本");
        data.put("length","2");
        rs32Address.put("0006",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位1使能");
        data.put("length","2");
        rs32Address.put("0007",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2使能");
        data.put("length","2");
        rs32Address.put("0008",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3使能");
        data.put("length","2");
        rs32Address.put("0009",data);

        data=new HashMap<String, String>(0);
        data.put("content","水泵1使能");
        data.put("length","2");
        rs32Address.put("000A",data);

        data=new HashMap<String, String>(0);
        data.put("content","水泵2使能");
        data.put("length","2");
        rs32Address.put("000B",data);

        data=new HashMap<String, String>(0);
        data.put("content","通讯地址");
        data.put("length","2");
        rs32Address.put("000C",data);

        data=new HashMap<String, String>(0);
        data.put("content","奇偶校验");
        data.put("length","2");
        rs32Address.put("000D",data);

        data=new HashMap<String, String>(0);
        data.put("content","波特率");
        data.put("length","2");
        rs32Address.put("000E",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位1高报警值");
        data.put("length","2");
        rs32Address.put("000F",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2高报警值");
        data.put("length","2");
        rs32Address.put("0010",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3高报警值");
        data.put("length","2");
        rs32Address.put("0011",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位1低报警值");
        data.put("length","2");
        rs32Address.put("0012",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2低报警值");
        data.put("length","2");
        rs32Address.put("0013",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3低报警值");
        data.put("length","2");
        rs32Address.put("0014",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位1传感器类型");
        data.put("length","2");
        rs32Address.put("0015",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2传感器类型");
        data.put("length","2");
        rs32Address.put("0016",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3传感器类型");
        data.put("length","2");
        rs32Address.put("0017",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位1档位一上限值");
        data.put("length","2");
        rs32Address.put("0018",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位1档位二上限值");
        data.put("length","2");
        rs32Address.put("0019",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位1档位三上限值");
        data.put("length","2");
        rs32Address.put("001A",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位1档位四上限值");
        data.put("length","2");
        rs32Address.put("001B",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位1档位五上限值");
        data.put("length","2");
        rs32Address.put("001C",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位1档位六上限值");
        data.put("length","2");
        rs32Address.put("001D",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2档位一上限值");
        data.put("length","2");
        rs32Address.put("001E",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2档位二上限值");
        data.put("length","2");
        rs32Address.put("001F",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2档位三上限值");
        data.put("length","2");
        rs32Address.put("0020",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2档位四上限值");
        data.put("length","2");
        rs32Address.put("0021",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2档位五上限值");
        data.put("length","2");
        rs32Address.put("0022",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2档位六上限值");
        data.put("length","2");
        rs32Address.put("0023",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3档位一上限值");
        data.put("length","2");
        rs32Address.put("0024",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3档位二上限值");
        data.put("length","2");
        rs32Address.put("0025",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3档位三上限值");
        data.put("length","2");
        rs32Address.put("0026",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3档位四上限值");
        data.put("length","2");
        rs32Address.put("0027",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3档位五上限值");
        data.put("length","2");
        rs32Address.put("0028",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3档位六上限值");
        data.put("length","2");
        rs32Address.put("0029",data);

        data=new HashMap<String, String>(0);
        data.put("content","恢复默认设置");
        data.put("length","2");
        rs32Address.put("00FF",data);

        //RS32设备状态
        data=new HashMap<String, String>(0);
        data.put("content","水位1检测值");
        data.put("length","2");
        rs32StatusAddress.put("0100",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位2检测值");
        data.put("length","2");
        rs32StatusAddress.put("0101",data);

        data=new HashMap<String, String>(0);
        data.put("content","水位3检测值");
        data.put("length","2");
        rs32StatusAddress.put("0102",data);

        data=new HashMap<String, String>(0);
        data.put("content","水泵1状态");
        data.put("length","2");
        rs32StatusAddress.put("0103",data);

        data=new HashMap<String, String>(0);
        data.put("content","水泵2状态");
        data.put("length","2");
        rs32StatusAddress.put("0104",data);

        data=new HashMap<String, String>(0);
        data.put("content","模块电源电压");
        data.put("length","2");
        rs32StatusAddress.put("0105",data);

        data=new HashMap<String, String>(0);
        data.put("content","reserved");
        data.put("length","4");
        rs32StatusAddress.put("0106",data);

        data=new HashMap<String, String>(0);
        data.put("content","reserved");
        data.put("length","4");
        rs32StatusAddress.put("0107",data);

        data=new HashMap<String, String>(0);
        data.put("content","reserved");
        data.put("length","2");
        rs32StatusAddress.put("0108",data);

        data=new HashMap<String, String>(0);
        data.put("content","reserved");
        data.put("length","2");
        rs32StatusAddress.put("0109",data);

        data=new HashMap<String, String>(0);
        data.put("content","开关状态");
        data.put("length","4");
        rs32StatusAddress.put("010A",data);

        //RS32支持的控制
        data=new HashMap<>(0);
        data.put("content","水泵1控制");
        data.put("length","2");
        rs32ControlAddress.put("0103",data);

        data=new HashMap<>(0);
        data.put("content","水泵2控制");
        data.put("length","2");
        rs32ControlAddress.put("0104",data);

    }

    public static void main(String[] args) {
        System.out.println(getRs32AddressName("0026"));
        System.out.println(getRs32StatusAddressName("0109"));
    }

    public static Map<String,String> getRs32AddressName(String name){
        return rs32Address.get(name);
    }

    public static Map<String,String> getRs32StatusAddressName(String name){return rs32StatusAddress.get(name);}
    public static Map<String,String> getRs32ControlAddressName(String name){return rs32ControlAddress.get(name);}

}
