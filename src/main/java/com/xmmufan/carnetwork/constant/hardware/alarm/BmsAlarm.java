package com.xmmufan.carnetwork.constant.hardware.alarm;

import java.util.HashMap;
import java.util.Map;

/**
 * Bms 报警信息  对应表
 * @author  chenli
 * @date      2019-8-18
 */
public class BmsAlarm {
    private static  Map<String, Alarm> bmsMap=new HashMap<>();

    static{
        bmsMap.put("FF0301",new Alarm("通讯异常","Alarm","2"));
        bmsMap.put("FF0302",new Alarm("通讯异常已恢复","Status",""));
        bmsMap.put("FF0303",new Alarm("电池过压","Status","NG"));
        bmsMap.put("FF0304",new Alarm("电池过压已恢复","Status",""));
        bmsMap.put("FF0305",new Alarm("电芯过压","Status","NG"));
        bmsMap.put("FF0306",new Alarm("电芯过压已恢复","Status",""));
        bmsMap.put("FF0307",new Alarm("电池低压","Status","NG"));
        bmsMap.put("FF0308",new Alarm("电池低压已恢复","Status",""));
        bmsMap.put("FF0309",new Alarm("电芯低压","Status","NG"));
        bmsMap.put("FF030A",new Alarm("电芯低压已恢复","Status",""));
        bmsMap.put("FF030B",new Alarm("充电过流","Alarm","2"));
        bmsMap.put("FF030C",new Alarm("充电过流已恢复","Status",""));
        bmsMap.put("FF030D",new Alarm("放电过流","Alarm","2"));
        bmsMap.put("FF030E",new Alarm("放电过流已恢复","Status",""));
        bmsMap.put("FF030F",new Alarm("MOS温度过高","Status","NG"));
        bmsMap.put("FF0310",new Alarm("MOS温度过高已恢复","Status",""));
        bmsMap.put("FF0311",new Alarm("充电过温","Status","NG"));
        bmsMap.put("FF0312",new Alarm("充电过温已恢复","Status",""));
        bmsMap.put("FF0313",new Alarm("放电过温","Status","NG"));
        bmsMap.put("FF0314",new Alarm("放电过温已恢复","Status",""));
        bmsMap.put("FF0315",new Alarm("充电低温","Alarm","NG"));
        bmsMap.put("FF0316",new Alarm("充电低温已恢复","Status",""));
        bmsMap.put("FF0317",new Alarm("放电低温","Status","NG"));
        bmsMap.put("FF0318",new Alarm("放电低温已恢复","Status",""));
        bmsMap.put("FF0319",new Alarm("SOC过低","Status","NG"));
        bmsMap.put("FF031A",new Alarm("SOC过低已恢复","Status",""));
        bmsMap.put("FF031B",new Alarm("电压故障","Status","NG"));
        bmsMap.put("FF031C",new Alarm("电压故障已恢复","Status",""));
        bmsMap.put("FF031D",new Alarm("温度故障","Status","NG"));
        bmsMap.put("FF031E",new Alarm("温度故障已恢复","Status",""));
        bmsMap.put("FF031F",new Alarm("电流检测故障","Status","NG"));
        bmsMap.put("FF0320",new Alarm("电流检测故障已恢复","Status",""));
        bmsMap.put("FF0321",new Alarm("电芯不平衡","Alarm","1"));
        bmsMap.put("FF0322",new Alarm("电芯不平衡已恢复","Status",""));
        bmsMap.put("FF0366",new Alarm("开始充电","Status",""));
        bmsMap.put("FF0365",new Alarm("","",""));
        bmsMap.put("FF0367",new Alarm("开始放电","Status",""));
        bmsMap.put("FF0368",new Alarm("","",""));
        bmsMap.put("FF0369",new Alarm("进入保护状态","Status",""));
        bmsMap.put("FF036A",new Alarm("","",""));
        bmsMap.put("FF036B",new Alarm("待机","Status",""));
        bmsMap.put("FF03C9",new Alarm("","Set",""));
    }

    public  static Alarm getAlarmByName(String name){
        return bmsMap.get(name);
    }
}
