package com.xmmufan.carnetwork.constant.hardware.alarm;

import java.util.HashMap;
import java.util.Map;

/**
 * Combi 报警信息  对应表
 * @author  chenli
 * @date      2019-8-18
 */
public class CombiAlarm {
    private static Map<String,Alarm> combiAlarm=new HashMap<>();
    static {
        combiAlarm.put("000301",new Alarm("通讯异常","Alarm","2"));
        combiAlarm.put("000302",new Alarm("通讯异常已恢复","Status",""));
        combiAlarm.put("000303",new Alarm("电池温度过高","Status","NG"));
        combiAlarm.put("000304",new Alarm("电池温度过高已恢复","Status",""));
        combiAlarm.put("000305",new Alarm("环境过温","Status","NG"));
        combiAlarm.put("000306",new Alarm("环境过温已恢复","Status",""));
        combiAlarm.put("000307",new Alarm("变压器过温","Status","NG"));
        combiAlarm.put("000308",new Alarm("变压器过温已恢复","Status",""));
        combiAlarm.put("000309",new Alarm("散热器过温","Status","NG"));
        combiAlarm.put("00030A",new Alarm("散热器过温已恢复","Status",""));
        combiAlarm.put("00030B",new Alarm("逆变器短路","Alarm","2"));
        combiAlarm.put("00030C",new Alarm("逆变器短路已恢复","Status",""));
        combiAlarm.put("00030D",new Alarm("电网输入波形异常","Status","NG"));
        combiAlarm.put("00030E",new Alarm("电网输入波形异常已恢复","Status",""));
        combiAlarm.put("00030F",new Alarm("电网输入频率异常","Status","NG"));
        combiAlarm.put("000310",new Alarm("电网输入频率异常已恢复","Status",""));
        combiAlarm.put("000311",new Alarm("电网输入电压异常","Status","NG"));
        combiAlarm.put("000312",new Alarm("电网输入电压异常已恢复","Status",""));
        combiAlarm.put("000313",new Alarm("发电机输入波形异常","Status","NG"));
        combiAlarm.put("000314",new Alarm("发电机输入波形异常已恢复","Status",""));
        combiAlarm.put("000315",new Alarm("发电机输入频率异常","Status","NG"));
        combiAlarm.put("000316",new Alarm("发电机输入频率异常已恢复","Status",""));
        combiAlarm.put("000317",new Alarm("发电机输入电压异常","Status","NG"));
        combiAlarm.put("000318",new Alarm("发电机输入电压异常已恢复","Status",""));
        combiAlarm.put("000319",new Alarm("逆变器过载","Alarm","2"));
        combiAlarm.put("00031A",new Alarm("逆变器过载已恢复","Status",""));
        combiAlarm.put("00031B",new Alarm("直流输入高压","Status","NG"));
        combiAlarm.put("00031C",new Alarm("直流输入高压已恢复","Status",""));
        combiAlarm.put("00031D",new Alarm("直流输入低压","Status","NG"));
        combiAlarm.put("00031E",new Alarm("直流输入低压已恢复","Status",""));
        combiAlarm.put("00031F",new Alarm("MPPT过流","Alarm","1"));
        combiAlarm.put("000320",new Alarm("MPPT过流已恢复","Status",""));
        combiAlarm.put("000321",new Alarm("PV总线电压过高","Alarm","3"));
        combiAlarm.put("000322",new Alarm("PV总线电压过高已恢复","Status",""));
        combiAlarm.put("000323",new Alarm("MPPT防反保护","Alarm","2"));
        combiAlarm.put("000324",new Alarm("MPPT防反保护已恢复","Status",""));
        combiAlarm.put("000325",new Alarm("MPPT短路","Alarm","2"));
        combiAlarm.put("000326",new Alarm("MPPT短路已恢复","Status",""));
        combiAlarm.put("000327",new Alarm("PV环境温度过高","Status","NG"));
        combiAlarm.put("000328",new Alarm("PV环境温度过高已恢复","Status",""));
        combiAlarm.put("000329",new Alarm("电池低压","Status","NG"));
        combiAlarm.put("00032A",new Alarm("电池低压已恢复","Status",""));
        combiAlarm.put("00032B",new Alarm("电池高压","Status","NG"));
        combiAlarm.put("00032C",new Alarm("电池高压已恢复","Status",""));
        combiAlarm.put("00032D",new Alarm("PV高压","Alarm","3"));
        combiAlarm.put("00032E",new Alarm("PV高压已恢复","Status",""));
        combiAlarm.put("00032F",new Alarm("风扇堵转","Alarm","1"));
        combiAlarm.put("000330",new Alarm("风扇堵转已恢复","Status",""));
        combiAlarm.put("000331",new Alarm("故障","Status","NG"));
        combiAlarm.put("000332",new Alarm("故障已恢复","Status",""));
        combiAlarm.put("000333",new Alarm("逆变器主开关误动作","Alarm","2"));
        combiAlarm.put("000334",new Alarm("逆变器主开关误动作已恢复","Status",""));
        combiAlarm.put("000365",new Alarm("进入逆变模式","Status",""));
        combiAlarm.put("000366",new Alarm("退出逆变模式","Status",""));
        combiAlarm.put("000367",new Alarm("进入旁路模式","Status",""));
        combiAlarm.put("000368",new Alarm("退出旁路模式","Status",""));
        combiAlarm.put("000369",new Alarm("进入市电充电模式","Status",""));
        combiAlarm.put("00036A",new Alarm("退出市电充电模式","Status",""));
        combiAlarm.put("00036B",new Alarm("进入PV充电模式","Status",""));
        combiAlarm.put("00036C",new Alarm("退出PV充电模式","Status",""));
        combiAlarm.put("00036D",new Alarm("待机","Status",""));
        combiAlarm.put("00036E",new Alarm("","Status",""));
        combiAlarm.put("00038D",new Alarm("LVD设置","Set",""));
        combiAlarm.put("00038F",new Alarm("电池容量设置","Set",""));
        combiAlarm.put("000391",new Alarm("最大充电率设置","Set",""));
    }

    public  static Alarm getAlarmByName(String name){
        return combiAlarm.get(name);
    }
}
