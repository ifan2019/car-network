package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

public class Ddx_000F {
    private static Map<String, Map<String,String>> ddxStatusAddress=new HashMap<String, Map<String, String>>(0);
    private static Map<String,Map<String,String>>  ddxAddress=new HashMap<>(0);
    private static Map<String,String> data;

    static{
        data=new HashMap<>(0);
        data.put("content","序列号");
        data.put("length","30");
        ddxAddress.put("0000",data);

        data=new HashMap<>(0);
        data.put("content","制造商");
        data.put("length","30");
        ddxAddress.put("0001",data);

        data=new HashMap<>(0);
        data.put("content","型号");
        data.put("length","30");
        ddxAddress.put("0002",data);


        data=new HashMap<>(0);
        data.put("content","TBB序列号");
        data.put("length","30");
        ddxAddress.put("0003",data);

        data=new HashMap<>(0);
        data.put("content","TBB型号");
        data.put("length","15");
        ddxAddress.put("0004",data);

        data=new HashMap<>(0);
        data.put("content","CTRLfirmware版本");
        data.put("length","10");
        ddxAddress.put("0005",data);

        data=new HashMap<>(0);
        data.put("content","Reserved");
        data.put("length","10");
        ddxAddress.put("0006",data);

        data=new HashMap<>(0);
        data.put("content","UNIX时间戳");
        data.put("length","4");
        ddxAddress.put("0007",data);

        data=new HashMap<>(0);
        data.put("content","协议版本");
        data.put("length","2");
        ddxAddress.put("0008",data);

        data=new HashMap<>(0);
        data.put("content","RS485通讯地址");
        data.put("length","2");
        ddxAddress.put("0009",data);

        data=new HashMap<>(0);
        data.put("content","校验位");
        data.put("length","2");
        ddxAddress.put("000A",data);

        data=new HashMap<>(0);
        data.put("content","485波特率设置");
        data.put("length","2");
        ddxAddress.put("000B",data);

        data=new HashMap<>(0);
        data.put("content","后备电池类型");
        data.put("length","2");
        ddxAddress.put("000C",data);

        data=new HashMap<>(0);
        data.put("content","后备电池AH数设置");
        data.put("length","2");
        ddxAddress.put("000D",data);

        data=new HashMap<>(0);
        data.put("content","额定输出直流电压");
        data.put("length","2");
        ddxAddress.put("000E",data);

        data=new HashMap<>(0);
        data.put("content","后备电池低压保护点");
        data.put("length","2");
        ddxAddress.put("000F",data);

        data=new HashMap<>(0);
        data.put("content","后备电池高压保护点");
        data.put("length","2");
        ddxAddress.put("0010",data);

        data=new HashMap<>(0);
        data.put("content","设定后备电池充电率");
        data.put("length","2");
        ddxAddress.put("0011",data);

        data=new HashMap<>(0);
        data.put("content","后备电池温度补偿");
        data.put("length","2");
        ddxAddress.put("0012",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之CV电压设置");
        data.put("length","2");
        ddxAddress.put("0013",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之CF电压设置");
        data.put("length","2");
        ddxAddress.put("0014",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之EQ电压设置");
        data.put("length","2");
        ddxAddress.put("0015",data);

        data=new HashMap<>(0);
        data.put("content","Reserved");
        data.put("length","2");
        ddxAddress.put("0016",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之EQ时间设置");
        data.put("length","2");
        ddxAddress.put("0017",data);

        data=new HashMap<>(0);
        data.put("content","EQ提醒周期");
        data.put("length","2");
        ddxAddress.put("0018",data);

        data=new HashMap<>(0);
        data.put("content","后备电池低压保护点恢复值");
        data.put("length","2");
        ddxAddress.put("0019",data);

        data=new HashMap<>(0);
        data.put("content","后备电池高压保护点恢复值");
        data.put("length","2");
        ddxAddress.put("001A",data);

        data=new HashMap<>(0);
        data.put("content","额定输出电流");
        data.put("length","2");
        ddxAddress.put("001B",data);

        data=new HashMap<>(0);
        data.put("content","最大CC时间");
        data.put("length","2");
        ddxAddress.put("001C",data);

        data=new HashMap<>(0);
        data.put("content","充电曲线之CV最大时间设置");
        data.put("length","2");
        ddxAddress.put("001D",data);

        data=new HashMap<>(0);
        data.put("content","Cycle Time");
        data.put("length","2");
        ddxAddress.put("001E",data);

        data=new HashMap<>(0);
        data.put("content","电池低温保护使能");
        data.put("length","2");
        ddxAddress.put("001F",data);

        data=new HashMap<>(0);
        data.put("content","电池低温保护");
        data.put("length","2");
        ddxAddress.put("0020",data);


        data=new HashMap<>(0);
        data.put("content","电池低温限流使能");
        data.put("length","2");
        ddxAddress.put("0021",data);

        data=new HashMap<>(0);
        data.put("content","电池限流温度（低温）");
        data.put("length","2");
        ddxAddress.put("0022",data);

        data=new HashMap<>(0);
        data.put("content","电池限流电流（低温）");
        data.put("length","2");
        ddxAddress.put("0023",data);


        data=new HashMap<>(0);
        data.put("content","电池高温保护使能");
        data.put("length","2");
        ddxAddress.put("0024",data);

        data=new HashMap<>(0);
        data.put("content","电池高温保护");
        data.put("length","2");
        ddxAddress.put("0025",data);

        data=new HashMap<>(0);
        data.put("content","电池高温限流使能");
        data.put("length","2");
        ddxAddress.put("0026",data);

        data=new HashMap<>(0);
        data.put("content","电池限流温度（高温）");
        data.put("length","2");
        ddxAddress.put("0027",data);

        data=new HashMap<>(0);
        data.put("content","电池限流电流（高温）");
        data.put("length","2");
        ddxAddress.put("0028",data);

        data=new HashMap<>(0);
        data.put("content","智能发电机低压保护");
        data.put("length","2");
        ddxAddress.put("0029",data);

        data=new HashMap<>(0);
        data.put("content","智能发电机低压保护回差");
        data.put("length","2");
        ddxAddress.put("002A",data);

        data=new HashMap<>(0);
        data.put("content","传统发电机低压保护");
        data.put("length","2");
        ddxAddress.put("002B",data);


        data=new HashMap<>(0);
        data.put("content","传统发电机低压保护回差");
        data.put("length","2");
        ddxAddress.put("002C",data);


        data=new HashMap<>(0);
        data.put("content","PV低压保护");
        data.put("length","2");
        ddxAddress.put("002D",data);

        data=new HashMap<>(0);
        data.put("content","PV低压保护回差");
        data.put("length","2");
        ddxAddress.put("002E",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","2");
        ddxAddress.put("002F",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","2");
        ddxAddress.put("0030",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","2");
        ddxAddress.put("0031",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","2");
        ddxAddress.put("0032",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","2");
        ddxAddress.put("0033",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","2");
        ddxAddress.put("0034",data);


        data=new HashMap<>(0);
        data.put("content","恢复默认设置");
        data.put("length","2");
        ddxAddress.put("00FF",data);









        //DDX配置状态地址
        data=new HashMap<String, String>(0);
        data.put("content","PV输入电压");
        data.put("length","2");
        ddxStatusAddress.put("0100",data);

        data=new HashMap<String, String>(0);
        data.put("content","PV输入电流");
        data.put("length","2");
        ddxStatusAddress.put("0101",data);

        data=new HashMap<String, String>(0);
        data.put("content","启动电池电压");
        data.put("length","2");
        ddxStatusAddress.put("0102",data);

        data=new HashMap<String, String>(0);
        data.put("content","启动电池电流");
        data.put("length","2");
        ddxStatusAddress.put("0103",data);

        data=new HashMap<String, String>(0);
        data.put("content","后备电池充电电压");
        data.put("length","2");
        ddxStatusAddress.put("0104",data);

        data=new HashMap<String, String>(0);
        data.put("content","后备电池充电电流");
        data.put("length","2");
        ddxStatusAddress.put("0105",data);

        data=new HashMap<String, String>(0);
        data.put("content","后备电池温度");
        data.put("length","2");
        ddxStatusAddress.put("0106",data);

        data=new HashMap<String, String>(0);
        data.put("content","reserve");
        data.put("length","2");
        ddxStatusAddress.put("0107",data);

        data=new HashMap<String, String>(0);
        data.put("content","reserve");
        data.put("length","2");
        ddxStatusAddress.put("0108",data);

        data=new HashMap<String, String>(0);
        data.put("content","reserve");
        data.put("length","2");
        ddxStatusAddress.put("0109",data);

        data=new HashMap<String, String>(0);
        data.put("content","reserve");
        data.put("length","2");
        ddxStatusAddress.put("010A",data);

        data=new HashMap<String, String>(0);
        data.put("content","reserve");
        data.put("length","2");
        ddxStatusAddress.put("010B",data);

        data=new HashMap<String, String>(0);
        data.put("content","运行/故障状态信息字");
        data.put("length","4");
        ddxStatusAddress.put("010C",data);
    }

    public static Map<String, String> getDdxStatusAddress(String name) {
        return ddxStatusAddress.get(name);
    }
    public static Map<String,String> getDdxAddress(String name){
        return ddxAddress.get(name);
    }


}
