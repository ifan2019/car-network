package com.xmmufan.carnetwork.constant.hardware.alarm;

import java.util.HashMap;
import java.util.Map;

/**
 * ddx报警信息 对应表
 * @author ifan
 * @version 1.0
 * @date 2019/8/20
 */
public class DdxAlarm {
    private static Map<String,Alarm> ddx = new HashMap<>();

    static{
        ddx.put("003601",new Alarm("通讯异常","Alarm","2"));
        ddx.put("003602",new Alarm("通讯异常已恢复","Status",""));
        ddx.put("003603",new Alarm("电池低压","Status","NG"));
        ddx.put("003604",new Alarm("电池低压已恢复","Status",""));
        ddx.put("003605",new Alarm("电池高压","Status","NG"));
        ddx.put("003606",new Alarm("电池高压已恢复","Status",""));
        ddx.put("003607",new Alarm("PV低压","Status","NG"));
        ddx.put("003608",new Alarm("PV低压已恢复","Status",""));
        ddx.put("003609",new Alarm("PV高压","Alarm","3"));
        ddx.put("00360A",new Alarm("PV高压已恢复","Status",""));
        ddx.put("00360B",new Alarm("模块故障","Status","NG"));
        ddx.put("00360C",new Alarm("模块故障已恢复","Status",""));
        ddx.put("00360D",new Alarm("电池温度过高","Status","NG"));
        ddx.put("00360E",new Alarm("电池温度过高已恢复","Status",""));
        ddx.put("00360F",new Alarm("电池温度过低","Status","NG"));
        ddx.put("003610",new Alarm("电池温度过低已恢复","Status",""));
        ddx.put("003611",new Alarm("内部温度过高","Status","NG"));
        ddx.put("003612",new Alarm("内部温度过高已恢复","Status",""));
        ddx.put("003613",new Alarm("启动电池低压","Alarm","1"));
        ddx.put("003614",new Alarm("启动电池低压已恢复","Status",""));
        ddx.put("003615",new Alarm("启动电池高压","Alarm","1"));
        ddx.put("003616",new Alarm("启动电池高压已恢复","Status",""));
        ddx.put("003617",new Alarm("充电器过载","Status","NG"));
        ddx.put("003618",new Alarm("充电器过载已恢复","Status",""));
        ddx.put("003619",new Alarm("充电器短路","Status","NG"));
        ddx.put("00361A",new Alarm("充电器短路已恢复","Status",""));
        ddx.put("00361B",new Alarm("电池BTS未接","Alarm","1"));
        ddx.put("00361C",new Alarm("电池BTS未接已恢复","Status",""));
        ddx.put("00361D",new Alarm("电池BTS短路","Alarm","2"));
        ddx.put("00361E",new Alarm("电池BTS短路已恢复","Status",""));

        ddx.put("003665",new Alarm("启动电池开始给后备电池充电","Status",""));
        ddx.put("003666",new Alarm("启动电池停止给后备电池充电","Status",""));
        ddx.put("003667",new Alarm("PV开始给后备电池充电","Status",""));
        ddx.put("003668",new Alarm("PV停止给后备电池充电","Status",""));
        ddx.put("003669",new Alarm("内部温度过高","Status",""));
        ddx.put("00366A",new Alarm("内部温度过高已恢复","Status",""));

        ddx.put("00368D",new Alarm("LVD设置成功","Status",""));
        ddx.put("00368F",new Alarm("电池容量设置成功","Status",""));
        ddx.put("003691",new Alarm("最大充电率设置成功","Status",""));
    }

    public static Alarm getAlarmByName(String name){
        return ddx.get(name);
    }
}
