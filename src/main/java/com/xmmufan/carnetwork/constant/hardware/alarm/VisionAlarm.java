package com.xmmufan.carnetwork.constant.hardware.alarm;

import java.util.HashMap;
import java.util.Map;

/**
 * Vision 报警信息  对应表
 * @author  chenli
 * @date      2019-8-18
 */
public class VisionAlarm {
    private static Map<String,Alarm> visionAlarm=new HashMap<>();
    static {
        visionAlarm.put("001B01",new Alarm("触控屏通讯异常","Alarm","2"));
        visionAlarm.put("001B02",new Alarm("触控屏通讯异常已恢复","Status",""));
        visionAlarm.put("001B03",new Alarm("电池低压或SOC低（1级）","Alarm","1"));
        visionAlarm.put("001B04",new Alarm("电池低压或SOC低已恢复","Status","2"));
        visionAlarm.put("001B05",new Alarm("电池低压或SOC低（2级）","Alarm","2"));
        visionAlarm.put("001B06",new Alarm("","","2"));
        visionAlarm.put("001B07",new Alarm("电池低压（3级）","Alarm","3"));
        visionAlarm.put("001B08",new Alarm("","",""));
        visionAlarm.put("001B09",new Alarm("电池高压","Alarm","2"));
        visionAlarm.put("001B0A",new Alarm("电池高压已恢复","Status",""));
        visionAlarm.put("001B0B",new Alarm("电池低温","Alarm","3"));
        visionAlarm.put("001B0C",new Alarm("电池低温已恢复","Status",""));
        visionAlarm.put("001B0D",new Alarm("电池高温","Alarm","3"));
        visionAlarm.put("001B0E",new Alarm("电池高温已恢复","Status",""));
        visionAlarm.put("001B65",new Alarm("开启","Status",""));
        visionAlarm.put("001B66",new Alarm("固件更新成功","Status",""));
        visionAlarm.put("001B67",new Alarm("固件更新失败","Status",""));
        visionAlarm.put("001BC9",new Alarm("","Set",""));
    }

    public static Alarm getAlarmByName(String name){
        return visionAlarm.get(name);
    }
}
