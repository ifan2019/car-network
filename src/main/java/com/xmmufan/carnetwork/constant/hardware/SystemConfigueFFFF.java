package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

public class SystemConfigueFFFF {
    private static Map<String,Map<String,String>> address = new HashMap<>(0);
    private static Map<String,Map<String,String>> status=new HashMap<>(0);
    private static Map<String,Map<String,String>> control=new HashMap<>(0);
    private static Map<String,String> data;

    static{
        data=new HashMap<>(0);
        data.put("content","系统类型");
        data.put("length","4");data.clear();
        address.put("0000",data);

        data=new HashMap<>(0);
        data.put("content","本地登录用户名与密码");
        data.put("length","41");
        address.put("0001",data);


        data=new HashMap<>(0);
        data.put("content","时间设置");
        data.put("length","4");
        address.put("0002",data);


        data=new HashMap<>(0);
        data.put("content","监控中心序列号");
        data.put("length","30");
        address.put("0003",data);


        data=new HashMap<>(0);
        data.put("content","系统序列号");
        data.put("length","30");
        address.put("0004",data);


        data=new HashMap<>(0);
        data.put("content","历史纪录上传类型");
        data.put("length","2");
        address.put("0005",data);


        data=new HashMap<>(0);
        data.put("content","Reverse");
        data.put("length","2");
        address.put("0006",data);


        data=new HashMap<>(0);
        data.put("content","Reverse");
        data.put("length","2");
        address.put("0007",data);


        data=new HashMap<>(0);
        data.put("content","事件连续查询起始索引");
        data.put("length","4");
        address.put("0008",data);


        data=new HashMap<>(0);
        data.put("content","更新固件时间");
        data.put("length","4");
        address.put("0009",data);


        data=new HashMap<>(0);
        data.put("content","系统安装时间");
        data.put("length","4");
        address.put("000A",data);


        data=new HashMap<>(0);
        data.put("content","电池类型");
        data.put("length","1");
        address.put("000B",data);


        data=new HashMap<>(0);
        data.put("content","电池容量");
        data.put("length","2");
        address.put("000C",data);


        data=new HashMap<>(0);
        data.put("content","直流输入低压保护点");
        data.put("length","2");data.clear();
        address.put("000D",data);


        data=new HashMap<>(0);
        data.put("content","PV低压保护点");
        data.put("length","2");
        address.put("000E",data);


        data=new HashMap<>(0);
        data.put("content","AEA限流值");
        data.put("length","1");
        address.put("000F",data);

        data=new HashMap<>(0);
        data.put("content","交流低压保护电压");
        data.put("length","2");
        address.put("0010",data);


        data=new HashMap<>(0);
        data.put("content","交流高压保护电压");
        data.put("length","2");
        address.put("0011",data);


        data=new HashMap<>(0);
        data.put("content","额定交流输出电压");
        data.put("length","1");
        address.put("0012",data);


        data=new HashMap<>(0);
        data.put("content","额定交流输出频率");
        data.put("length","1");
        address.put("0013",data);


        data=new HashMap<>(0);
        data.put("content","节能模式阈值");
        data.put("length","1");
        address.put("0014",data);


        data=new HashMap<>(0);
        data.put("content","最大充电率");
        data.put("length","1");
        address.put("0015",data);


        data=new HashMap<>(0);
        data.put("content","恒压充电电压");
        data.put("length","2");
        address.put("0016",data);


        data=new HashMap<>(0);
        data.put("content","最大恒压充电时间");
        data.put("length","3");
        address.put("0017",data);


        data=new HashMap<>(0);
        data.put("content","浮充电压");
        data.put("length","2");
        address.put("0018",data);


        data=new HashMap<>(0);
        data.put("content","最大恒流充电时间");
        data.put("length","3");
        address.put("0019",data);


        data=new HashMap<>(0);
        data.put("content","温度补偿系数");
        data.put("length","1");
        address.put("001A",data);


        data=new HashMap<>(0);
        data.put("content","EQ电压设置");
        data.put("length","2");
        address.put("001B",data);


        data=new HashMap<>(0);
        data.put("content","EQ时间设置");
        data.put("length","2");
        address.put("001C",data);


        data=new HashMap<>(0);
        data.put("content","逆变器工作模式");
        data.put("length","1");
        address.put("001D",data);


        data=new HashMap<>(0);
        data.put("content","恢复出厂设置");
        data.put("length","1");
        address.put("001E",data);


        data=new HashMap<>(0);
        data.put("content","语言设置");
        data.put("length","1");
        address.put("001F",data);


        data=new HashMap<>(0);
        data.put("content","额定功率");
        data.put("length","2");
        address.put("0020",data);


        data=new HashMap<>(0);
        data.put("content","额定直流电压");
        data.put("length","1");
        address.put("0021",data);


        data=new HashMap<>(0);
        data.put("content","设备添加");
        data.put("length","1");
        address.put("0022",data);


        data=new HashMap<>(0);
        data.put("content","温度单位");
        data.put("length","1");
        address.put("0023",data);


        data=new HashMap<>(0);
        data.put("content","逆变器节能模式启动");
        data.put("length","1");
        address.put("0024",data);


        data=new HashMap<>(0);
        data.put("content","蓝牙英文名称");
        data.put("length","18");
        address.put("0025",data);


        //系统状态数据地址
        data=new HashMap<>(0);
        data.put("content","系统接点数量");
        data.put("length","2");
        status.put("0100",data);

        data=new HashMap<>(0);
        data.put("content","固件更新状态");
        data.put("length","2");
        status.put("0101",data);

        data=new HashMap<>(0);
        data.put("content","更新中的设备");
        data.put("length","4");
        status.put("0102",data);

        data=new HashMap<>(0);
        data.put("content","当前固件更新进度");
        data.put("length","2");
        status.put("0103",data);

        data=new HashMap<>(0);
        data.put("content","insideTemperature");
        data.put("length","1");
        status.put("0104",data);

        data=new HashMap<>(0);
        data.put("content","outsideTemperature");
        data.put("length","1");
        status.put("0105",data);

        data=new HashMap<>(0);
        data.put("content","SOC");
        data.put("length","2");
        status.put("0106",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","1");
        status.put("0107",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","2");
        status.put("0108",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","2");
        status.put("0109",data);

        data=new HashMap<>(0);
        data.put("content","系统报警");
        data.put("length","240");
        status.put("010A",data);

        data=new HashMap<>(0);
        data.put("content","系统总事件数");
        data.put("length","4");
        status.put("010B",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","4");
        status.put("010C",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","4");
        status.put("010D",data);

        data=new HashMap<>(0);
        data.put("content","总电源");
        data.put("length","1");
        status.put("010E",data);

        data=new HashMap<>(0);
        data.put("content","夜间模式");
        data.put("length","1");
        status.put("010F",data);

        data=new HashMap<>(0);
        data.put("content","鸣叫状态");
        data.put("length","1");
        status.put("0110",data);

        data=new HashMap<>(0);
        data.put("content","逆变器开关状态");
        data.put("length","1");
        status.put("0111",data);

        data=new HashMap<>(0);
        data.put("content","InverterWorkingState");
        data.put("length","2");
        status.put("0112",data);

        data=new HashMap<>(0);
        data.put("content","后备电池状态");
        data.put("length","2");
        status.put("0113",data);

        data=new HashMap<>(0);
        data.put("content","Time to go");
        data.put("length","8");
        status.put("0114",data);

        data=new HashMap<>(0);
        data.put("content","后备电池电压");
        data.put("length","4");
        status.put("0115",data);

        data=new HashMap<>(0);
        data.put("content","后备电池电流");
        data.put("length","4");
        status.put("0116",data);

        data=new HashMap<>(0);
        data.put("content","后备电池温度");
        data.put("length","4");
        status.put("0117",data);

        data=new HashMap<>(0);
        data.put("content","后备电池循环次数");
        data.put("length","4");
        status.put("0118",data);



        data=new HashMap<>(0);
        data.put("content","PV当天发电量");
        data.put("length","8");
        status.put("0119",data);

        data=new HashMap<>(0);
        data.put("content","水泵开关状态");
        data.put("length","1");
        status.put("011A",data);

        data=new HashMap<>(0);
        data.put("content","设备连接状态");
        data.put("length","2");
        status.put("011B",data);


        //系统控制
        data=new HashMap<>(0);
        data.put("content","设备连接状态");
        data.put("length","4");
        control.put("010B",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","4");
        control.put("010C",data);

        data=new HashMap<>(0);
        data.put("content","Reversed");
        data.put("length","4");
        control.put("010D",data);

        data=new HashMap<>(0);
        data.put("content","总电源状态");
        data.put("length","1");
        control.put("010E",data);

        data=new HashMap<>(0);
        data.put("content","夜间模式");
        data.put("length","1");
        control.put("010F",data);

        data=new HashMap<>(0);
        data.put("content","鸣叫状态");
        data.put("length","1");
        control.put("0110",data);

        data=new HashMap<>(0);
        data.put("content","逆变器开关状态");
        data.put("length","1");
        control.put("0111",data);

        data=new HashMap<>(0);
        data.put("content","水泵开关状态");
        data.put("length","4");
        control.put("0117",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);


        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

        data=new HashMap<>(0);
        data.put("content","清除事件记录");
        data.put("length","4");
        control.put("1000",data);

    }

    public static void main(String[] args) {
        String s="fe000121fe";
        System.out.println(getAddressName("0025"));
        System.out.println(getStatusName("0117"));
    }

    public static Map<String, String> getAddressName(String name){
        return address.get(name);
    }

    public static  Map<String,String> getStatusName(String name){
        return status.get(name);
    }

    public static Map<String,String> getControlName(String name) {
        return control.get(name);
    }
}
