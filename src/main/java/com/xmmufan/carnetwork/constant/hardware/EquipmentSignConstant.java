package com.xmmufan.carnetwork.constant.hardware;

public class EquipmentSignConstant {

    public final static String CCS = "0001";
    public final static String CYBER = "0002";
    public final static String COMBI = "0003";
    public final static String MEDU = "0004";
    public final static String SP_CTRL = "0005";
    public final static String SP_LCD = "0006";
    public final static String CRYSTAL = "0007";
    public final static String MP = "0008";
    public final static String LCM10 = "0009";
    public final static String RS32 = "000a";
    public final static String CMP = "000b";
    public final static String LCM15 = "000c";
    public final static String MSATER = "000d";
    public final static String DX = "000e";
    public final static String DDX = "000f";
    public final static String VISION = "0001b";
    public final static String BM = "0030";
    public final static String XCM16 = "0036";
    public final static String BMS = "ff03";
    public final static String SYSTEM_SIGN = "ffff";

}
