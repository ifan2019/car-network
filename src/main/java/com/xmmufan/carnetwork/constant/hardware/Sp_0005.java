package com.xmmufan.carnetwork.constant.hardware;

import java.util.HashMap;
import java.util.Map;

public class Sp_0005 {
    private static Map<String, Map<String,String>> spAddress=new HashMap<String, Map<String, String>>(0);
    private static Map<String,Map<String,String>> spStatusAddress=new HashMap<String, Map<String, String>>(0);
    private static Map<String,String> data;
    static{
        data=new HashMap<String, String>(0);
        data.put("content","系统语言");
        data.put("length","1");
        spAddress.put("1000",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池类型");
        data.put("length","1");
        spAddress.put("1001",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池AH数设置");
        data.put("length","1");
        spAddress.put("1002",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池充电率");
        data.put("length","1");
        spAddress.put("1003",data);

        data=new HashMap<String, String>(0);
        data.put("content","充电电压温度补偿");
        data.put("length","1");
        spAddress.put("1004",data);

        data=new HashMap<String, String>(0);
        data.put("content","最大充电电流");
        data.put("length","1");
        spAddress.put("1005",data);

        data=new HashMap<String, String>(0);
        data.put("content","额定直流电压");
        data.put("length","1");
        spAddress.put("1006",data);

        data=new HashMap<String, String>(0);
        data.put("content","PV阵列序号");
        data.put("length","1");
        spAddress.put("1007",data);

        data=new HashMap<String, String>(0);
        data.put("content","PV额定电压(暂不支持,保留)");
        data.put("length","2");
        spAddress.put("1008",data);

        data=new HashMap<String, String>(0);
        data.put("content","PV额定功率(暂不支持,保留)");
        data.put("length","2");
        spAddress.put("1009",data);

        data=new HashMap<String, String>(0);
        data.put("content","直流低压保护点");
        data.put("length","2");
        spAddress.put("100A",data);

        data=new HashMap<String, String>(0);
        data.put("content","直流高压保护点");
        data.put("length","2");
        spAddress.put("100B",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","2");
        spAddress.put("100C",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","2");
        spAddress.put("100D",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","2");
        spAddress.put("100E",data);

        data=new HashMap<String, String>(0);
        data.put("content","充电曲线之CV电压设置");
        data.put("length","2");
        spAddress.put("100F",data);

        data=new HashMap<String, String>(0);
        data.put("content","充电曲线之CF电压设置");
        data.put("length","2");
        spAddress.put("1010",data);

        data=new HashMap<String, String>(0);
        data.put("content","充电曲线之EQ电压设置");
        data.put("length","2");
        spAddress.put("1011",data);

        data=new HashMap<String, String>(0);
        data.put("content","充电曲线之CV2最大时间设置");
        data.put("length","2");
        spAddress.put("1012",data);

        data=new HashMap<String, String>(0);
        data.put("content","充电曲线之EQ时间设置");
        data.put("length","2");
        spAddress.put("1013",data);

        data=new HashMap<String, String>(0);
        data.put("content","EQ提醒周期");
        data.put("length","1");
        spAddress.put("1014",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","2");
        spAddress.put("1015",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","2");
        spAddress.put("1016",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","2");
        spAddress.put("1017",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","2");
        spAddress.put("1018",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","2");
        spAddress.put("1019",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","2");
        spAddress.put("101A",data);

        data=new HashMap<String, String>(0);
        data.put("content","干接点触发源设置");
        data.put("length","1");
        spAddress.put("101B",data);

        data=new HashMap<String, String>(0);
        data.put("content","Reserved");
        data.put("length","1");
        spAddress.put("101C",data);


        data=new HashMap<String, String>(0);
        data.put("content","RTC时间设置");
        data.put("length","7");
        spAddress.put("101D",data);


        data=new HashMap<String, String>(0);
        data.put("content","RS485通讯地址");
        data.put("length","1");
        spAddress.put("101E",data);


        data=new HashMap<String, String>(0);
        data.put("content","校验位");
        data.put("length","1");
        spAddress.put("101F",data);


        data=new HashMap<String, String>(0);
        data.put("content","485波特率设置");
        data.put("length","1");
        spAddress.put("1020",data);

        data=new HashMap<String, String>(0);
        data.put("content","序列号");
        data.put("length","30");
        spAddress.put("1021",data);

        data=new HashMap<String, String>(0);
        data.put("content","型号");
        data.put("length","30");
        spAddress.put("1022",data);

        data=new HashMap<String, String>(0);
        data.put("content","制造商");
        data.put("length","30");
        spAddress.put("1023",data);

        data=new HashMap<String, String>(0);
        data.put("content","CTRL firmware版本");
        data.put("length","10");
        spAddress.put("1024",data);

        data=new HashMap<String, String>(0);
        data.put("content","LCD firmware版本");
        data.put("length","10");
        spAddress.put("1025",data);

        data=new HashMap<String, String>(0);
        data.put("content","TBB序列号");
        data.put("length","30");
        spAddress.put("1026",data);

        data=new HashMap<String, String>(0);
        data.put("content","TBB型号");
        data.put("length","15");
        spAddress.put("1027",data);

        //SP设备地址
        data=new HashMap<String, String>(0);
        data.put("content","PV输入电压");
        data.put("length","2");
        spStatusAddress.put("1064",data);

        data=new HashMap<String, String>(0);
        data.put("content","PV输入电流(暂不支持,保留)");
        data.put("length","2");
        spStatusAddress.put("1065",data);

        data=new HashMap<String, String>(0);
        data.put("content","PV输入功率(暂不支持,保留)");
        data.put("length","2");
        spStatusAddress.put("1066",data);

        data=new HashMap<String, String>(0);
        data.put("content","每日PV板发电度数(暂不支持,保留)");
        data.put("length","2");
        spStatusAddress.put("1067",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池充电电压");
        data.put("length","2");
        spStatusAddress.put("1068",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池充电电流");
        data.put("length","2");
        spStatusAddress.put("1069",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池充电功率");
        data.put("length","2");
        spStatusAddress.put("106A",data);

        data=new HashMap<String, String>(0);
        data.put("content","每日SP充电度数");
        data.put("length","2");
        spStatusAddress.put("106B",data);

        data=new HashMap<String, String>(0);
        data.put("content","散热片温度");
        data.put("length","2");
        spStatusAddress.put("106C",data);

        data=new HashMap<String, String>(0);
        data.put("content","机内温度");
        data.put("length","2");
        spStatusAddress.put("106D",data);

        data=new HashMap<String, String>(0);
        data.put("content","电池温度");
        data.put("length","2");
        spStatusAddress.put("106E",data);

        data=new HashMap<String, String>(0);
        data.put("content","实时充电能量数据高两字节");
        data.put("length","2");
        spStatusAddress.put("106F",data);

        data=new HashMap<String, String>(0);
        data.put("content","实时充电能量数据低两字节");
        data.put("length","2");
        spStatusAddress.put("1070",data);

        data=new HashMap<String, String>(0);
        data.put("content","运行/故障状态信息字");
        data.put("length","2");
        spStatusAddress.put("1071",data);


    }

    public static void main(String[] args) {
        System.out.println(getSpAddressName("1025"));
        System.out.println(getSpStatusAddressName("106E"));

    }
    public static Map<String,String> getSpAddressName(String name){
        return spAddress.get(name);
    }

    public static Map<String,String> getSpStatusAddressName(String name){
        return spStatusAddress.get(name);
    }
}
