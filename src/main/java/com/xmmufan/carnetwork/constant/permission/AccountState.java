package com.xmmufan.carnetwork.constant.permission;

/**
 * @author Mr ifan/詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 *     常见的用户账号状态码封装
 * </p>
 */
public final class AccountState {

    /*
    * 用户锁定的状态
    * */
    public final static byte LOCKED = 1;

    /*
    * 用户未激活的状态
    * */
    public final static byte NOT_CERTIFIED = -1;

    /*
    * 正常状态
    * */
    public final static byte ACTIVE = 0;

}
