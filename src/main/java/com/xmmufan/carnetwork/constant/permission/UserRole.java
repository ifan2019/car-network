package com.xmmufan.carnetwork.constant.permission;

/**
 * @author Mr.ifan| 詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 * </p>
 */
public final class UserRole {

    public static final String ROLE_ADMIN = "role_admin";

    public static final String ROLE_USER = "role_user";

}
