package com.xmmufan.carnetwork.constant.permission;

/**
 * @author Mr ifan/詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 *     常见用户权限字段封装
 * </p>
 */
public final class UserPermission {

    public final static String USER_CREATE = "user:create";

    public final static String USER_UPDATE = "user:update";

    public final static String USER_DELETE = "user:delete";

    public final static String USER_QUERY  = "user:query";


}
