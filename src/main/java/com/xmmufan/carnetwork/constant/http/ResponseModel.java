package com.xmmufan.carnetwork.constant.http;


/**
 * @author Mr ifan/詹奕凡
 * @version 1.0
 * @date 2018/11/13
 * <p>
 *     返回前端实体类封装
 * </p>
 */
public final class ResponseModel {


    private Integer statusCode = 200;

    private String message = "返回成功";

    private Object data;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ResponseModel(){

    }
    public ResponseModel(Integer statusCode,String message,Object data){
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseModel{" +
                "statusCode=" + statusCode +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
