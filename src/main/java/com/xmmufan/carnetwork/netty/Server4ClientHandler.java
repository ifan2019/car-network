package com.xmmufan.carnetwork.netty;

import com.xmmufan.carnetwork.repository.dao.CarDao;
import com.xmmufan.carnetwork.util.CarMap;
import com.xmmufan.carnetwork.util.UtilMap;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author ifan
 */
@Slf4j
public class Server4ClientHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {




    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) throws Exception {
        log.info("in function channel Read");
        //获取通道消息(放入的是随机数和carId)
        String message= msg.text().replaceAll("fe","");
        String command=message.substring(0,4);
        String random=message.substring(4,10);
        String carVin=message.substring(10);
        log.info("random = {} carId= {}", random ,carVin);
        //设置通道和carMap
        UtilMap.addChannel(random,ctx.channel());
        CarMap.addMap(random,carVin);
        if("0010".equals(command)){
            ctx.writeAndFlush(new TextWebSocketFrame("fe00110000fe"));
        }else if("0030".equals(command)){
            ctx.channel().closeFuture().sync();
        }
    }
}
