package com.xmmufan.carnetwork.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * @author ifan
 */
public class Server4Client {
    /**
     * 监听线程组，监听客户端请求
     */
    private EventLoopGroup acceptorGroup;
    /**
     * 处理客户端相关操作线程组，负责处理与客户端的数据通讯
     */
    private EventLoopGroup clientGroup;

    private ServerBootstrap bootstrap;

    public Server4Client(){
        init();
    }

    private void init() {
        acceptorGroup = new NioEventLoopGroup();
        clientGroup = new NioEventLoopGroup();
        bootstrap = new ServerBootstrap();
        //绑定线程组
        bootstrap.group(acceptorGroup,clientGroup);
        //设置通讯模式为NIO
        bootstrap.channel(NioServerSocketChannel.class);
        //设置缓冲区大小
        bootstrap.option(ChannelOption.SO_BACKLOG,1024);
        //SO_SNDBUF 发送缓冲区，SO_RCVBUF接收缓冲区，
        //SO_KEEPALIVE 开启心跳监测（保证连接有效）
        bootstrap.option(ChannelOption.SO_SNDBUF,16*1024)
                .option(ChannelOption.SO_RCVBUF,16*1024)
                .option(ChannelOption.SO_KEEPALIVE,true);
    }

    /**
     * 监听处理逻辑
     * @param port 监听端口
     * @return ChannelFuture
     * @throws InterruptedException
     */
    public ChannelFuture doAccept(int port) throws InterruptedException {
        bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                ChannelHandler[] acceptorHandlers = new ChannelHandler[3];
                acceptorHandlers[0] = new WebSocketChannelInitializer();
                socketChannel.pipeline().addLast(acceptorHandlers);
            }
        });
        //bind 绑定端口，可以绑定多个端口
        //sync 开启监听，返回ChannelFuture，代表监听成功后的一个对应的未来结果
        //可以使用此对象实现后续的服务器和客户端的交互
        return bootstrap.bind(port);
    }

    /**
     * shutdownGracefully 安全关闭方法，保证不抛弃任何一个已接收的客户端请求
     */
    public void release(){
        this.acceptorGroup.shutdownGracefully();
        this.clientGroup.shutdownGracefully();
    }

    public static void run() {
        ChannelFuture future = null;
        Server4Client server = null;
        try {
            server = new Server4Client();
            future = server.doAccept(3000);
            System.out.println("server for client start");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            if(null!=future){
                try {
                    future.channel().closeFuture().sync();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(null!=server){
                server.release();
            }
        }
    }
}
