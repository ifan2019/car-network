package com.xmmufan.carnetwork.netty;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xmmufan.carnetwork.entity.monitor.CarInfo;
import com.xmmufan.carnetwork.repository.dao.CarInfoDao;
import com.xmmufan.carnetwork.util.CarMap;
import com.xmmufan.carnetwork.util.UtilMap;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.util.*;
@Component
@Slf4j
public class ReceiveQueen {

    @Autowired
    private CarInfoDao carInfoDao;

//    @JmsListener(destination = "hardware3")
    public void receiveQueue(TextMessage obj) {
        String msg = null;
        Map<String,Map<String,String>> map=new HashMap<>(0);
        Map<String,String> invertMap=null;
        String invertSign[]={"outputVoltage","outputCurrent","outputPower","chargingCurrent","aea"};
//,"waterPump","solutionTank","greyTank","blakTank"
        try {
            msg = obj.getText();
            if (msg != null) {
                   //获取消息前的设别标识
                String sequenceSign=msg.substring(0,4);
                //具体数据
                String data=msg.substring(4);
                System.out.println("data===="+data);
                JSONObject jsonObject= JSON.parseObject(data);
                Map<String,Object> map1=jsonObject.getInnerMap();
                Map<String,Object> map2=jsonObject.getJSONObject("invert").getInnerMap();
                System.out.println("object=="+map2.size());
                System.out.println("value===="+map2.get("outputVoltage"));
                for (int i=0;i<invertSign.length;i++){
                    invertMap=new HashMap<>(0);
                    Object value=map2.get(invertSign[i]);
                    invertMap.put(invertSign[i],value.toString());
                }
                System.out.println("invertMap===="+invertMap);

                //获取所有的车辆id和设备标识
                List<CarInfo> carInfoList=carInfoDao.selectCarInfo();
                System.out.println("list=="+carInfoList.size());
                int spiltLength=10;

                   //遍历所有的车辆信息，匹配设备标识并找到通道
                   for (CarInfo info:carInfoList){
                        log.info("info.getCarSequenceSign().equals(sequenceSign)={}",(info.getCarSequenceSign().equals(sequenceSign)) );
                       if(info.getCarSequenceSign().equals(sequenceSign)){
                           for (String list:getKeyByCarId(info.getId().toString())){
                               System.out.println("abc");
                               Channel channel=UtilMap.getChannelByName(list);
                               log.info("list = {} channel = {}",list,  channel);
                               //写入数据到通道
                               if(data.length()>spiltLength){
                                   System.out.println("进入判断");
                                   for (int i=0;i<data.length()/spiltLength+1;i++){
                                       String str="";
                                       if(i==data.length()/spiltLength){
                                           str=data.substring(i*spiltLength);
                                           str=str+"fe";
                                       }else{
                                           str=data.substring(i*spiltLength,spiltLength*(i+1));
                                       }
                                      channel.writeAndFlush(new TextWebSocketFrame(str));
                                       log.info("str====="+str);
                                   }
                               }
                               channel.writeAndFlush(new TextWebSocketFrame(data));
                           }
                       }
                   }
               }
        } catch (JMSException e) {
            System.out.println("JMSException==================");
        }
        try {
            obj.acknowledge();
        } catch (Exception e) {
            System.out.println("MQ acknowledge error");
        }
    }

    private List<String> getKeyByCarId(String carId){
        String str="";
        List<String> keyList=new ArrayList<>();
        Map<String,String> map=CarMap.getCarMap();
        for (Object object : map.entrySet()) {
            str = object.toString();
            if (carId.equals(str.substring(str.indexOf("=") + 1, str.length()))) {
                keyList.add(str.substring(0, str.lastIndexOf("=")));
            }
        }
        return keyList;
    }

}
