package com.xmmufan.carnetwork.netty;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

public class Server4Hardware {

    /**
     * 监听线程组，监听客户端请求
     */
    private EventLoopGroup acceptorGroup;
    /**
     * 处理客户端相关操作线程组，负责处理与客户端的数据通讯
     */

    private EventLoopGroup clientGroup;

    private ServerBootstrap httpBootStrap;

    public Server4Hardware(){
        init();
    }

    private void init() {
        acceptorGroup = new NioEventLoopGroup();
        clientGroup = new NioEventLoopGroup();
        httpBootStrap = new ServerBootstrap();
        //绑定线程组
        httpBootStrap.group(acceptorGroup,clientGroup);
        //设置通讯模式为NIO
        httpBootStrap.channel(NioServerSocketChannel.class);
        //设置缓冲区大小
        httpBootStrap.option(ChannelOption.SO_BACKLOG,1024);
        //SO_SNDBUF 发送缓冲区，SO_RCVBUF接收缓冲区，
        //SO_KEEPALIVE 开启心跳监测（保证连接有效）
        httpBootStrap.option(ChannelOption.SO_SNDBUF,16*1024)
                .option(ChannelOption.SO_RCVBUF,16*1024)
                .option(ChannelOption.SO_KEEPALIVE,true);
    }

    /**
     * 监听处理逻辑
     * @param port 监听端口
     * @return ChannelFuture
     * @throws InterruptedException
     */
    public ChannelFuture doAccept(int port) throws InterruptedException {
        /**
         * childHandler 使bootstrap 独有的方法，是用于提供处理对象的
         * 可以一次性添加若干个处理逻辑，是类似责任链模式处理方式。
         * 增加A，B两个处理逻辑，在处理客户端请求数据的时候，根据A->B 顺序依次处理
         * ChannelInitializer 用于提供处理器的模型对象
         *  其中定义了 initChannel方法，用于初始化处理逻辑责任链的。
         *  可以保证服务端的bootstrap只初始化一次处理器，提供处理逻辑的重用，避免反复创建处理器对象，节约资源
         */
        httpBootStrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                ChannelPipeline pipeline = socketChannel.pipeline();
                pipeline.addLast(new Server4HardwareHandler());
//                pipeline.addLast(new WebSocketChannelInitializer());
            }
        });

        //bind 绑定端口，可以绑定多个端口
        //sync 开启监听，返回ChannelFuture，代表监听成功后的一个对应的未来结果
        //可以使用此对象实现后续的服务器和客户端的交互

        return httpBootStrap.bind(port).sync();
    }

    /**
     * shutdownGracefully 安全关闭方法，保证不抛弃任何一个已接收的客户端请求
     */
    public void release(){
        this.acceptorGroup.shutdownGracefully();
        this.clientGroup.shutdownGracefully();
    }

    public static void run() {
        ChannelFuture future1 = null;
        ChannelFuture future2 = null;
        Server4Hardware server1 = null;
        Server4Client server2 = null;

        try {
            server1 = new Server4Hardware();
            future1 = server1.doAccept(9999);

            server2 = new Server4Client();
            future2 = server2.doAccept(3000);

            System.out.println("server started");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            if(null!=future1){
                try {
                    future1.channel().closeFuture().sync();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(null!= future2){
                try {
                    future2.channel().closeFuture().sync();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(null!=server1){
                server1.release();
            }
            if(null!= server2){
                server2.release();
            }
        }
    }


}
