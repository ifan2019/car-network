package com.xmmufan.carnetwork.netty;

import com.xmmufan.carnetwork.util.ParseHelper;
import com.xmmufan.carnetwork.util.RedisUtil;
import com.xmmufan.carnetwork.util.Util;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 * 服务端处理逻辑
 * Sharable注解
 *  代表当前handler是一个可以分享的处理器，意味着，服务器注册此handler之后，可以分享给多个客户端使用，
 *  如果不加此注解，每次客户端请求，都需要为这个客户端重新创建一个新的handler对象。
 *  如果handler是sharable的，避免定义可写的实例变量
 *  bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
 *  protected void initChannel(SocketChannel socketChannel) throws Exception {
 *          socketChannel.pipeline().addLast(new XxxHandler());
 *      }
 *  })
 * @author ifan
 */
@Sharable
@Component
@Slf4j
public class Server4HardwareHandler extends ChannelInboundHandlerAdapter {

    private static Server4HardwareHandler server4HardwareHandler;
    private static final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    @Autowired
    private JmsTemplate jmsTemplate;



    public Server4HardwareHandler(){

    }

    @PostConstruct
    public void init() {
        server4HardwareHandler = this;
    }

    /**
     * 用户处理读取数据请求的逻辑
     * @param ctx 包含与客户端建立连接的资源，如： 对应的channel
     * @param msg 读取到的数据，默认类型为ByteBuf.是对ByteBuffer的封装
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx,Object msg) throws Exception {
        channels.add(ctx.channel());
        String channelId = ctx.channel().id().asLongText();
        ByteBuf readBuffer = (ByteBuf) msg;
        byte[] tempDatas = new byte[readBuffer.readableBytes()];
        readBuffer.readBytes(tempDatas);
        server4HardwareHandler.jmsTemplate.setReceiveTimeout(1);
        StringBuilder all = new StringBuilder("");
        byte b = tempDatas[0];
        int date = b&0xff;
        String s;
        for (byte tempData : tempDatas) {
            date = tempData & 0xff;
            if(date < 0x10){
                s = "0"+Integer.toHexString(date);
            }else{
                s = Integer.toHexString(date);
            }
            //Integer.toHexString(i)
            all.append(s);
        }
        log.info("接收到的数据{}", all);
        if("41542b4349505354415455530d".equals(all.toString())){
            return;
        }
        //起始位置，判断是否为fe
        //如果相等，说明发送的是数据开头
        if(all.toString().startsWith("fe")){
            //取出包长度
            int packageLengthInt = Integer.parseInt(all.substring(2,6),16);
            if(tempDatas.length - 4 == packageLengthInt){
                // 发送的为完整数据
                log.info("接受到的完整数据为 {}" , all);
                //直接进行解析
                String parsing = ParseHelper.parse(all.toString(),ctx.channel());
                if(!"".equals(parsing)){
                    log.info("返回的给硬件的数据是：{}",parsing);
                    byte[] bytes = Util.hexStringToByteArray(parsing.toLowerCase());
                    ctx.writeAndFlush(Unpooled.copiedBuffer(bytes));
                }
            }else if(tempDatas.length - 4 < packageLengthInt){
                log.info("小于包长度,放入mq中的为{}" , all );
                //小于包长度，说明二次发送的数据
                //添加到mq中
                server4HardwareHandler.jmsTemplate.convertAndSend(channelId,all);
            }else if(tempDatas.length - 4 > packageLengthInt){
                String[] datas = all.toString().split("fe");
                for (String temp : datas) {
                    StringBuilder sb = new StringBuilder(temp);
                    if(!"".equals(temp)){
                        StringBuilder data = sb.insert(0, "fe").append("fe");
                        String parsing = ParseHelper.parse(data.toString(), ctx.channel());
                        if(!"".equals(parsing)){
                            log.info("返回的给硬件的数据是：{}",parsing);
                            byte[] bytes = Util.hexStringToByteArray(parsing.toLowerCase());
                            ctx.writeAndFlush(Unpooled.copiedBuffer(bytes));
                        }
                    }
                }
            }
        } else if(all.toString().substring(2).startsWith("fe")){
            String str = all.substring(2);
            //取出包长度
            int packageLengthInt = Integer.parseInt(str.substring(2,6),16);
            if(tempDatas.length - 4 == packageLengthInt){
                // 发送的为完整数据
                log.info("接受到的完整数据为 {}" , str);
                //直接进行解析
                String parsing = ParseHelper.parse(str,ctx.channel());
                if(!"".equals(parsing)){
                    log.info("返回的给硬件的数据是：{}",parsing);
                    byte[] bytes = Util.hexStringToByteArray(parsing.toLowerCase());
                    ctx.writeAndFlush(Unpooled.copiedBuffer(bytes));
                }
            }else if(tempDatas.length - 4 < packageLengthInt){
                log.info("小于包长度,放入mq中的为{}" , str );
                //小于包长度，说明二次发送的数据
                //添加到mq中
                server4HardwareHandler.jmsTemplate.convertAndSend(channelId,all);
            }
        }else{
            log.info("不是fe开头");
            //不是fe开头，说明为上次数据的结尾
            //先取出
            Object payload = server4HardwareHandler.jmsTemplate.receiveAndConvert(channelId);
            //payload 为mq中上次的数据流
            //说明发送的信息为以下情况 1. 发送的数据为xxxfexxx
            String lastMessage = "";
            if(payload != null){
                lastMessage = payload.toString();
            }else{
                int feIndex = all.toString().indexOf("fe");
                if(feIndex != all.toString().length() -2){
                    log.info("fe前莫名带东西");
                    all = new StringBuilder(all.substring(feIndex));
                }
            }
            //拼接成本次的伪完整数据
            String temp = lastMessage + all;
            //判断本次的伪完整数据是否是真完整数据
            int packageLength = Integer.parseInt(temp.substring(2,6),16);
            //两个两一组，判断总长度是否为符合包长度的长度
            if(temp.length()/2 - 4  == packageLength){
                //数据完整，进行解析
                log.info("接收到的数据为 {}", temp);
                String parsing = ParseHelper.parse(temp,ctx.channel());
                if(!"".equals(parsing)){
                    log.info("返回给硬件的数据是{}：" , parsing);
                    byte[] bytes = Util.hexStringToByteArray(parsing.toLowerCase());
                    ctx.writeAndFlush(Unpooled.copiedBuffer(bytes));
                }
            }else{
                log.info("不符合长度");
                if(!temp.startsWith("fe")){
                    log.info("不是fe");
                    return;
                }
                //不符合，等待下次发送数据
                server4HardwareHandler.jmsTemplate.convertAndSend(channelId,temp);
            }
        }
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        System.out.println(cause.getCause().getMessage());
        System.out.println(cause.getMessage());
        System.out.println(simpleDateFormat.format(new Date()) + " server exceptionCaught method run ..... ");
        ctx.close();
    }
}
