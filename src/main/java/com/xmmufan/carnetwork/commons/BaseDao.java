package com.xmmufan.carnetwork.commons;


import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * myBatis 通用mapper 父接口 mybatis的dao层只需要extends 此接口，即可获取一些常用的crud方法
 * @param <T>
 * @author Mr ifan/詹奕凡
 * @date 2019/5/19
 * @version 1.0
 */
public interface BaseDao<T> extends Mapper<T>, MySqlMapper<T> {

}
