package com.xmmufan.carnetwork.service;

import java.util.HashMap;

/**
 * @author  陈丽
 * @date    2019-5-25
 */
public interface CarStatusService {
    HashMap<String, Object> getCarStatus(int carId);


    HashMap<String, Object> getCarStatusAndSafe();
}
