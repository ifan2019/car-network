package com.xmmufan.carnetwork.service;


import com.xmmufan.carnetwork.entity.monitor.CarInfo;
import com.xmmufan.carnetwork.entity.monitor.UserB;
import com.xmmufan.carnetwork.entity.monitor.UserC;
import com.xmmufan.carnetwork.entity.monitor.UserF;
import com.xmmufan.carnetwork.entity.vo.CarInfoVo;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author 陈丽
 * @version 1.0
 * @date 2019/5/23
 */
public interface CarInfoService {
    List<Map> getCarInfo(UserB userB);

    Map<String,Object> getCarInfoList(Long currentAdminId);

    int addCarInfo(CarInfo carInfo,Long cityId,Long carTypePriceId, Long currentAdminId);

    Map<String, Object> getCarInfoByCondition(Long id,String message);

    boolean deleteCarIfo(CarInfo carInfo);

    boolean updateCarInfo(CarInfo carInfo,Long cityId,Long carTypePriceId, Long currentAdminId);

    List<Map> getWeb_bCarInfoByCondition(int id, String message);

    List<Map<String,String>> getCountByChassis(Long currentAdminId);

    Map<String, Object> getByCountByLeaseStatus(Long currentAdminId);

    CarInfoVo getCarInfoById(Long id);
}
