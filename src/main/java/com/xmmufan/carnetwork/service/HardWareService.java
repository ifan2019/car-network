package com.xmmufan.carnetwork.service;

import java.util.List;
import java.util.Map;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/7/29
 */
public interface HardWareService {


    List<Map> query(String startAddress, String endAddress, String commandType, String data,String carVin);

    int control(String address, String startAddress, String commandStatus,String carVin);

    int config(String address, String data, String carVin);

    Map<String,Object> getPageData(String carVin);

}

