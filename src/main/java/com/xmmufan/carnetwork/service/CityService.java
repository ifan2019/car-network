package com.xmmufan.carnetwork.service;

import com.xmmufan.carnetwork.entity.monitor.City;

import java.util.List;
import java.util.Set;

public interface CityService {

    Set<City> getCityByCarAdminId(Long currentAdminId);
}
