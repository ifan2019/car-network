package com.xmmufan.carnetwork.service;


import java.util.Map;

/**
 * @author  陈丽
 * @date    2019-5-26
 */
public interface RentService{
    Map<String, Object> getRentInfoList(Long carAdminId);

    Map<String, Object> getRentInfoByCondition(Long id, String message);
}
