package com.xmmufan.carnetwork.service;

import com.xmmufan.carnetwork.entity.monitor.UserB;
import com.xmmufan.carnetwork.entity.vo.UserBInfoVo;

import java.util.List;

/**
 * @author Mr.ifan/詹奕凡
 * @date 2019/6/13
 * @version 1.0
 */
public interface UserBService {
    /**
     * 根据username查询用户
     * @param username
     * @return
     */
    UserB findByUsername(String username);

    List<UserBInfoVo> getAllUserB();
}
