package com.xmmufan.carnetwork.service;

import java.util.Map;

/**
 * @author  陈丽
 * @date    2019-6-2
 */
public interface CarTypeService {
    Map<String, Object> getListCarType();
}
