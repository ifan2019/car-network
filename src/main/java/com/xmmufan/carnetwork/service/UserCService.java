package com.xmmufan.carnetwork.service;

import com.xmmufan.carnetwork.entity.monitor.Order;

public interface UserCService {

    Order getUserRentInfo(String telephone);
}
