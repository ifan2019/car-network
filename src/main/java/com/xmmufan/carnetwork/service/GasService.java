package com.xmmufan.carnetwork.service;

import com.xmmufan.carnetwork.entity.monitor.CarInfo;

import java.util.HashMap;
import java.util.Map;
/**
 * @Author 陈丽
 * @Date   2019/5/24
 *
 */
public interface GasService {
  

    Map<String, Object> getAppCeletricparam(int carId);

    Map<String, Object> getWebBeletricparam(CarInfo carInfo);

    HashMap<String, Object> getEletricSystem();
}
