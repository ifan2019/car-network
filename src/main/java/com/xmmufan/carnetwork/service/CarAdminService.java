package com.xmmufan.carnetwork.service;


import com.xmmufan.carnetwork.entity.monitor.Admin;

public interface CarAdminService {

    /**
     * 根据username查询用户
     * @param username
     * @return
     */
    Admin findByUsername(String username);
}
