package com.xmmufan.carnetwork.service;

import com.xmmufan.carnetwork.entity.monitor.Operating;

import java.util.Map;

/**
 * @author   陈丽
 * @date     2019-5-25
 */
public interface OperatingService {
    Map<String, Object> getOperatingInfo(int carId);

    Map<String, Object> getUserSafeOrOperating();

    Map<String, Object> updateOperating(String currentStatus, int controlId);
}
