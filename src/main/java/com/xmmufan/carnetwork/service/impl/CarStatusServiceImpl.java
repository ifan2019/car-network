package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.service.CarStatusService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
/**
 * @author 陈丽
 * @date   2019-5-25
 */
@Service
public class CarStatusServiceImpl implements CarStatusService {


    @Override
    public HashMap<String, Object> getCarStatus(int carId) {
        HashMap<String, Object> map=new HashMap<>(0);

        //胎压监测
        HashMap<String,Object> tirePressure=new HashMap<>(0);
        tirePressure.put("frontLeftTyre",2.6);
        tirePressure.put("frontRightTyre",2.6);
        tirePressure.put("backLeftTyre",36.0);
        tirePressure.put("backRightTyre",2.6);

        //三轴姿态
        HashMap<String,Object> threeAxisAttitude=new HashMap<>(0);
        threeAxisAttitude.put("headingAngle",90.0);
        threeAxisAttitude.put("yawAngle",90.0);
        threeAxisAttitude.put("centroidSideslipAngle",90.0);

        map.put("licentsePlate","闽XXXXXX");
        map.put("tirePressure",tirePressure);
        map.put("threeAxisAttitude",threeAxisAttitude);
        return map;
    }



    @Override
    public HashMap<String, Object> getCarStatusAndSafe() {
        HashMap<String,Object> map=new HashMap<>(0);
        HashMap<String,Object> tirePressureMonitoring=new HashMap<>(0);
        HashMap<String,Object>leftFrontTire=new HashMap<>(0);
        leftFrontTire.put("Threshold",26);
        leftFrontTire.put("currentValue",2.6);

        HashMap<String,Object> rightFrontTire=new HashMap<>(0);
        rightFrontTire.put("Threshold",26);
        rightFrontTire.put("currentValue",2.6);

        HashMap<String,Object> leftRearTire=new HashMap<>(0);
        leftRearTire.put("Threshold",26);
        leftRearTire.put("currentValue",2.6);

        HashMap<String,Object> rightRearTire=new HashMap<>(0);
        rightRearTire.put("Threshold",26);
        rightRearTire.put("currentValue",2.6);

        tirePressureMonitoring.put("leftFrontTire",leftFrontTire);
        tirePressureMonitoring.put("rightFrontTire",rightFrontTire);
        tirePressureMonitoring.put("leftRearTire",leftRearTire);
        tirePressureMonitoring.put("rightRearTire",rightRearTire);
        HashMap<String,Object> triaxialAttitude=new HashMap<>(0);
        triaxialAttitude.put("heading",90);
        triaxialAttitude.put("yawAngle",90);
        triaxialAttitude.put("centroidSideAngle",90);
        map.put("licensePlate","闽Dxxxxx");
        map.put("speed",60);
        map.put("currentPosition","厦门大云房车");
        map.put("tirePressureMonitoring",tirePressureMonitoring);
        map.put("triaxialAttitude",triaxialAttitude);
        return map;
    }
}
