package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.entity.monitor.Order;
import com.xmmufan.carnetwork.repository.dao.RentDao;
import com.xmmufan.carnetwork.repository.jpa.AdminRepository;
import com.xmmufan.carnetwork.repository.jpa.RentRepository;
import com.xmmufan.carnetwork.service.RentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author  陈丽
 * @date    2019-5-26
 */
@Service
public class RentServiceImpl implements RentService {
    @Autowired
    private RentDao rentDao;

    @Autowired
    private RentRepository rentRepository;

    @Autowired
    private AdminRepository carAdminRepository;

    @Override
    public Map<String, Object> getRentInfoList(Long currentAdminId) {
        HashMap<String,Object> map=new HashMap<>(0);
        Long id = carAdminRepository.getOne(currentAdminId).getUserF().getId();
        List<Order> rentList = rentRepository.findAllByCarInfoUserFIdAndCarInfoLeaseStatusAndOrderStatusIn(id,"租赁", Arrays.asList("0","4","5"));
        List<Map> rentalmessages=new ArrayList<>();
        HashMap<String,Object> rentalmessagesInfo;
        for(Order rent:rentList){
            rentalmessagesInfo=new HashMap<>(0);
            rentalmessagesInfo.put("key",rent.getId());
            rentalmessagesInfo.put("orderNumber",rent.getOrderNumber());
            rentalmessagesInfo.put("VIN",rent.getCarInfo().getCarVin());
            rentalmessagesInfo.put("licensePlate",rent.getCarInfo().getLicentsePlate());
            rentalmessagesInfo.put("leasePerson",rent.getUserC().getUserName());
            rentalmessagesInfo.put("idCard",rent.getUserC().getUserIdCard());
            rentalmessages.add(rentalmessagesInfo);
        }
        map.put("rentalmessages",rentalmessages);
        return map;
    }

    @Override
    public Map<String, Object> getRentInfoByCondition(Long id, String message) {
        HashMap<String, Object> map = new HashMap<>(0);
        List<Map> rentInfoList=new ArrayList<>();
        HashMap<String,Object> rentInfoMap;
        try {
            if(message!=null){
                List<Order> rentInfos = rentDao.selectByCondition(carAdminRepository.getOne(id).getUserF().getId(),message);
                for (Order rentInfo:rentInfos) {
                    rentInfoMap=new HashMap<>(0);
                    rentInfoMap.put("orderNumber",rentInfo.getOrderNumber() );
                    rentInfoMap.put("VIN",rentInfo.getCarInfo().getCarVin());
                    rentInfoMap.put("licensePlate",rentInfo.getCarInfo().getLicentsePlate());
                    rentInfoMap.put("leasePerson",rentInfo.getUserC().getUserName());
                    rentInfoMap.put("idCard",rentInfo.getUserC().getUserIdCard());
                    rentInfoList.add(rentInfoMap);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        map.put("carInfos",rentInfoList);
        return map;

    }
}
