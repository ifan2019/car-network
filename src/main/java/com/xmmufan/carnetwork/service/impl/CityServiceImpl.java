package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.entity.monitor.Admin;
import com.xmmufan.carnetwork.entity.monitor.City;
import com.xmmufan.carnetwork.entity.monitor.UserF;
import com.xmmufan.carnetwork.repository.jpa.AdminRepository;
import com.xmmufan.carnetwork.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class CityServiceImpl implements CityService {
    @Autowired
    private AdminRepository carAdminRepository;


    @Override
    public Set<City> getCityByCarAdminId(Long currentAdminId){
        try{
            Admin carAdmin = carAdminRepository.getOne(currentAdminId);
            UserF userF = carAdmin.getUserF();
            return new HashSet<>(userF.getCityList());
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
