package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.entity.monitor.Device;
import com.xmmufan.carnetwork.repository.DeviceRepository;
import com.xmmufan.carnetwork.service.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/8/13
 */
@Service
@Slf4j
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Override
    public List<Device> getAllDevice() {
        List<Device> devices;
        try {
            devices = deviceRepository.findAll();
        } catch (Exception e) {
            log.error("获取所有的设备错误!" + e.getMessage());
            return null;
        }
        return devices;
    }
}
