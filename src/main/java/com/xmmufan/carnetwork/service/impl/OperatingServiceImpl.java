package com.xmmufan.carnetwork.service.impl;
import com.xmmufan.carnetwork.entity.monitor.Operating;
import com.xmmufan.carnetwork.repository.dao.OperatingDao;
import com.xmmufan.carnetwork.repository.jpa.OperatingRepository;
import com.xmmufan.carnetwork.service.OperatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author   陈丽
 * @date     2019-5-25
 */
@Service
public class OperatingServiceImpl implements OperatingService {
    @Autowired
    private OperatingDao operatingDao;

    @Autowired
    private OperatingRepository operatingRepository;

    @Override
    public Map<String, Object> getOperatingInfo(int carId) {
        List<Operating> operatingList=operatingDao.selectOperatingByCarId(carId);
        HashMap<String,Object> map=new HashMap<>(0);
        //灯光控制
        List<Map<String,Object>> lightControl=new ArrayList<>();
        HashMap<String,Object> lightInfo;
        //情景控制
        List<Map<String,Object>> scenarioMode=new ArrayList<>();
        HashMap<String,Object> scenarioInfo;
        for (Operating operating:operatingList) {
            if (operating.getOperatingType().getTypeName().equals("灯光控制")) {
                lightInfo = new HashMap<>(0);
                lightInfo.put("controlId", operating.getId());
                lightInfo.put("lightStatusName",operating.getStatusName());
                lightInfo.put("lightCurrentStatus", operating.getCurrentStatus());
                lightControl.add(lightInfo);
            }
            if(operating.getOperatingType().getTypeName().equals("情景控制")){
                scenarioInfo=new HashMap<>(0);
                scenarioInfo.put("controlId",operating.getId());
                scenarioInfo.put("scenarioStatusName", operating.getStatusName());
                scenarioInfo.put("scenarioCurrentStatus",operating.getCurrentStatus());
                scenarioMode.add(scenarioInfo);
            }
        }
        //负载状态
        HashMap<String,Object> loadState=new HashMap<>(0);
        loadState.put("maximumBearingWeight",100);
        loadState.put("nuclearLoading",10);

        //环境系统
        HashMap<String,Object> environmentalSystem=new HashMap<>(0);
        environmentalSystem.put("co",10);
        environmentalSystem.put("co2",10);
        environmentalSystem.put("vehicleTemperature",10);
        environmentalSystem.put("outsideTemperature",10);
        map.put("lightControl",lightControl);
        map.put("loadState",loadState);
        map.put("scenarioMode",scenarioMode);
        map.put("environmentalSystem",environmentalSystem);
        return map;
    }

    @Override
    public Map<String, Object> updateOperating(String currentStatus,int controlId) {
        Operating operating = operatingRepository.getOne(controlId);
        operating.setCurrentStatus(currentStatus+"");
        operating = operatingRepository.save(operating);
//        Operating operating1 = operatingDao.selectOne(operating);
//        System.out.println(operating1);
        HashMap<String,Object> map=new HashMap<>(0);
        map.put("controlId",operating.getId());
        map.put("currentStatus",operating.getCurrentStatus());
        return map;
    }

    @Override
    public Map<String, Object> getUserSafeOrOperating() {
        HashMap<String,Object> map=new HashMap<>(0);
        //智能控制
        List<Map> intelligentControl=new ArrayList<>();
        HashMap<String,Object> intelligentControlInfo1=new HashMap<>(0);
        intelligentControlInfo1.put("intelligentControlName","防盗模式");
        intelligentControlInfo1.put("intelligentControlStatus",false);
        HashMap<String,Object> intelligentControlInfo2=new HashMap<>(0);
        intelligentControlInfo2.put("intelligentControlName","季节模式");
        intelligentControlInfo2.put("intelligentControlStatus",true);
        intelligentControl.add(intelligentControlInfo1);
        intelligentControl.add(intelligentControlInfo2);
        //环境系统
        HashMap<String,Object> environmentalSystem=new HashMap<>(0);
        environmentalSystem.put("CO",20);
        environmentalSystem.put("CO2",100);
        map.put("intelligentControl",intelligentControl);
        map.put("environmentalSystem",environmentalSystem);
        return map;
    }
}
