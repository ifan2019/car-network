package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.entity.monitor.CarInfo;
import com.xmmufan.carnetwork.repository.dao.CarInfoDao;
import com.xmmufan.carnetwork.repository.jpa.CarInfoRepository;
import com.xmmufan.carnetwork.service.GasService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * @author 陈丽
 * @date   2019/5/24
 *
 */
@Service
@Slf4j
public class GasServiceImpl implements GasService {

    @Autowired
    private CarInfoRepository carInfoRepository;
    @Override
    public HashMap<String,Object> getAppCeletricparam(int carId) {
        HashMap<String,Object> map=new HashMap<>(0);
        HashMap<String,Object> invert=new HashMap<>(0);
        HashMap<String,Object> tank=new HashMap<>(0);
        HashMap<String,Object> waterPump=new HashMap<>(0);
        HashMap<String,Object> lithiumBattery=new HashMap<>(0);
        map.put("dunamo",100);
        map.put("energy",100);
        invert.put("inverterStatus",1);
        invert.put("inverterMaintain","转化器需要维护");
        invert.put("invertValue",100);
        map.put("inverter",invert);
        tank.put("wasteWaterTankStatus",0);
        tank.put("solutionTankStatus",1);
        tank.put("tankMaintain",null);
        map.put("tank",tank);
        waterPump.put("waterPumpStatus",0);
        waterPump.put("waterMaintain",null);
        map.put("waterPump",waterPump);
        lithiumBattery.put("lithiumBatteryCycle",100);
        map.put("lithiumBattery",lithiumBattery);
        return map;
    }


    @Override
    public HashMap<String, Object> getWebBeletricparam(CarInfo carInfo) {
        HashMap<String,Object> map=new HashMap<>(0);
//        CarInfo carInfo1=new CarInfo();
//        carInfo1.setCarVin(carInfo.getCarVin());
        try{
            List<CarInfo> carInfos= carInfoRepository.getCarInfoByCarStatusAndCarVin("true", carInfo.getCarVin());
            if(carInfos.size()==0){
                return null;
            }
            //发电量数据
            List<Object> generationCapacity=new ArrayList<>();
            HashMap<String,Object> generationCapacityInfo=new HashMap<>(0);
            //煤气桶发电量数据
            generationCapacityInfo.put("name","发电机");
            generationCapacityInfo.put("number",100.0);
            generationCapacity.add(generationCapacityInfo);
            //能量使用数据
            List<Object> energyUse=new ArrayList<>();
            HashMap<String,Object> energyUserInfo=new HashMap<>(0);
            //空调能量使用数据
            energyUserInfo.put("name","空调");
            energyUserInfo.put("number",100.0);
            energyUse.add(energyUserInfo);
            //锂电池循环使用数据
            List<Object> lithiumBatteryCycle=new ArrayList<>();
            HashMap<String,Object> lithiumBatteryCycleInfo=new HashMap<>(0);
            lithiumBatteryCycleInfo.put("time","2019-5-25");
            lithiumBatteryCycleInfo.put("status","正常");
            lithiumBatteryCycle.add(lithiumBatteryCycleInfo);
            map.put("generationCapacity",generationCapacity);
            map.put("energyUse",energyUse);
            map.put("lithiumBatteryCycle",lithiumBatteryCycle);
            //逆变器实时使用速率
            map.put("inverterRealTime",100.00);
        }catch (Exception e){
            log.error("Vin不存在");
        }
        return map;
    }

    @Override
    public HashMap<String, Object> getEletricSystem() {
        HashMap<String,Object> map=new HashMap<>(0);
        HashMap<String,Object> invert=new HashMap<>(0);
        HashMap<String,Object> tank=new HashMap<>(0);
        HashMap<String,Object> waterPump=new HashMap<>(0);
        HashMap<String,Object> lithiumBattery=new HashMap<>(0);
        invert.put("inverterStatus",0);
        invert.put("outputPower","0.0");
        invert.put("outputVoltage",0.0);
        invert.put("chargeCurrent",0.0);
        invert.put("outputCurrent",0.0);
        invert.put("aea",16);
        map.put("inverter",invert);
        tank.put("clearWater",15);
        tank.put("greyWater",18);
        tank.put("blackWater",10);
        map.put("tank",tank);
        waterPump.put("waterPumpStatus",0);
        waterPump.put("waterMaintain",null);
        map.put("waterPump",waterPump);
        return map;
    }
}
