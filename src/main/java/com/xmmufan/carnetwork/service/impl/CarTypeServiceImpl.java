package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.entity.monitor.Car;
import com.xmmufan.carnetwork.entity.monitor.CarType;
import com.xmmufan.carnetwork.repository.dao.CarTypeDao;
import com.xmmufan.carnetwork.service.CarTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author  陈丽
 * @date    2019-6-2
 */
@Service
public class CarTypeServiceImpl implements CarTypeService {
    @Autowired
    private CarTypeDao carTypeDao;


    @Override
    public Map<String, Object> getListCarType() {
        HashMap<String,Object> map=new HashMap<>(0);
        List<CarType> carTypeList=carTypeDao.selectAll();
        List<Map> rvType=new ArrayList<>();
        HashMap<String,Object> rvTypeInfo;
        for (CarType carType :carTypeList){
            rvTypeInfo=new HashMap<>(0);
            rvTypeInfo.put("RVTypeId",carType.getId());
            rvTypeInfo.put("RVTypeName",carType.getCarTypeName()+carType.getCarModel());
            rvType.add(rvTypeInfo);
        }
        map.put("rvType",rvType);

        return map;
    }
}
