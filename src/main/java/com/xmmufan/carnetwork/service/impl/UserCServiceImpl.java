package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.entity.monitor.Order;
import com.xmmufan.carnetwork.repository.jpa.RentRepository;
import com.xmmufan.carnetwork.service.UserCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Mr.ifan/詹奕凡
 * @date 2019/6/13
 * @version 1.0
 */
@Service
public class UserCServiceImpl implements UserCService {

    @Autowired
    private RentRepository rentRepository;


    @Override
    public Order getUserRentInfo(String telephone){
        Order rent;
        try {
            rent = rentRepository.getRentByUserCUserTel(telephone);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return rent;
    }
}
