package com.xmmufan.carnetwork.service.impl;
import com.xmmufan.carnetwork.entity.vo.CarInfoVo;
import com.xmmufan.carnetwork.repository.CityRepository;
import com.xmmufan.carnetwork.repository.jpa.CarDeviceRepository;
import com.xmmufan.carnetwork.entity.monitor.*;
import com.xmmufan.carnetwork.repository.dao.CarInfoDao;
import com.xmmufan.carnetwork.repository.jpa.*;
import com.xmmufan.carnetwork.service.CarInfoService;
import com.xmmufan.carnetwork.util.IPFSUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

/**
 * @author 陈丽
 * @version 1.0
 * @date 2019/5/23
 */
@Service
@Slf4j
public class CarInfoServiceImpl implements CarInfoService {

    @Autowired
    private CarInfoDao carInfoDao;

    @Autowired
    private CarInfoRepository carInfoRepository;

    @Autowired
    private AdminRepository carAdminRepository;

//    @Autowired
//    private CarDao carDao;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CarTypeRepository carTypeRepository;

    @Autowired
    private UserBRepository userBRepository;

    @Autowired
    private CarDeviceRepository carDeviceRepository;

    @Autowired
    private IPFSUtil ipfsUtil;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private ImgUrlRepository imgUrlRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private CarTypePriceRepository carTypePriceRepository;
    private static final String FILEPATH = "https://ipfs.spectrumrpc.cn/ipfs/";


    @Override
    public List<Map> getCarInfo(UserB userB) {
        List<Map> mapList=new ArrayList<>();
        HashMap<String,Object> map;
        List<CarInfo> carInfos;
        HashMap<String,Object> basicInfor;
        HashMap<String,Object>electrical;
        try {
            carInfos=carInfoRepository.getCarInfoByUserBId(userB.getId());
            for (CarInfo info: carInfos) {
                map=new HashMap<>(0);
                basicInfor=new HashMap<>(0);
                electrical=new HashMap<>(0);
                System.out.println(info.getCarVin());
                System.out.println(info.getLicentsePlate());
                basicInfor.put("vin",info.getCarVin());
                basicInfor.put("licentsePlate",info.getLicentsePlate());

                basicInfor.put("fleetName",info.getUserF().getEnterprise());
                basicInfor.put("Type",info.getCar().getCarType());
                electrical.put("electricity","200w");
                electrical.put("energy","200w");
                electrical.put("lithiumBattery","200w");
                electrical.put("inverter","200w");
                map.put("basicInfor",basicInfor);
                map.put("electrical",electrical);
                mapList.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return mapList;
    }

    @Override
    public Map<String, Object> getCarInfoList(Long currentAdminId) {
        try{
            HashMap<String, Object> map = new HashMap<>(0);
            List<CarInfo> carInfoList = carInfoRepository.findAllByCarStatusAndUserF("true",carAdminRepository.getOne(currentAdminId).getUserF());
            System.out.println("carInfio==="+carInfoList.size());
            List<Map> carmessageslist = new ArrayList<>();
            HashMap<String, Object> carmessageslistInfo;
            for (CarInfo info : carInfoList) {
                carmessageslistInfo = new HashMap<>(0);
                carmessageslistInfo.put("key", info.getId());
                carmessageslistInfo.put("carID", info.getCar().getId());
                carmessageslistInfo.put("VIN", info.getCarVin());
                carmessageslistInfo.put("licensePlate", info.getLicentsePlate());
                CarTypePrice carTypePrice = getCarTypePrice(info);
                carmessageslistInfo.put("vehicleType", carTypePrice.getTypeName());
                carmessageslistInfo.put("vehicleTypeId",carTypePrice.getId());
                carmessageslistInfo.put("chassiscInformation", info.getChassisInfo());
                carmessageslistInfo.put("leaseStatus", info.getLeaseStatus());
                City city = info.getStore().getCity();
                carmessageslistInfo.put("cityId",city.getCityId());
                carmessageslistInfo.put("cityName",city.getCityName());
                carmessageslistInfo.put("vision",info.getCarSequenceSign());
                carmessageslist.add(carmessageslistInfo);
            }
            map.put("carmessageslist", carmessageslist);
            return map;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private CarTypePrice getCarTypePrice(CarInfo carInfo){
        System.out.println(carInfo.getId());
        Long carTypeId = carInfo.getCar().getCarType1().getId();
        List<Device> deviceList = carInfo.getDeviceList();
        System.out.println(carTypeId+"  "+deviceList.size());
        StringBuilder sb = new StringBuilder();
        deviceList.forEach(device -> {
            sb.append(device.getDeviceId()).append(",");
        });
        return carTypePriceRepository.getCarTypePriceByCarDeviceIdsAndCarTypeId(sb.toString(), carTypeId);
    }


    @Override
    public int addCarInfo(CarInfo carInfo ,Long cityId,Long carTypePriceId, Long currentAdminId) {
        try{
            List<CarInfo> carInfos1 = carInfoRepository.getCarInfoByCarStatusAndCarVin("true",carInfo.getCarVin());
            List<CarInfo> carInfos2=carInfoRepository.getCarInfoByCarStatusAndLicentsePlate("true",carInfo.getLicentsePlate());
            if(carInfos1.size()>=1&&carInfos2.size()>=1){
                return 3;
            }else if(carInfos1.size()>=1) {
                return 1;
            }else if(carInfos2.size()>=1){
                return 2;
            }
            Car car = new Car();
            CarTypePrice carTypePrice = carTypePriceRepository.getOne(carTypePriceId);
            CarType carType = carTypeRepository.getOne(carTypePrice.getCarTypeId());
            //ad car type
            car.setCarType(carType.getCarTypeName());
            car.setCarType1(carType);
            car = carRepository.save(car);
            carInfo.setCar(car);
            UserF userF = carAdminRepository.getOne(currentAdminId).getUserF();
            Store store = storeRepository.getStoreByCityCityIdAndUserFId(cityId,userF.getId());
            carInfo.setStore(store);
            carInfo.setLeaseStatus("未入库");
            carInfo.setCarStatus("true");
            carInfo.setUserF(userF);
            carInfo.setBrand(carTypePrice.getBrand());
            carInfo.setUserB(userBRepository.findUserBByBrand(carTypePrice.getBrand()));
            carInfoRepository.save(carInfo);
            // 添加设备
            String devices = carTypePrice.getCarDeviceIds();
            for(String deviceId: devices.split(",")){
                CarDevice carDevice = new CarDevice();
                Device device = new Device();
                device.setDeviceId(Long.parseLong(deviceId));
                carDevice.setDevice(device);
                carDevice.setCarInfo(carInfo);
                carDeviceRepository.save(carDevice);
            }
            return 4;
        }catch (Exception e){
            log.error("添加车辆信息错误");
            e.printStackTrace();
            return 0;
        }
    }


    @Override
    public Map<String, Object> getCarInfoByCondition(Long id,String message) {
        HashMap<String, Object> map = new HashMap<>(0);
        Set<Map> carInfoList=new LinkedHashSet<>();
        HashMap<String,Object> carInfoMap;
        try {
            if(message!=null){
//                List<CarInfo> carInfos = carInfoDao.selectByCondition(id,message);
                List<CarInfo> carInfos = carInfoRepository.selectByCondition(id,message);
                System.out.println(carInfos.size());
                for (CarInfo carInfo1:carInfos) {
                    carInfoMap=new HashMap<>(0);
                    carInfoMap.put("key", carInfo1.getCar().getId());
                    carInfoMap.put("VIN", carInfo1.getCarVin());
                    carInfoMap.put("licensePlate", carInfo1.getLicentsePlate());
                    carInfoMap.put("chassiscInformation", carInfo1.getChassisInfo());
                    CarTypePrice carTypePrice = getCarTypePrice(carInfo1);
                    carInfoMap.put("vehicleType", carTypePrice.getTypeName());
                    carInfoMap.put("vehicleTypeId",carTypePrice.getId());
                    City city = carInfo1.getStore().getCity();
                    carInfoMap.put("cityId",city.getCityId());
                    carInfoMap.put("cityName",city.getCityName());
                    carInfoMap.put("leaseStatus",carInfo1.getLeaseStatus());
                    carInfoMap.put("vision",carInfo1.getCarSequenceSign());
                    carInfoList.add(carInfoMap);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        map.put("carInfos",carInfoList);
        return map;

    }

    @Override
    public boolean deleteCarIfo(CarInfo carInfo) {
        try {
            if("租赁".equals(carInfo.getLeaseStatus())){
                log.info("不能删除已经租赁的车");
                return false;
            }else{
                carInfo = carInfoRepository.getOne(carInfo.getId());
                carInfo.setCarStatus(false + "");
                carInfoRepository.save(carInfo);
            }
        } catch (Exception e) {
            log.error("删除车辆信息失败!" + e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateCarInfo(CarInfo carInfo, Long cityId,Long carTypePriceId, Long currentAdminId) {
        try{
            // add car info
            CarInfo  carInfoDB = carInfoRepository.getOne(carInfo.getId());
            Car car = carInfoDB.getCar();
            CarTypePrice carTypePrice = carTypePriceRepository.getOne(carTypePriceId);
            CarType carType = carTypeRepository.getOne(carTypePrice.getCarTypeId());
            //ad car type
            car.setCarType(carType.getCarTypeName());
            car.setCarType1(carType);
            car = carRepository.save(car);
            carInfo.setCar(car);
            UserF userF = carAdminRepository.getOne(currentAdminId).getUserF();
            Store store = storeRepository.getStoreByCityCityIdAndUserFId(cityId,userF.getId());
            carInfo.setStore(store);
            carInfo.setLeaseStatus(carInfoDB.getLeaseStatus());
            carInfo.setCarStatus("true");
            carInfo.setUserF(userF);
            carInfo.setUserB(userBRepository.findUserBByBrand(carTypePrice.getBrand()));
            carInfo.setCarSequenceSign(carInfoDB.getCarSequenceSign());
            carInfo.setCarVin(carInfoDB.getCarVin());
            carInfo.setLicentsePlate(carInfoDB.getLicentsePlate());
            carInfo.setChassisInfo(carInfoDB.getChassisInfo());
            carInfoRepository.save(carInfo);// 添加设备
            String devices = carTypePrice.getCarDeviceIds();
            carDeviceRepository.deleteByCarInfo(carInfo);
            for(String deviceId: devices.split(",")){
                CarDevice carDevice = new CarDevice();
                Device device = new Device();
                device.setDeviceId(Long.parseLong(deviceId));
                carDevice.setDevice(device);
                carDevice.setCarInfo(carInfo);
                carDeviceRepository.save(carDevice);
            }
        }catch (Exception e){
            log.error("修改车辆信息失败!" +e.getMessage());
            return false;
        }
        return true;
    }


    /**
     * 不同底盘信息的数量class
     * @return
     */
    @Override
    public  List<Map<String,String>> getCountByChassis(Long currentAdminId) {
        long userfId=carAdminRepository.getOne(currentAdminId).getUserF().getId();
        long carInfoCount=carInfoRepository.countAllByUserFIdAndCarStatus(userfId,"true");
        int dzChassisNum=0;
        int dtChassisNum=0;
        int benzChassisNum=0;
        int ivecoChassisNum=0;
        List<Map<String,String>> data=carInfoDao.getCountByChassis(userfId);
        System.out.println("dataSize==="+data.size());
        List<Map<String,String>> tempData=new ArrayList<>();
        for (Map<String, String> datum : data) {
            System.out.println(datum.get("name"));
            if ("大众底盘".equals(datum.get("name"))) {
                dzChassisNum += Integer.parseInt(datum.get("value"));
                tempData.add(datum);
            } else if ("大通底盘".equals(datum.get("name"))) {
                dtChassisNum += Integer.parseInt(datum.get("value"));
                tempData.add(datum);
            } else if ("奔驰斯宾特底盘".equals(datum.get("name"))) {
                benzChassisNum += Integer.parseInt(datum.get("value"));
                tempData.add(datum);
            } else if ("国产依维柯底盘".equals(datum.get("name"))) {
                ivecoChassisNum += Integer.parseInt(datum.get("value"));
                tempData.add(datum);
            }
        }
        long otherChassisNum=carInfoCount-(dzChassisNum+dtChassisNum+benzChassisNum+ivecoChassisNum);
        Map<String,String> otherMap=new HashMap<>(0);
        otherMap.put("name","其他");
        otherMap.put("value",otherChassisNum+"");
        tempData.add(otherMap);
        return tempData;
    }

    @Override
    public Map<String, Object> getByCountByLeaseStatus(Long currentAdminId) {
        Map<String,Object> map=new HashMap<>(0);
        List<Map<String,Object>> data=carInfoDao.getCountByLeaseStatus(carAdminRepository.getOne(currentAdminId).getUserF().getId());
        Set<Object> dateTime=new LinkedHashSet<>();
        List<String> freeNum=new ArrayList<>();
        List<String> leaseNum=new ArrayList<>();
        for (Map<String, Object> datum : data) {
            dateTime.add(datum.get("time"));
        }
        for (Map<String, Object> datum : data) {
            if (datum.get("lease_status").equals("空闲")) {
                String fnum = datum.get("num").toString();
                freeNum.add(fnum);
            } else {
                String fnum2 = datum.get("num").toString();
                leaseNum.add(fnum2);
            }
        }
        map.put("dateTime",dateTime);
        map.put("free",freeNum);
        map.put("lease",leaseNum);
        return map;
    }

    @Override
    public CarInfoVo getCarInfoById(Long id) {
        try{
            CarInfo carInfo = carInfoRepository.getOne(id);
            return new CarInfoVo(carInfo);
        }catch (Exception e){
            log.error("获取carinfo 失败!" +e.getMessage());
            return null;
        }
    }

    @Override
    public List<Map> getWeb_bCarInfoByCondition(int id, String message) {
        List<Map> mapList=new ArrayList<>();
        HashMap<String,Object> map;
        List<CarInfo> carInfos;
        HashMap<String,Object> basicInfor;
        HashMap<String,Object>electrical;
        try {
            carInfos = carInfoDao.selectWeb_bCarInfoByCondition(id,message);
            for (CarInfo info: carInfos) {
                map=new HashMap<>(0);
                basicInfor=new HashMap<>(0);
                electrical=new HashMap<>(0);
                basicInfor.put("vin",info.getCarVin());
                basicInfor.put("licentsePlate",info.getLicentsePlate());
                basicInfor.put("fleetName",info.getUserF().getEnterprise());
                basicInfor.put("Type",info.getCar().getCarType());
                electrical.put("electricity","200w");
                electrical.put("energy","200w");
                electrical.put("lithiumBattery","200w");
                electrical.put("inverter","200w");
                map.put("basicInfor",basicInfor);
                map.put("electrical",electrical);
                mapList.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return mapList;
    }

}
