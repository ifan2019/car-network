package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.entity.monitor.Admin;
import com.xmmufan.carnetwork.repository.jpa.AdminRepository;
import com.xmmufan.carnetwork.service.CarAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarAdminServiceImpl implements CarAdminService {
    @Autowired
    AdminRepository carAdminRepository;

    @Override
//    @Cacheable(value = "user_c_info",keyGenerator = "wiselyKeyGenerator")
    public Admin findByUsername(String username) {
        List<Admin> adminList = carAdminRepository.findByUserName(username);
        return adminList.isEmpty()?null:adminList.get(0);
    }
}
