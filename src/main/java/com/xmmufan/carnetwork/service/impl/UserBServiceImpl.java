package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.constant.permission.AccountState;
import com.xmmufan.carnetwork.entity.monitor.UserB;
import com.xmmufan.carnetwork.entity.vo.UserBInfoVo;
import com.xmmufan.carnetwork.repository.jpa.UserBRepository;
import com.xmmufan.carnetwork.service.UserBService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mr.ifan/詹奕凡
 * @date 2019/6/13
 * @version 1.0
 */
@Service
@Slf4j
public class UserBServiceImpl implements UserBService {

    @Autowired
    UserBRepository userBRepository;

    @Override
//    @Cacheable(value = "user_b_info",keyGenerator = "wiselyKeyGenerator")
    public UserB findByUsername(String username) {
        System.out.println("findByUsername");
        return userBRepository.findUserBByUserName(username);
    }

    @Override
    public List<UserBInfoVo> getAllUserB() {
        List<UserBInfoVo> userBInfoVos = new ArrayList<>();
        try {
            userBRepository.getUserBByState(AccountState.ACTIVE).forEach(user -> {
                userBInfoVos.add(new UserBInfoVo(user));
            });
            return userBInfoVos;
        }catch (Exception e){
            log.info("获取userb错误!" + e.getMessage());
            return null;
        }
    }
}
