package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.entity.monitor.Admin;
import com.xmmufan.carnetwork.entity.monitor.UserF;
import com.xmmufan.carnetwork.repository.dao.CarDao;
import com.xmmufan.carnetwork.repository.jpa.AdminRepository;
import com.xmmufan.carnetwork.repository.jpa.CarInfoRepository;
import com.xmmufan.carnetwork.repository.jpa.CarRepository;
import com.xmmufan.carnetwork.service.CarService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author   陈丽
 * @date     2019-5-26
 */
@Service
@Slf4j
public class CarServiceImpl implements CarService {

    @Autowired
    private CarDao carDao;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private AdminRepository carAdminRepository;

    @Autowired
    private CarInfoRepository carInfoRepository;

    @Override
    public Map<String, Object> getMapInfo(int carId) {
        HashMap<String,Object> map=new HashMap<>(0);
        List<Double> startingPoint=new ArrayList<>();
        startingPoint.add(118.073467);
        startingPoint.add(24.450968);
        List<Double> nowPoint=new ArrayList<>();
        nowPoint.add(118.082033);
        nowPoint.add(24.630538);
        List<Double> endPoint=new ArrayList<>();
        endPoint.add(118.059395);
        endPoint.add(24.612685);
        map.put("startingPoint",startingPoint);
        map.put("nowPoint",nowPoint);
        map.put("endPoint",endPoint);
        return map;
    }

    @Override
    public Map<String, Object> getCarInfoNumberByCarType(Long currentAdminId) {
        Map<String,Object> map=new HashMap<>(0);
        Admin carAdmin = carAdminRepository.getOne(currentAdminId);
        UserF userF = carAdmin.getUserF();
        int selfPropelledNum = carInfoRepository.countByCarCarTypeAndUserFAndCarStatus("自行式",userF,"true");
        int pullingTypeNum = carInfoRepository.countByCarCarTypeAndUserFAndCarStatus("拖挂式",userF,"true");
        int mobileVillaNum = carInfoRepository.countByCarCarTypeAndUserFAndCarStatus("移动别墅",userF,"true");
        map.put("selfPropelled",selfPropelledNum);
        map.put("pullingType",pullingTypeNum);
        map.put("mobileVilla",mobileVillaNum);
        return map;
    }
}
