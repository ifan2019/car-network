package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Mr.ifan | 詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 * 用户权限接口详细类
 * </p>
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysRoleServiceImpl implements SysRoleService {

//    private final SysRoleRepository sysRoleRepository;
//
//    @Autowired
//    public SysRoleServiceImpl(SysRoleRepository sysRoleRepository) {
//        this.sysRoleRepository = sysRoleRepository;
//    }
//
//    @Override
//    public SysRole findByRoleName(String roleName) {
//        List<SysRole> roles = sysRoleRepository.findByRole(roleName);
//        return roles.isEmpty() ? null : roles.get(0);
//    }
//
//    @Override
//    public List<SysRole> findAllRoles() {
//        return sysRoleRepository.findAll();
//    }
//
//    @Override
//    public SysRole saveOrUpdateRole(SysRole role) {
//        return sysRoleRepository.saveAndFlush(role);
//    }
}
