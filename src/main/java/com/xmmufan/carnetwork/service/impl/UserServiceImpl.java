package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.service.UserService;
import org.apache.shiro.authz.UnauthorizedException;
import org.hibernate.type.ObjectType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Mr.ifan | 詹奕凡
 * @version 1.0
 * <p>
 *     用户服务详细接口类
 *         Cache 注解简介
 *         1. @Cacheable 主要针对方法配置，根据方法的请求参数对结果进行缓存
 *         2. @CacheEvict 清空缓存
 *         3. @CachePut 保证了方法的调用，也把结果放入缓存
 *              注解属性 ：
 *
 *                  1. value 缓存的名字
 *                  2. key 缓存的key
 *                  3. condition 放入缓存的条件，只有为true才会放入缓存
 *                  4. allEntries(@CacheEvict的属性)，是否清空所有缓存，默认为false，若为true
 *                      ，方法指定完毕清空所有缓存
 *                  5. beforeInvocation (@CacheEvict的属性)是否在方法执行前就清空，缺省为 false，如果指定
 *                      为 true，则在方法还没有执行的时候就清空缓存，
 *                      缺省情况下，如果方法执行抛出异常，则不会清空
 *                  6. unless (@Cacheable,@CachePut的属性) 用于否决缓存的，不像condition，该表达式只在方
 *                      法执行之后判断，此时可以拿到返回值result进行判断。条件为true不会缓存，fasle才缓存
 *
 *
 * </p>
 */
@Service
@Transactional(rollbackFor = UnauthorizedException.class)
public class UserServiceImpl implements UserService {


//    @Override
//    @Transactional(readOnly = true)
//    public User findByUsername(String username) {
//        List<User> users = userRepository.findByUsername(username);
//        return users.isEmpty() ? null : users.get(0);
//    }
//
//    @Override
//    public User saveUser(User user) {
//        return userRepository.save(user);
//    }
//
//    @Override
//    public User updateUser(User user) {
//
//        User cacheUser = findById(user.getId());
//        cacheUser.setSalt(user.getSalt());
//        cacheUser.setPassword(user.getPassword());
//        return userRepository.saveAndFlush(cacheUser);
//    }
//
//    @Override
//    @Transactional(readOnly = true)
//    public User findById(Long id) {
//        return userRepository.findById(id).orElse(null);
//    }
//
//    @Override
//    @Cacheable(value = "user_info_vo",key = "'user_info_vo'+ #offset/#limit",unless = "#result eq null")
//    public List<UserInfoVo> list(int offset, int limit) {
//        System.out.println("search============");
//        Pageable pageable = PageRequest.of(offset/limit, limit);
//        List<User> content = userRepository.findAll(pageable).getContent();
//        List<UserInfoVo> result = new ArrayList<>();
//        for(User user:content){
//            result.add(new UserInfoVo(user));
//        }
//        return result;
//    }


}
