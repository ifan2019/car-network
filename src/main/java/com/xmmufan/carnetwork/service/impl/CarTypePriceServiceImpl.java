package com.xmmufan.carnetwork.service.impl;

import com.xmmufan.carnetwork.entity.monitor.*;
import com.xmmufan.carnetwork.entity.vo.CarTypePriceVo;
import com.xmmufan.carnetwork.repository.jpa.*;
import com.xmmufan.carnetwork.service.CarTypePriceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/9/28
 */
@Service
@Slf4j
public class CarTypePriceServiceImpl implements CarTypePriceService {
    @Autowired
    private AdminRepository carAdminRepository;
    @Autowired
    private CarTypePriceRepository carTypePriceRepository;
    @Autowired
    private CarDeviceRepository carDeviceRepository;
    @Autowired
    private CarInfoRepository carInfoRepository;
    @Autowired
    private CarTypeRepository carTypeRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean addOrUpdateCarTypePrice(CarTypePriceVo carTypePriceVo) {
        Long currentAdminId = carTypePriceVo.getCurrentAdminId();
        Admin carAdmin = carAdminRepository.getOne(currentAdminId);
        UserF user = carAdmin.getUserF();
        Set<City> cityList = new HashSet<>(user.getCityList());
        try{
            for (City city : cityList) {
                CarTypePrice carTypePrice = new CarTypePrice(carTypePriceVo);
                carTypePrice.setUserF(user);
                carTypePrice.setCity(city);
                List<CarTypePrice> carTypePrices = carTypePriceRepository.getAllByUserFAndCity(user,city);
                boolean canSave = true;
                for(CarTypePrice temp: carTypePrices){
                    if(carTypePrice.equals(temp)){
                        canSave = false;
                        break;
                    }
                }
                if(canSave){
                    List<Device> deviceList = new ArrayList<>();
                    CarTypePrice carTypePriceDb;
                    if(carTypePriceVo.getId()!=null){
                        carTypePriceDb = carTypePriceRepository.getOne(carTypePriceVo.getId());
                    }else{
                        carTypePriceDb = carTypePrice;
                    }
                    for(String id: carTypePriceDb.getCarDeviceIds().split(",")){
                        deviceList.add(new Device().setDeviceId(Long.parseLong(id)));
                    }
                    Set<CarInfo> carInfoList = carInfoRepository.getCarInfoByBrandAndCarCarTypeAndDeviceListIn(carTypePriceDb.getBrand(), carTypeRepository.getOne(carTypePriceDb.getCarTypeId()).getCarTypeName(), deviceList);
                    for(CarInfo carInfo: carInfoList){
                        carDeviceRepository.deleteByCarInfo(carInfo);
                        String devices = carTypePrice.getCarDeviceIds();
                        for (String deviceId : devices.split(",")) {
                            if(carDeviceRepository.getCarDeviceByDeviceDeviceIdAndCarInfoId(Long.parseLong(deviceId),carInfo.getId()) == null){
                                CarDevice carDevice = new CarDevice();
                                Device device = new Device();
                                device.setDeviceId(Long.parseLong(deviceId));
                                carDevice.setDevice(device);
                                carDevice.setCarInfo(carInfo);
                                carDeviceRepository.save(carDevice);
                            }
                        }
                    }
                    carTypePriceRepository.save(carTypePrice);
                }

            }
        }catch (Exception e){
            log.error("添加车辆类型失败!");
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Override
    public Set<CarTypePriceVo> getCarTypePriceList(Long currentAdminId) {
        Set<CarTypePriceVo> carTypePriceVoSet = new LinkedHashSet<>();
        try{
            UserF user = carAdminRepository.getOne(currentAdminId).getUserF();
            List<CarTypePrice> carTypePriceList = carTypePriceRepository.getAllByUserF(user);
            carTypePriceList.forEach(carTypePrice -> {
                carTypePriceVoSet.add(new CarTypePriceVo(carTypePrice));
            });
            return carTypePriceVoSet;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteById(Long id) {
//        try {
//
//        }
//        carTypePriceRepository.deleteById(id);
        return false;
    }


}
