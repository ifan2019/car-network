package com.xmmufan.carnetwork.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xmmufan.carnetwork.repository.dao.CarInfoDao;
import com.xmmufan.carnetwork.service.CarInfoService;
import com.xmmufan.carnetwork.service.HardWareService;
import com.xmmufan.carnetwork.util.RedisUtil;
import com.xmmufan.carnetwork.util.RequestStringEncoding;
import com.xmmufan.carnetwork.util.Util;
import com.xmmufan.carnetwork.util.UtilMap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.LockSupport;


/**
 * @author ifan
 * @version 1.0
 * @date 2019/7/29
 */
@Service
@Slf4j
public class HardWareServiceImpl implements HardWareService {
    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private CarInfoDao carInfoDao;

    @Override
//    @SuppressWarnings("unchecked")
    public List<Map> query(String startAddress, String endAddress, String commandType, String data,String carVin) {
        String sequence=carInfoDao.getCarSequenceByCarVin(carVin);
        Channel channel = UtilMap.getChannelByName("ffff");
        if(channel!=null){
            String s = RequestStringEncoding.judgeOperationType(commandType, startAddress, endAddress, data);
            byte[] bytes = Util.hexStringToByteArray(s);
            channel.writeAndFlush(Unpooled.copiedBuffer(bytes));
            jmsTemplate.setReceiveTimeout(3000);
            //+ (startAddress + endAddress).toUpperCase()
            Object receive = jmsTemplate.receiveAndConvert(sequence + "_query" );
            if(receive!=null){
                JSONArray jsonArray = JSONArray.parseArray(receive.toString());
                return jsonArray.toJavaList(Map.class);
            }
            return null;
        }
        return null;
    }

    @Override
    public int control(String address, String commandType, String commandStatus,String carVin) {
        try {
            String sequence=carInfoDao.getCarSequenceByCarVin(carVin);
            Channel channel=UtilMap.getChannelByName("ffff");
            String message=RequestStringEncoding.judgeOperationType(commandType,address,"",commandStatus);
            if(channel!=null){
                return receiveMessageForMq(sequence+"_control", channel, message);
            }
        }catch (JMSException e){
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int config(String address, String data, String carVin) {
        try{
            String sequence=carInfoDao.getCarSequenceByCarVin(carVin);
            Channel channel=UtilMap.getChannelByName("ffff");
//            Channel channel=UtilMap.getChannelByName(sequence);
            if(channel!=null){
                String message = RequestStringEncoding.judgeOperationType("0030",address,"",data);
                return receiveMessageForMq(sequence+"_config", channel, message);
            }
        }catch(JMSException e){
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public Map<String, Object> getPageData(String carVin) {
        //水箱：通过 003600013C - 003600013E 查询
        /*
         * 逆变器： 0003011076 查询CF工作状态，
         * 0-待机模式，1-旁路模式，2-电池逆变模式，3-错误模式，4-节能模式
         * 如果为旁路状态，说明逆变器接入市电，返回给前端的electricityState为true，并且inputVoltage=220
         * 如果为非旁路状态，说明没有接入市电，返回的并且inputVoltage=---，electricityState=false
         * 如果返回为待机模式，说明逆变器失联？，返回给App的switchFaultState为？
         * 通过 000301107A 查询负载功率，返回给App carryPower
         * 是否故障通过查询报警信息来判断
         */
        /*
         * electric
         * electricPower 通过 ffff000119 查询PV当天发电量/24
         * switchFaultState 查询报警
         * electricAllI  000f010105 查询后备电池充电电流
         *
         */
        /*
         * light
         *
         */

        /*
         * 电池详情页 livingBattery
         * time           ff03010016  电池状态为充电时，为充满需要时间,电池状态为放电时，为放空需要时间
         * batteryType    ff03010019    0:就绪 1：充电 2：放电 3：保护
         * capacity       ff03010027   实际容量
         * batteryTypeNum ff03010001    放电电流为负数,充电为正数
         * batteryVoltage ff03010000    电池电压,若失联为0
         */

        /*
         * 水箱 tank  通过003600013C - 003600013E 查询
         */


//        String sequence=carInfoDao.getCarSequenceByCarVin(carVin);
//        Channel channel=UtilMap.getChannelByName("ffff");
//        if(channel!=null){
//
//        }
        return null;
    }

    private int receiveMessageForMq(String queueName, Channel channel, String message) throws JMSException {
        byte[] bytes= Util.hexStringToByteArray(message);
        channel.writeAndFlush(Unpooled.copiedBuffer(bytes));
        jmsTemplate.setReceiveTimeout(5000);
        Message result = jmsTemplate.receive(queueName);
        if(result==null){
            return -2;
        }
        if("true".equals(result.getStringProperty("text"))){
            return 0;
        }
        return 0;
    }
}
