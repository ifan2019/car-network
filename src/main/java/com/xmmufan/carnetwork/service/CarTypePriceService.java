package com.xmmufan.carnetwork.service;

import com.xmmufan.carnetwork.entity.vo.CarTypePriceVo;

import java.util.Set;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/9/28
 */
public interface CarTypePriceService {

    boolean addOrUpdateCarTypePrice(CarTypePriceVo carTypePriceVo);

    Set<CarTypePriceVo> getCarTypePriceList(Long currentAdminId);

    boolean deleteById(Long id);
}
