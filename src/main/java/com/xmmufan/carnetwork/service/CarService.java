package com.xmmufan.carnetwork.service;

import java.util.Map;

/**
 * @author   陈丽
 * @date     2019-5-26
 */
public interface CarService {

    Map<String, Object> getMapInfo(int carId);

    Map<String, Object> getCarInfoNumberByCarType(Long currentAdminId);
}
