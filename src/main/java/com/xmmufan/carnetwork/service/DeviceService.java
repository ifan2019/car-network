package com.xmmufan.carnetwork.service;


import com.xmmufan.carnetwork.entity.monitor.Device;

import java.util.List;

public interface DeviceService {


    List<Device> getAllDevice();

}
