package com.xmmufan.carnetwork.aspect;

import java.lang.annotation.*;

/**
 * @author Mr ifan/詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 *     加密注解
 *     加上该注解，在方法执行前进行拦截，对参数内的password与salt进行赋值加密
 *     注：如果参数未包含password与salt字段，则抛出com.xmmufan.project.constant.exception.AccountException
 * </p>
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EncryptedPasswordAndSalt {

    //加密方式
    String algorithm() default "MD5";

    //加密次数
    int hashIterations() default 8;

}
