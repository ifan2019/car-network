package com.xmmufan.carnetwork.aspect;

import java.lang.annotation.*;

/**
 * @author Mr.ifan | 詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 *     更新权限缓存注解
 * </p>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UpdatePermissionCache {

}
