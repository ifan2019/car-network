package com.xmmufan.carnetwork.aspect;

import com.xmmufan.carnetwork.configuration.permission.ShiroService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author Mr.ifan | 詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 *     修改权限切入点
 * </p>
 */

@Aspect
@Component
@Slf4j
public class UpdatePermissionAspect {

    private final ShiroService shiroService;

    @Autowired
    public UpdatePermissionAspect(ShiroService shiroService) {
        this.shiroService = shiroService;
    }

    @Pointcut("@annotation(updatePermissionCache)")
    public void pointcut(UpdatePermissionCache updatePermissionCache) {
    }

    @After(value = "pointcut(updatePermissionCache)", argNames = "joinPoint, updatePermissionCache")
    public void updatePermissionCache(JoinPoint joinPoint, UpdatePermissionCache updatePermissionCache){
        shiroService.updatePermission();
    }

}
