package com.xmmufan.carnetwork.aspect;

import com.xmmufan.carnetwork.algorithm.SnowFlake;
import com.xmmufan.carnetwork.entity.monitor.UserB;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author Mr.ifan | 詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 * 注解切面类
 * </p>
 *
 * springboot 如何面向切面
 *          1. 引入aop依赖
 *              <dependency>
 *                  <groupId>org.springframework.boot</groupId>
 *                  <artifactId>spring-boot-starter-aop</artifactId>
 *              </dependency>
 *         2. 定义相应的注解：
 *
 *         3. 定义相应的aop类，例如此类
 *
 */

@Aspect
@Component
@Slf4j
public class EncryptAspect {

    @Pointcut("@annotation(encryptedPassword)")
    public void mark(EncryptedPasswordAndSalt encryptedPassword) {
    }

    /**
     * 切面自动获取参数User，当id不存在值时，使用雪花算法生成ID
     * @param joinPoint  切入点
     * @param encryptedPassword  加密密码
     * @author Mr.ifan
     * @date 2019/5/19
     */
    @Before(value = "mark(encryptedPassword)", argNames = "joinPoint,encryptedPassword")
    public void doBefore(JoinPoint joinPoint, EncryptedPasswordAndSalt encryptedPassword) throws Throwable {
        log.info("切面");
        Object[] params = joinPoint.getArgs();
        try {
            for (Object user : params) {
                if (/*user instanceof User ||*/ user instanceof UserB) {
                    Method getIdMethod = user.getClass().getDeclaredMethod("getId");
                    Long idVal = (Long) getIdMethod.invoke(user);
                    if (idVal == null || idVal == 0) {
                        Field userId = user.getClass().getDeclaredField("id");
                        userId.setAccessible(true);
                        userId.set(user, new SnowFlake().nextId());
                    }

                    Field userPassword = user.getClass().getDeclaredField("password");
                    Field userSalt = user.getClass().getDeclaredField("salt");
                    userSalt.setAccessible(true);
                    userPassword.setAccessible(true);
                    userSalt.set(user, new SecureRandomNumberGenerator().nextBytes().toHex());
                    userPassword.set(user, new SimpleHash(encryptedPassword.algorithm(),
                            userPassword.get(user), ByteSource.Util.bytes(userSalt.get(user)),
                            encryptedPassword.hashIterations()).toString());
                }
            }
        } catch (Exception e) {
            log.error("Do encrypt method occupied an error: ", e);
            throw new AccountException("There is no object contains field 'password'. ");
        }
    }

}
