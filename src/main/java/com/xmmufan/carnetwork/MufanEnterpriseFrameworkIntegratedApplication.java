package com.xmmufan.carnetwork;

import com.xmmufan.carnetwork.netty.Server4Client;
import com.xmmufan.carnetwork.netty.Server4Hardware;
import com.xmmufan.carnetwork.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.mybatis.spring.annotation.MapperScan;

import java.util.HashMap;
import java.util.Map;

/**
 * Springboot主启动类
 * @author Mr ifan/詹奕凡
 * @date 2019/5/19
 * @version 1.0
 */
@SpringBootApplication
@MapperScan("com.xmmufan.carnetwork.repository.dao")
@EnableCaching
@EnableJpaAuditing
@EnableScheduling
public class MufanEnterpriseFrameworkIntegratedApplication {

//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//        return builder.sources(MufanEnterpriseFrameworkIntegratedApplication.class);
//    }

    private static RedisUtil redisUtil;

    @Autowired
    public static void setRedisUtil(RedisUtil redisUtil){
        MufanEnterpriseFrameworkIntegratedApplication.redisUtil = redisUtil;
    }

    public static void main(String[] args) {
        SpringApplication.run(MufanEnterpriseFrameworkIntegratedApplication.class, args);

        Server4Hardware.run();
    }

}
