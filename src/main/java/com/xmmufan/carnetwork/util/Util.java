package com.xmmufan.carnetwork.util;

public class Util {
    private static final int SERIES_LENGTH = 14;

    public static byte[] hexStringToByteArray(String hexString) {
        hexString = hexString.replaceAll(" ", "");
        int len = hexString.length();
        byte[] bytes = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            // 两位一组，表示一个字节,把这样表示的16进制字符串，还原成一个字节
            bytes[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character
                    .digit(hexString.charAt(i + 1), 16));
        }
        return bytes;
    }

    public static boolean checkSeries(String mSeries){
        char[] seriesList = mSeries.toCharArray();
        char crc = 0;
        if(seriesList.length != SERIES_LENGTH){
            return false;
        }
        String crcStr = mSeries.substring(12, 14);
        for (int i=0; i< seriesList.length - 2 ;i++) {
            crc ^= seriesList[i];
            for (int j = 0; j < 8; j++) {
                if((crc & 0x80) != 0){
                    crc = (char)((crc << 1) ^ 0x07);
                }else{
                    crc <<= 1;
                }
            }
        }
        String checkSum = String.format("%02X", crc & 0xff);
        return checkSum.equals(crcStr.trim());
    }
}
