package com.xmmufan.carnetwork.util;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ifan
 * @version 1.0
 *
 */
@Component
public class FileUploadUtil {

    public String upload(MultipartFile license) {
        COSClientUtil cosClientUtil=new COSClientUtil();
        String name=null;
        try {
            name=cosClientUtil.uploadFile2Cos(license);
        }catch (Exception e){
            e.printStackTrace();
        }
        String imgUrl=cosClientUtil.getImgUrl(name);
        String [] split=imgUrl.split("\\?");
        return split[0];
    }
}
