package com.xmmufan.carnetwork.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 固件文件解密util
 *
 * @author ifan
 * @version 1.0
 * @date 2019/11/30
 */
public class FileLZW {
    static private int INIT_BITS = 9;
    static private int MAX_BITS = 14;
    static private int HASHING_SHIFT = MAX_BITS - 8;
    static private int TABLE_SIZE = 18041;
    static private int CHECK_TIME = 100;
    private int[] codeValue;
    private int[] prefixCode;
    private int[] appendCharacter;
    private int[] decodeStack = new int[4000];

    private int numBits;
    private long bytesIn, bytesOut;
    private long maxCode;
    private int outputBitCount;
    private long outputBitBuffer;
    private int inputBitCount;
    private int inputBitBuffer;

    private int mAXVAL(int n) {
        return ((1 << (n)) - 1);
    }

    private void initGlobalVar() {
        numBits = INIT_BITS;
        bytesIn = 0;
        bytesOut = 0;
        maxCode = mAXVAL(numBits);
    }

    public FileLZW() {
        initGlobalVar();
        codeValue = new int[TABLE_SIZE];
        prefixCode = new int[TABLE_SIZE];
        appendCharacter = new int[TABLE_SIZE];

        for (int i = 0; i < TABLE_SIZE; i++) {
            codeValue[i] = 0;
            prefixCode[i] = 0;
            appendCharacter[i] = 0;
        }

    }


    public int findMatch(int hashPrefix, int hashCharacter) {
        int index, offset;

        index = (hashCharacter << HASHING_SHIFT) ^ hashPrefix;
        if (index == 0) {
            offset = 1;
        } else {
            offset = TABLE_SIZE - index;
        }
        while (true) {
            if (codeValue[index] == -1) {
                return (index);
            }
            if (prefixCode[index] == hashCharacter && appendCharacter[index] == hashCharacter) {
                return (index);
            }
            index -= offset;
            if (index < 0) {
                index += TABLE_SIZE;
            }
        }
    }

    private Integer decodeString(int[] buffer, int pos, int code) {
        int i = 0;
        while (code > 255) {
            if (code >= TABLE_SIZE) {
                return null;
            }
            Integer val = appendCharacter[code];
            buffer[pos++] = val;
            code = prefixCode[code];
            if (i++ >= 4000) {
                return null;
            }
        }
        buffer[pos] = code;

        return pos;
    }


    private int inputCode(BufferedInputStream input) throws IOException {
        int returnValue;
        while (inputBitCount <= 24) {
            long val = input.read();
            //System.out.println(input_bit_count+","+input_bit_buffer+",val:"+val);
            inputBitBuffer |= val << (24 - inputBitCount);
            inputBitCount += 8;
        }
        returnValue = (inputBitBuffer >>> (32 - numBits));
        //System.out.println("=="+input_bit_count+","+input_bit_buffer+",return_value:"+return_value);
        inputBitBuffer <<= numBits;
        inputBitCount -= numBits;
        //System.out.println("=="+input_bit_count+","+input_bit_buffer+",return_value:"+return_value);
        return (returnValue);
    }


    private void outputCode(FileOutputStream output, int code) throws IOException {
        outputBitBuffer |= (long) code << (32 - numBits - outputBitCount);
        outputBitCount += numBits;
        while (outputBitCount >= 8) {
            output.write((int) (outputBitBuffer >> 24));
            outputBitBuffer <<= 8;
            outputBitCount -= 8;
            bytesOut++;
        }
    }

    public boolean expand(String input, String output) throws IOException {
        boolean retVal = false;
        if ((input != null) && (output != null)) {
            File inputFile, outputFile;
            BufferedInputStream fis;
            BufferedOutputStream fos;
            inputFile = new File(input);
            outputFile = new File(output);
            fis = new BufferedInputStream(new FileInputStream(inputFile));
            fos = new BufferedOutputStream(new FileOutputStream(outputFile));
            initGlobalVar();
            inputBitCount = 0;
            inputBitBuffer = 0;
            retVal = expand(fis, fos);
            //System.out.println(input_file.length());
            //System.out.println(output_file.length());
            fos.close();
            fis.close();

        }
        return retVal;
    }


    public boolean expand(BufferedInputStream input, BufferedOutputStream output) throws IOException {
        int firstCode = 258;
        Integer nextCode = firstCode;
        Integer newCode;
        Integer oldCode = 0;
        int character = 0,
                counter = 0,
                clearFlag = 1;
        Integer string;
        System.out.println("Expanding\n");
        int terminator = 257;
        while ((newCode = inputCode(input)) != terminator) {
            if (clearFlag != 0) {
                clearFlag = 0;
                oldCode = newCode;
                character = oldCode;
                output.write(oldCode);
                continue;
            }
            int clearTable = 256;
            if (newCode == clearTable) {
                clearFlag = 1;
                numBits = INIT_BITS;
                nextCode = firstCode;
                maxCode = mAXVAL(numBits);
                continue;
            }
            if (++counter >= 1000) {
                counter = 0;
            }
            int decodeStackPos = 0;
            if (newCode >= nextCode) {
                decodeStack[decodeStackPos] = character;
                string = decodeString(decodeStack, decodeStackPos + 1, oldCode);
            } else {
                string = decodeString(decodeStack, decodeStackPos, newCode);
            }
            if (string == null) {
                System.out.println("-------------- error:string == null --------------------");
                return false;
            }

            character = decodeStack[string];

            while (string >= decodeStackPos) {
                output.write(decodeStack[string--]);
            }

            if (nextCode <= maxCode) {
                prefixCode[nextCode] = oldCode;
                appendCharacter[nextCode++] = character;
                if (nextCode == maxCode && numBits < MAX_BITS) {
                    maxCode = mAXVAL(++numBits);
                }
            }
            oldCode = newCode;
        }
        return true;
    }
}
