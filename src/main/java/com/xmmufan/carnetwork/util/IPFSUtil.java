package com.xmmufan.carnetwork.util;


import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.multihash.Multihash;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * IPFS工具类
 * @author zyg
 * @date 2019/3/18
 */
@Component
public class IPFSUtil {
    private static IPFS ipfs = new IPFS("/ip4/119.23.104.188/tcp/5001");
//http://193.148.69.60:8080/ipfs/QmXMYJGZAk3JhepuG3LXifLYTsWib8t3Hfntv2m1bAoD7w
    /**
     * 将图片添加到IPFS网络中
     * @param filePathName 文件的路径
     * @return String(file hash)
     * @throws IOException
     */
    public String upload(String filePathName) throws IOException {
        NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(new File(filePathName));
        MerkleNode addResult = ipfs.add(file).get(0);
        return addResult.hash.toString();
    }

    /**
     * 将IPFS网络中的文件下载到本地中
     * @param fileName 保存文件的路径
     * @param hash 要下载的文件hash
     * @throws Exception
     */
    public void download(String fileName,String hash) throws Exception{
        Multihash filePointer = Multihash.fromBase58(hash);
        byte[] data = ipfs.cat(filePointer);
        if(data != null){
            File file  = new File(fileName);
            if(file.exists()){
                file.delete();
            }
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data,0,data.length);
            fos.flush();
            fos.close();
        }
    }
}
