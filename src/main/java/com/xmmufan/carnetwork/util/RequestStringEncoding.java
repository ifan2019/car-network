package com.xmmufan.carnetwork.util;
import com.xmmufan.carnetwork.constant.hardware.CommandType;
import com.xmmufan.carnetwork.constant.hardware.CrcUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author  陈丽
 * @date    2019-7-25
 */
public class RequestStringEncoding {

    private static StringBuilder getDefaultString(){
        StringBuilder defaultString=new StringBuilder();
        defaultString.insert(0,"fe");
        defaultString.insert(2,"xxxx");
        defaultString.insert(6,"0012");
        StringBuilder s = new StringBuilder(Integer.toHexString((int) (Math.random() * 16777215)));
        while(s.length() < 6){
            s.insert(0, "0");
        }
        defaultString.insert(10,"00"+ s.toString());
        defaultString.insert(18,"051a");
        defaultString.insert(22,"00000000");
        defaultString.insert(30,"5d1062f2");
        return defaultString;
    }

    private static String getHeadLength(List<String> strList){
        String headLength=Integer.toHexString(strList.size());
        if(strList.size()+2<0x10){
            headLength="000"+Integer.toHexString(strList.size());
        }else if(strList.size()+2<0x100){
            headLength="00"+Integer.toHexString(strList.size()+2);

        }else if(strList.size()<0x1000){
            headLength="0"+Integer.toHexString(strList.size()+2);
        }
        return headLength;
    }

    private  static String getCrcData(List<String> strList){
        List<Integer> crcList=new ArrayList<>();
        for (String str:strList) {
            crcList.add(Integer.parseInt(str,16));
        }
        return CrcUtil.contextLoads(crcList.stream().mapToInt(Integer::valueOf).toArray());
    }

    public static void main(String[] args) {
//        String s = Integer.toHexString(8080);
//        System.out.println(s);
        String s = judgeOperationType("0050", "ffff000106", "ffff00011A", "");
        System.out.println(s);

        //System.out.println("size=="+"fe0044001200eda83604ea000000005d1062f2004005ffff001000290000000000000000000000000000000075736572260000000000000000000000000000000002020003f5e9fe".length());
    }


    public static String judgeOperationType(String command,String startAddress,String endAddress,String data){
        String result="";
        if(CommandType.CONFIG_INSTRUCTION_SEND.equals(command)){
            result=configRequest(command,startAddress,data);
        }
        else if(CommandType.CONTROL_INSTRUCTION_SEND.equals(command)){
            result=controlRequest(command,startAddress,data);
        }
        else if(CommandType.SELECT_INSTRUCTION_SEND.equals(command)){
            result=queryRequest(command,startAddress,endAddress);
        }
        else if(CommandType.IP_SET_COMMAND_SEND.equals(command)){
            String[] ipAndPort = data.split(":");
            result=ipSettingRequest(command,ipAndPort[0],Integer.parseInt(ipAndPort[1]));
        }else if(CommandType.START_OR_FINISH_UPDATE_FIRMWARE_SEND.equals(command)){
            result = startOrShutDownHardWardCommand(0,0,0);
        }
        return result;
    }

//    private static String ipSettingRequest(String command) {
//        StringBuilder all = getDefaultString();
//        all.insert(38,command);
//        all.insert(42,"19216801");
//        all.insert(50,"8080");
//        String a=all.substring(6,54);
//        List<String> strList = ParseHelper.getStrList(a, 2);
//        List<Integer> crcList=new ArrayList<>();
//        String crcData=getCrcData(strList);
//        all.insert(54,crcData);
//        all.replace(2,6,getHeadLength(strList));
//        all.insert(58,"fe");
//        List<String> result= ParseHelper.getStrList(all.toString(),4);
//        return String.join("",result);
//    }

//    private static String configRequest(String command, String startAddress, String data) {
//        all=getDefaultString();
//        all.insert(38,command);
//        all.insert(42,"05");
//        all.insert(44,startAddress);
//        List<String> datas=StringHelper.getStrList(data,2);
//        String length = Integer.toHexString(datas.size());
//        if(datas.size()<0x10){
//            length = "0"+Integer.toHexString(datas.size());
//        }
//        all.insert(54,length);
//        all.insert(56,data);
//        String a=all.substring(6,56+data.length());
//        List<String> strList = StringHelper.getStrList(a, 2);
//        List<Integer> crcList=new ArrayList<>();
//        for (String str:strList) {
//            crcList.add(Integer.parseInt(str,16));
//        }
//        String crcData = CrcUtil.contextLoads(crcList.stream().mapToInt(Integer::valueOf).toArray());
//        all.insert(56+data.length(),crcData);
//        all.replace(2,6,getHeadLength(strList));
//        all.insert(56+data.length()+4,"fe");
//        List<String> result=StringHelper.getStrList(all.toString(),4);
//        System.out.println("sdfa==="+ String.join("",result));
//        return String.join("",result);
//
//    }

    private static String configRequest(String command,String startAddress,String data){
        return controlRequest(command,startAddress,data);
    }

    /**
     * 控制指令封装 0040
     * @param command 指令类型
     * @param startAddress 起始地址
     * @param data 控制的数据
     * @return String.class 封装完成的控制数据
     */
    private static String controlRequest(String command, String startAddress,String data) {
        StringBuilder all = getDefaultString();
        all.insert(38,command);
        // 地址长度
        all.insert(42,"05");
        //加密
        //设置起始地址
        all.insert(44,startAddress);
        List<String> datas = ParseHelper.getStrList(data,2);
        String length = Integer.toHexString(datas.size());
        if(datas.size() < 0x10){
            length = "0" + Integer.toHexString(datas.size());
        }
        //数据长度
        all.insert(54,length);
        //数据
        all.insert(56,data);
        //截取需要加密的数据
        String before = all.substring(42, 56 + data.length());
        byte[] bytes = Util.hexStringToByteArray(before);
        StringBuilder res = new StringBuilder();
        //加密算法
        for(byte b: bytes){
            int temp = (~(((((b^1)+2)-3)^4)));
            if(temp < 0 ){
                temp += 256;
            }
            if(temp < 10){
                res.append("0").append(Integer.toHexString(temp));
            }else{
                res.append(Integer.toHexString(temp));
            }
        }
        //替换加密数据
        all.replace(42,56+ data.length(), res.toString());
        //end
        String a=all.substring(6,56+data.length());
        List<String> strList = ParseHelper.getStrList(a, 2);
        String crcData=getCrcData(strList);
        all.insert(56+data.length(),crcData);
        all.replace(2,6,getHeadLength(strList));
        all.insert(56+data.length()+4,"fe");
        List<String> result= ParseHelper.getStrList(all.toString(),4);
        return String.join("",result);

    }


    /**
     * 查询指令请求数据封装 0050
     * @param command 指令类型
     * @param startAddress 起始地址
     * @param endAddress 结束地址
     * @return String.class 封装完成的数据
     */
    private static String queryRequest(String command,String startAddress,String endAddress) {
        StringBuilder all = getDefaultString();
        all.insert(38,command);
        //地址长度
        all.insert(42,"05");
        //起始地址
        all.insert(44,startAddress);
        //结束地址
        all.insert(54,endAddress);
        String a=all.substring(6,64);
        List<String> strList = ParseHelper.getStrList(a, 2);
        String crcData = getCrcData(strList);
        all.insert(64,crcData);
        all.replace(2,6,getHeadLength(strList));
        all.insert(68,"fe");
        List<String> result= ParseHelper.getStrList(all.toString(),4);
        return String.join("",result);
    }

    /**
     * ip设置命令
     * @param command 0080
     * @param ipAddress 192.168.60.130
     * @param port 8080
     * @return String.class
     */
    private static String ipSettingRequest(String command,String ipAddress,int port){
        StringBuilder all = getDefaultString();
        all.insert(38,command);
        String[] address = ipAddress.split("\\.");
        StringBuilder ipStringBuilder = new StringBuilder();
        for (String tempAddress : address) {
            ipStringBuilder.append(Integer.toHexString(Integer.parseInt(tempAddress)));
        }
        //IPV4地址
        all.insert(42,ipStringBuilder.toString());
        System.out.println(all);
        String hexPort = Integer.toHexString(port);
        all.insert(50, hexPort);
        int index = 50 + hexPort.length();
        String a=all.substring(6, index);
        List<String> strList = ParseHelper.getStrList(a, 2);
        String crcData=getCrcData(strList);
        all.insert(index, crcData);
        all.replace(2,6,getHeadLength(strList));
        all.insert(50+hexPort.length() + crcData.length(),"fe");
        List<String> result= ParseHelper.getStrList(all.toString(),4);
        return String.join("",result);
    }

    /**
     *
     * @param status status
     * @param type type
     * @param dataPackage 包综述（4字节） 位置44-47
     * @return boolean 是否操作成功
     */
    private static String startOrShutDownHardWardCommand(int status,int type,int dataPackage){
        StringBuilder all = getDefaultString();
        all.insert(38,"0070");

        return "";
    }
}
