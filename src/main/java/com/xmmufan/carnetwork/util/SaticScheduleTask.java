package com.xmmufan.carnetwork.util;

import com.alibaba.fastjson.JSONArray;
import com.xmmufan.carnetwork.constant.hardware.*;
import com.xmmufan.carnetwork.constant.hardware.alarm.Alarm;
import com.xmmufan.carnetwork.entity.mongodb.Electric;
import com.xmmufan.carnetwork.entity.mongodb.Inverter;
import com.xmmufan.carnetwork.entity.mongodb.LivingBattery;
import com.xmmufan.carnetwork.entity.mongodb.Tank;
import com.xmmufan.carnetwork.repository.dao.CarInfoDao;
import com.xmmufan.carnetwork.service.HardWareService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Configuration
@Slf4j
public class SaticScheduleTask {

    private static String[] aeaLimitData = new String[]{"4A","6A","10A","12A","16A","20A","25A","30A","无限制"};

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private CarInfoDao carInfoDao;
    @Autowired
    private HardWareService hardWareService;
    @Autowired
    private MongoTemplate mongoTemplate;



    @Scheduled(cron = "0 * * * * ?")
    @SuppressWarnings("unchecked")
    private void configureTasks() {
        //获取所有车辆
//        List<CarInfo> carInfos = carInfoDao.selectAll();
//        List<Map> alarm = hardWareService.query("ffff00010A","ffff00010B","0050","","12345678912345678");
//        List<Map> systemStatusQueryResult = hardWareService.query("ffff000106","ffff00011B","0050","","12345678912345678");
//        List<Map> xcm16StatusQueryResult = hardWareService.query("003600013C", "003600013E", "0050", "", "12345678912345678");
//        List<Map> inverterStatusQueryResult = hardWareService.query("0003011064", "000301107A", "0050", "", carVin);
//        List<Map> electricStatus = hardWareService.query("010105", "000f010105", "0050", "", "12345678912345678");

//        Tank tankBean = new Tank();
//        LivingBattery livingBattery = new LivingBattery();
//        Inverter inverter = new Inverter();
//        Electric electric = new Electric();

//        if(systemStatusQueryResult!=null){
//            Map<String, List<String>> systemStatusMap = (Map<String, List<String>>) getStreamMap(systemStatusQueryResult).get(0);
//            JSONArray alarmList1 = JSONArray.parseArray(JSONArray.toJSONString(systemStatusMap.get("010A")));
//            JSONArray alarmList2 = JSONArray.parseArray(JSONArray.toJSONString(systemStatusMap.get("010B")));
//            List<String> alarmQueryResult = alarmList1.toJavaList(String.class);
//            alarmQueryResult.addAll(alarmList2.toJavaList(String.class));
//            Map<String,List<Map<String,String>>> alarmResult = new HashMap(16);
//            // 处理报警数据
//            for (int i = 0; i < alarmQueryResult.size(); i += 12) {
//                String data = String.join("", alarmQueryResult.subList(i, i + 12));
//                if("000000000000000000000000".equals(data)){
//                    continue;
//                }
//                log.info("data={}",data);
//                //报警信息的12字节包含1. 设备标识,事件码,时间戳
//                String equipmentSign = data.substring(0, 8);
//                String event = data.substring(8,16);
//                String time = data.substring(16,24);
//                Alarm a = ParseHelper.getAlarmByEquipment(event);
//                long timeLong = Long.parseLong(time,16)*1000;
//                //问题再次
//                String equipmentAddress = equipmentSign.substring(2, 6);
//                log.info("equipmentAddress={}",equipmentAddress);
//                log.info("Alarm={}",a);
//                String message = EquipmentSign.getByName(equipmentAddress.toUpperCase()) + a.getDescription();
//                String equipmentName = ParseHelper.getEquipmentNameByEquipmentAddress(equipmentAddress);
//                Date alarmTime = new Date(timeLong);
//                a.setTime(alarmTime);
//                a.setEquipmentName(equipmentName);
//                HashMap<String,String> alarmMap = new HashMap<>(4);
//                alarmMap.put("time",new SimpleDateFormat("yyy-MM-dd hh:mm:ss").format(timeLong));
//                alarmMap.put("message", message);
//                alarmMap.put("rank", a.getSort());
//                alarmMap.put("type", a.getType());
//                List<Map<String, String>> list = alarmResult.get(equipmentName);
//                if(list == null){
//                    list = new ArrayList<>();
//                }
//                list.add(alarmMap);
//                Criteria criteria = new Criteria();
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(alarmTime);
//                criteria.and("time").is(calendar.getTime()).and("equipmentName").is(equipmentName);
//                Query query = new Query(criteria);
//                List<Alarm> alarms = mongoTemplate.find(query, Alarm.class);
//                if(alarms.isEmpty()){
//                    log.info("save to mongodb");
//                    mongoTemplate.save(a);
//                }
//                alarmResult.put(equipmentName,list);
//            }
//            log.info("alarmResult={}",alarmResult);
//            JSONArray jsonArray;
////            JSONArray jsonArray = JSONArray.parseArray(JSONArray.toJSONString(systemStatusMap.get("000B")));
////            livingBattery.setBatteryType(jsonArray.getString(0));
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(systemStatusMap.get("1115")));
//            List<String> list = jsonArray.toJavaList(String.class);
//            livingBattery.setTime(Integer.parseInt(String.join("",list),16)+"");
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(systemStatusMap.get("0106")));
//            livingBattery.setCapacity(Integer.parseInt(jsonArray.getString(0)+jsonArray.getString(1)));
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(systemStatusMap.get("011A")));
////            list = jsonArray.toJavaList(String.class);
////            electric.setElectricPower(Integer.parseInt(String.join("",list),16)+"");
////            log.info("electric={}",electric);
////        }


//        if(alarm != null){
//            List<String> alarmList = (List<String>) alarm.get(0).get("010A");
//            alarmList.addAll((List<String>) alarm.get(1).get("010B"));
//            Map<String,List<Map<String,String>>> alarmResult = new HashMap(16);
//            for (int i = 0; i < alarmList.size(); i += 12) {
//                String data = String.join("", alarmList.subList(i, i + 12));
//                if("000000000000000000000000".equals(data)){
//                    continue;
//                }
//                //报警信息的12字节包含1. 设备标识,事件码,时间戳
//                String equipmentSign = data.substring(0, 8);
//                String event = data.substring(8,16);
//                String time = data.substring(16,24);
//                Alarm a = ParseHelper.getAlarmByEquipment(event);
//                long timeLong = Long.parseLong(time,16)*1000;
//                String equipmentAddress = equipmentSign.substring(2, 6);
//                String message = EquipmentSign.getByName(equipmentAddress.toUpperCase()) + a.getDescription();
//                String equipmentName = ParseHelper.getEquipmentNameByEquipmentAddress(equipmentAddress);
//                Date alarmTime = new Date(timeLong);
//                a.setTime(alarmTime);
//                a.setEquipmentName(equipmentName);
//                HashMap<String,String> alarmMap = new HashMap<>(4);
//                alarmMap.put("time",new SimpleDateFormat("yyy-MM-dd hh:mm:ss").format(timeLong));
//                alarmMap.put("message", message);
//                alarmMap.put("rank", a.getSort());
//                alarmMap.put("type", a.getType());
//                List<Map<String, String>> list = alarmResult.get(equipmentName);
//                if(list == null){
//                    list = new ArrayList<>();
//                }
//                list.add(alarmMap);
//                Criteria criteria = new Criteria();
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(alarmTime);
//                criteria.and("time").is(calendar.getTime()).and("equipmentName").is(equipmentName);
//                Query query = new Query(criteria);
//                List<Alarm> alarms = mongoTemplate.find(query, Alarm.class);
//                if(alarms.isEmpty()){
//                    log.info("save to mongodb");
//                    mongoTemplate.save(a);
//                }
//                alarmResult.put(equipmentName,list);
//            }
//            log.info("alarmResult={}",alarmResult);
//        }

//        Tank tank = resolveTankData("12345678912345678");
        Map<String, List<Alarm>> alarmList = resolveAlarm("12345678912345678");
        log.info("alarmMap = {} ",alarmList);
        if(alarmList.get("xcm16") != null){

        }
        Inverter inverter = resolverInverterData("12345678912345678");

//        Electric electric = resolveElectricData("12345678912345678");

//        List<Map> query = hardWareService.query("0003011000", "0003011021", "0050",
//                "", "12345678912345678");
//        log.info("query={}",query);

//        if(query != null){
//            List<Object> map = getStreamMap(query);
//            Map<String, String> result = (Map<String, String>) map.get(0);
//            JSONArray aeaLimitArray = JSONArray.parseArray(JSONArray.toJSONString(result.get("1021")));
//            String aeaLimit = aeaLimitArray.getString(0);
//            String binaryString = Integer.toBinaryString(Integer.parseInt(aeaLimit, 16));
//            if(binaryString.length()< 8 || binaryString.startsWith("0")){
//                //aeaLimitData
//                int index = Integer.parseInt(binaryString, 2);
//            }else if(binaryString.startsWith("1")){
//                int limit = Integer.parseInt(binaryString.substring(1), 2);
//                System.out.println(limit * 0.5);
//            }
//            // System.out.println((byte)Integer.parseInt("D0", 16));
//        }

//        if(xcm16StatusQueryResult!=null){
//            List<Object> list = getStreamMap(xcm16StatusQueryResult);
//            Map<String, List<String>> tankMap = (Map<String, List<String>>) list.get(0);
//            JSONArray jsonArray;
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(tankMap.get("013C")));
//            tankBean.setSolutionTank(Integer.parseInt(jsonArray.getString(0),16) + "");
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(tankMap.get("013D")));
//            tankBean.setGreyTank(Integer.parseInt(jsonArray.getString(0),16) + "");
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(tankMap.get("013E")));
//            tankBean.setBlackTank(Integer.parseInt(jsonArray.getString(0),16) + "");
//            tankBean.setTime(new Date());
//            tankBean.setCarVin("1234567891234678");
//            mongoTemplate.save(tankBean);
//            log.info("tankBean={}", tankBean);
//            List<Map> inverterStatusQueryResult = hardWareService.query("0003011064", "000301107A", "0050", "", "12345678912345678");
//            if(inverterStatusQueryResult != null){
//                list = getStreamMap(inverterStatusQueryResult);
//                Map<String,List<String>> invertMap = (Map<String, List<String>>) list.get(0);
//                JSONArray cfWorkState = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("1076")));
//                String inverterStatus = cfWorkState.getString(0);
//                if("01".equals(inverterStatus)){
//                    inverter.setInputVoltage("220");
//                    inverter.setElectricityState(true);
//                }else  {
//                    inverter.setInputVoltage("---");
//                    inverter.setElectricityState(false);
//                }
//                JSONArray carryPower = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("107A")));
//                if("01".equals(inverterStatus) || "02".equals(inverterStatus)){
//                    inverter.setCarryPower(Integer.parseInt(carryPower.getString(0) + carryPower.getString(1))+"");
//                }
//                //switchFaultState通过查询报警来设置
//                //输出电压
//                JSONArray outputVoltageArray = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("1069")));
//                int outputVoltage = Integer.parseInt(outputVoltageArray.getString(0) + outputVoltageArray.getString(1),16);
//                //输出电流
//                JSONArray outputCurrentArray = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("106B")));
//                int outputCurrent = Integer.parseInt(outputCurrentArray.getString(0) + outputCurrentArray.getString(1),16);
//                int outputPower = (outputVoltage / 10) * (outputCurrent / 10);
//                JSONArray intPutCurrentArray = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("1068")));
//                inverter.setOutputPower(outputPower + "");
//                inverter.setInputCurrent(Integer.parseInt(intPutCurrentArray.getString(0) + intPutCurrentArray.getString(1),16) + "");
//                mongoTemplate.save(inverter);
//                log.info("inverter1={}",inverter);
//            }
//        }



        /*
         * 报警信息
         */
//        if(alarm != null){
//            List<String> alarmList = (List<String>) alarm.get(0).get("010A");
//            alarmList.addAll((List<String>) alarm.get(1).get("010B"));
//            Map<String,List<Map<String,String>>> alarmResult = new HashMap(16);
//            for (int i = 0; i < alarmList.size(); i += 12) {
//                String data = String.join("", alarmList.subList(i, i + 12));
//                if("000000000000000000000000".equals(data)){
//                    continue;
//                }
//                //报警信息的12字节包含1. 设备标识,事件码,时间戳
//                String equipmentSign = data.substring(0, 8);
//                String event = data.substring(8,16);
//                String time = data.substring(16,24);
//                Alarm a = ParseHelper.getAlarmByEquipment(event);
//                long timeLong = Long.parseLong(time,16)*1000;
//                String equipmentAddress = equipmentSign.substring(2, 6);
//                String message = EquipmentSign.getByName(equipmentAddress.toUpperCase()) + a.getDescription();
//                String equipmentName = ParseHelper.getEquipmentNameByEquipmentAddress(equipmentAddress);
//                Date alarmTime = new Date(timeLong);
//                a.setTime(alarmTime);
//                a.setEquipmentName(equipmentName);
//                HashMap<String,String> alarmMap = new HashMap<>(4);
//                alarmMap.put("time",new SimpleDateFormat("yyy-MM-dd hh:mm:ss").format(timeLong));
//                alarmMap.put("message", message);
//                alarmMap.put("rank", a.getSort());
//                alarmMap.put("type", a.getType());
//                List<Map<String, String>> list = alarmResult.get(equipmentName);
//                if(list == null){
//                    list = new ArrayList<>();
//                }
//                list.add(alarmMap);
//                Criteria criteria = new Criteria();
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(alarmTime);
////                calendar.add(Calendar.HOUR,-8);
//                criteria.and("time").is(calendar.getTime()).and("equipmentName").is(equipmentName);
//                Query query = new Query(criteria);
//                List<Alarm> alarms = mongoTemplate.find(query, Alarm.class);
//                if(alarms.isEmpty()){
//                    log.info("save to mongodb");
//                    mongoTemplate.save(a);
//                }
//                alarmResult.put(equipmentName,list);
//            }
//            System.out.println(alarmResult);
//        }

        /*
         * 水箱数据
         */
//        if(tank!=null) {
//            List<Object> list = getStreamMap(tank);
//            Map<String, List<String>> tankMap = (Map<String, List<String>>) list.get(0);
//            JSONArray jsonArray;
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(tankMap.get("013C")));
//            tankBean.setSolutionTank(jsonArray.getString(0));
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(tankMap.get("013D")));
//            tankBean.setGreyTank(jsonArray.getString(0));
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(tankMap.get("013E")));
//            tankBean.setBlackTank(jsonArray.getString(0));
//            log.info("tankBean={}", tankBean);
//        }
        /*
        逆变器数据
         */
//        if(inverter!=null){
//            List<Object> list = getStreamMap(inverter);
//            Map<String,List<String>> invertMap = (Map<String, List<String>>) list.get(0);
//            JSONArray jsonArray;
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("1076")));
//            String inverterStatus = jsonArray.getString(0);
//            if("01".equals(inverterStatus)){
//                inverter1.setInputVoltage("220");
//                inverter1.setElectricityState(true);
//            }else  {
//                inverter1.setInputVoltage("---");
//                inverter1.setElectricityState(false);
//            }
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("107A")));
//            if("01".equals(inverterStatus) || "02".equals(inverterStatus)){
//                inverter1.setCarryPower(jsonArray.getString(0));
//            }
//            //switchFaultState通过查询报警来设置
//            log.info("inverter1={}",inverter1);
//        }
        /*
         *电池
         */
//        if(batteryStatus!=null){
//            List<Object> list = getStreamMap(batteryStatus);
//            Map<String,List<String>> batteryMap = (Map<String, List<String>>) list.get(0);
//            JSONArray jsonArray;
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(batteryMap.get("000B")));
//            livingBattery.setBatteryType(jsonArray.getString(0));
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(batteryMap.get("1114")));
//            livingBattery.setTime(jsonArray.getString(0));
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(batteryMap.get("0106")));
//            livingBattery.setCapacity(Integer.parseInt(jsonArray.getString(0)));
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(batteryMap.get("0119")));
//            electric.setElectricPower(jsonArray.getString(0));
//            //switchFaultState通过查询报警来设置
//            log.info("livingBattery={}",livingBattery);
//        }
//        result.put("tank",tankBean);
//        result.put("livingBattery", livingBattery);
//        if(result.isEmpty()){
//            System.out.println(result);
//        }

        /*
         * electric数据
         */
//        if (electricStatus != null) {
//            List<Object> list = getStreamMap(electricStatus);
//            Map<String, List<String>> batteryMap = (Map<String, List<String>>) list.get(0);
//            JSONArray jsonArray;
//            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(batteryMap.get("0105")));
//            String s = jsonArray.getString(0) + jsonArray.getString(1);
//            electric.setElectricAlli((Integer.parseInt(s, 16) * 0.01) + "A");
//            //switchFaultState通过查询报警来设置
//            log.info("electric={}", electric);
//        }

//        if(tank!=null && cfWorkState!=null){
//            tank.forEach(map -> map.forEach((key, value)-> log.info("key={}, value= {}", key, value)));
//            log.info("cfWordState============");
//            cfWorkState.forEach(map -> map.forEach((key,value) -> {
//                log.info("key{}, value ={}",key,value);
//            }));
//        }
//        if(query!=null){
//            query.forEach(map -> map.forEach((key, value) -> log.info("key={}, value = {}",key,value)));
//        }

//        //数据处理
//        try {
//            String data="";
//            Map<String,List<Map<String,String>>> dataMap=new HashMap<>(0);
//            HashMap<String,String> tankMap;
//            HashMap<String,String> basisControlMap;
//            HashMap<String,String> lightControlMap;
//            HashMap<String,String> enviromentMap;
//            HashMap<String,String> systemMap;
//            //获取所有的车辆id和设备标识
//            List<CarInfo> carInfoList=carInfoDao.selectCarInfo();
//            int spiltLength=100;
//
//            //遍历所有的车辆信息，匹配设备标识并找到通道
//            for (CarInfo info:carInfoList){
//                Map<String,Object> objectMap = (Map<String,Object>)redisUtil.get(info.getCarSequenceSign());
//                if(objectMap==null){
//                    continue;
//                }
//                List<Map<String,String>> system=(List<Map<String,String>>)objectMap.get("系统标识");
//                if(system!=null){
//                    List<Map<String,String>> enviromentList=new ArrayList<>();
//                    List<Map<String,String>> systemList=new ArrayList<>();
//                    for (Map<String, String> stringStringMap : system) {
//                        systemMap = getMap(systemSign, stringStringMap);
//                        systemList.add(systemMap);
//                        enviromentMap=getMap(enviromentSign,stringStringMap);
//                        enviromentList.add(enviromentMap);
//                    }
//                    dataMap.put("system",systemList);
//                    dataMap.put("environment",enviromentList);
//                }
//                List<Map<String,String>> alert=(List<Map<String,String>>)objectMap.get("alert");
//                dataMap.put("alert",alert);
//
//                List<Map<String,String>> combi = (List<Map<String,String>>)objectMap.get("COMBI");
//                if(combi!=null){
//                    List<Map<String,String>> combiList=new ArrayList<>();
//                    for (Map<String, String> stringStringMap : combi) {
//                        systemMap = getMap(invertSign, stringStringMap);
//                        combiList.add(systemMap);
//                    }
//                    dataMap.put("inverter",combiList);
//                }
//                List<Map<String,String>> XCM16 = (List<Map<String,String>>)objectMap.get("XCM16");
//                if(XCM16!=null){
//                    List<Map<String,String>> tankList=new ArrayList<>();
//                    List<Map<String,String>> basisControlList=new ArrayList<>();
//                    List<Map<String,String>> lightList=new ArrayList<>();
//                    for(Map<String, String> stringStringMap : XCM16){
//                        tankMap=getMap(tankSign,stringStringMap);
//                        tankList.add(tankMap);
//                        basisControlMap=getMap(basicsControlSign,stringStringMap);
//                        basisControlList.add(basisControlMap);
//                        lightControlMap=getMap(lightControlSign,stringStringMap);
//                        lightList.add(lightControlMap);
//
//                    }
//                    dataMap.put("tank",tankList);
//                    dataMap.put("basicsControl",basisControlList);
//                    dataMap.put("lightControl",lightList);
//                }
//                JSONObject object= JSONObject.fromObject(dataMap);
//                data="fe0020"+object.toString()+"fe";
//                if(objectMap.size()>0){
//                    for (String list:getKeyByCarVin(info.getCarVin())){
//                        Channel channel=UtilMap.getChannelByName(list);
//                        //写入数据到通道channel
//                        if(data.length()>spiltLength){
//                            for (int i=0;i<data.length()/spiltLength+1;i++){
//                                String str="";
//                                if(i==data.length()/spiltLength){
//                                    str=data.substring(i*spiltLength);
//                                }else{
//                                    str=data.substring(i*spiltLength,spiltLength*(i+1));
//                                }
//                                channel.writeAndFlush(new TextWebSocketFrame(str));
//                            }
//                        }
//                        channel.writeAndFlush(new TextWebSocketFrame(data));
//                    }
//                }
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
    }

//    public static void main(String[] args) {
//        int a4 = Integer.parseInt("7", 16);
//        String binaryString = Integer.toBinaryString(a4);
//        System.out.println(binaryString);
//        if(binaryString.length()< 8 || binaryString.startsWith("0")){
//            //aeaLimitData
//            int index = Integer.parseInt(binaryString, 2);
//            System.out.println(index);
//            System.out.println("aeaLimit=" + aeaLimitData[index]);
//        }else if(binaryString.startsWith("1")){
//            int limit = Integer.parseInt(binaryString.substring(1), 2);
//            System.out.println(limit * 0.5);
//        }
//
//    }

    /**
     * 解析查询的水箱数据
     * @param carVin 车辆标识
     * @return Tank.class
     */
    @SuppressWarnings("unchecked")
    private Tank resolveTankData(String carVin){
        List<Map> xcm16StatusQueryResult = hardWareService.query("003600013C", "003600013E", "0050", "", carVin);
        if(xcm16StatusQueryResult!=null){
            Tank tankBean = new Tank();
            List<Object> list = getStreamMap(xcm16StatusQueryResult);
            Map<String, List<String>> tankMap = (Map<String, List<String>>) list.get(0);
            JSONArray jsonArray;
            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(tankMap.get("013C")));
            tankBean.setSolutionTank(Integer.parseInt(jsonArray.getString(0),16) + "");
            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(tankMap.get("013D")));
            tankBean.setGreyTank(Integer.parseInt(jsonArray.getString(0),16) + "");
            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(tankMap.get("013E")));
            tankBean.setBlackTank(Integer.parseInt(jsonArray.getString(0),16) + "");
            tankBean.setTime(new Date());
            tankBean.setCarVin(carVin);
            mongoTemplate.save(tankBean);
            log.info("tankBean={}", tankBean);
            return tankBean;
        }
        return null;
    }
    /**
     * 解析逆变器数据
     * @param carVin carVin
     * @return Inverter.class
     */
    @SuppressWarnings("unchecked")
    private Inverter resolverInverterData(String carVin){
        List<Map> inverterStatusQueryResult = hardWareService.query("0003011064", "000301107A", "0050", "", carVin);
        List<Map> inverterConfigQueryResult = hardWareService.query("0003011000", "0003011021", "0050", "", carVin);
        Inverter inverter = new Inverter();
        if(inverterStatusQueryResult != null){
            List<Object> list = getStreamMap(inverterStatusQueryResult);
            Map<String,List<String>> invertMap = (Map<String, List<String>>) list.get(0);
            JSONArray cfWorkState = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("1076")));
            int inverterStatus = Integer.parseInt(cfWorkState.getString(0),16);
            inverter.setInverterState(Combi_0003.getCombiDescription("1076").get(inverterStatus+""));
            if(inverterStatus == 1){
                inverter.setInputVoltage("220");
                inverter.setElectricityState(true);
            }else  {
                inverter.setInputVoltage("---");
                inverter.setElectricityState(false);
            }
            JSONArray carryPower = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("107A")));
            if(1 == inverterStatus || 2 == inverterStatus){
                inverter.setCarryPower(Integer.parseInt(carryPower.getString(0) + carryPower.getString(1))+"");
            }
            //switchFaultState通过查询报警来设置
            //输出电压
            JSONArray outputVoltageArray = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("1069")));
            int outputVoltage = Integer.parseInt(outputVoltageArray.getString(0) + outputVoltageArray.getString(1),16);
            //输出电流
            JSONArray outputCurrentArray = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("106B")));
            int outputCurrent = Integer.parseInt(outputCurrentArray.getString(0) + outputCurrentArray.getString(1),16);
            int outputPower = (outputVoltage / 10) * (outputCurrent / 10);
            JSONArray intPutCurrentArray = JSONArray.parseArray(JSONArray.toJSONString(invertMap.get("1068")));
            inverter.setOutputPower(outputPower + "");
            inverter.setInputCurrent(Integer.parseInt(intPutCurrentArray.getString(0) + intPutCurrentArray.getString(1),16) + "");
            log.info("inverter1={}",inverter);
        }
        if(inverterConfigQueryResult != null){
            List<Object> map = getStreamMap(inverterConfigQueryResult);
            Map<String, String> result = (Map<String, String>) map.get(0);
            JSONArray aeaLimitArray = JSONArray.parseArray(JSONArray.toJSONString(result.get("1021")));
            String aeaLimit = aeaLimitArray.getString(0);
            String binaryString = Integer.toBinaryString(Integer.parseInt(aeaLimit, 16));
            int limit = 0;
            if(binaryString.length()< 8 || binaryString.startsWith("0")){
                //aeaLimitData
                limit = Integer.parseInt(binaryString, 2);
            }else if(binaryString.startsWith("1")){
                limit = Integer.parseInt(binaryString.substring(1), 2);
                System.out.println();
            }
            inverter.setInputLimitCurrent(limit + "");
            // System.out.println((byte)Integer.parseInt("D0", 16));
        }else {
            inverter.setInputLimitCurrent("123456");
        }
        mongoTemplate.save(inverter);
        return inverter;
    }

    /**
     * 解析发电器数据
     * @param carVin carVin
     * @return Electric.class
     */
    @SuppressWarnings("unchecked")
    public Electric resolveElectricData(String carVin){
        List<Map> ddxStatusQueryResult = hardWareService.query("000f010100", "000f01010C", "0050", "", carVin);
        log.info("ddxStatusQueryResult={}" , ddxStatusQueryResult);
        if(ddxStatusQueryResult != null){
            Electric electric = new Electric();
            List<Object> list = getStreamMap(ddxStatusQueryResult);
            Map<String, List<String>> batteryMap = (Map<String, List<String>>) list.get(0);
            JSONArray jsonArray;
            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(batteryMap.get("0105")));
            String s = jsonArray.getString(0) + jsonArray.getString(1);
            electric.setElectricAlli((Integer.parseInt(s, 16) * 0.01) + "A");
            jsonArray = JSONArray.parseArray(JSONArray.toJSONString(batteryMap.get("0100")));
            s = jsonArray.getString(0) + jsonArray.getString(1);
            electric.setInputVoltage((Integer.parseInt(s, 16) * 0.01));
            //switchFaultState通过查询报警来设置
            log.info("electric={}", electric);
            return electric;
        }
        return null;
    }
    @SuppressWarnings("unchecked")
    public Map<String,List<Alarm>> resolveAlarm(String carVin){
        List<Map> alarm = hardWareService.query("ffff00010A","ffff00010B","0050","",carVin);
        if(alarm != null){
            List<String> alarmList = (List<String>) alarm.get(0).get("010A");
            alarmList.addAll((List<String>) alarm.get(1).get("010B"));
            Map<String,List<Alarm>> alarmResult = new HashMap(16);
            for (int i = 0; i < alarmList.size(); i += 12) {
                String data = String.join("", alarmList.subList(i, i + 12));
                //全零为无报警
                String replace = new String(new char[24]).replace("\0", "0");
                if(replace.equals(data)){
                    continue;
                }

                //报警信息的12字节包含1. 设备标识,事件码,时间戳
                String equipmentSign = data.substring(0, 8);
                String event = data.substring(8,16);
                String time = data.substring(16,24);
                Alarm a = ParseHelper.getAlarmByEquipment(event);
                long timeLong = Long.parseLong(time,16)*1000;
                String equipmentAddress = equipmentSign.substring(2, 6);
                String message = EquipmentSign.getByName(equipmentAddress.toUpperCase()) + a.getDescription();
                String equipmentName = ParseHelper.getEquipmentNameByEquipmentAddress(equipmentAddress);
                Date alarmTime = new Date(timeLong);
                a.setTime(alarmTime);
                a.setEquipmentName(equipmentName);
                List<Alarm> list = alarmResult.get(equipmentName);
                if(list == null){
                    list = new ArrayList<>();
                }
                list.add(a);
                Criteria criteria = new Criteria();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(alarmTime);
                criteria.and("time").is(calendar.getTime()).and("equipmentName").is(equipmentName);
                Query query = new Query(criteria);
                List<Alarm> alarms = mongoTemplate.find(query, Alarm.class);
                if(alarms.isEmpty()){
                    log.info("save to mongodb");
                    mongoTemplate.save(a);
                }
                alarmResult.put(equipmentName,list);
            }
            log.info("alarmResult={}",alarmResult);
            return alarmResult;
        }
        return null;
    }

    private List<Object> getStreamMap(List<Map> list) {
        Map<Object, Object> result = new HashMap<>(list.size());
        return list.stream().map(map1 -> {
            Set set = map1.keySet();
            for (Object o : set) {
                result.put(o, map1.get(o));
                return result;
            }
            return null;
        }).collect(Collectors.toList());
    }

    private List<String> getKeyByCarVin(String carVin) {
        String str;
        List<String> keyList = new ArrayList<>();
        Map<String, String> map = CarMap.getCarMap();
        for (Object object : map.entrySet()) {
            str = object.toString();
            if (carVin.equals(str.substring(str.indexOf("=") + 1, str.length()))) {
                keyList.add(str.substring(0, str.lastIndexOf("=")));
            }
        }
        return keyList;
    }

    public HashMap<String, String> getMap(String[] sign, Map<String, String> map) {
        HashMap<String, String> dataMap = new HashMap<>(0);
        for (String s : sign) {
            dataMap.put(s, map.get(s));
        }
        return dataMap;
    }
}
