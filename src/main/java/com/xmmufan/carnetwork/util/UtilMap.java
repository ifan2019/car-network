package com.xmmufan.carnetwork.util;

import io.netty.channel.Channel;

import java.util.HashMap;
import java.util.Map;

public class UtilMap {
    private static Map<String, Channel> channelMap=new HashMap<>(0);

    public static void addChannel(String key,Channel channel){
        channelMap.put(key,channel);
    }

    public static Channel getChannelByName(String name){
        return channelMap.get(name);
    }

}
