package com.xmmufan.carnetwork.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author  chenli
 * @date    2019-7-28
 *carMap用来存储前端传来的随机数据与carId
 */
public class CarMap {
    private static Map<String,String> carMap=new HashMap<>(0);

    public static void addMap(String random,String carVin){
        carMap.put(random,carVin);
    }

    public static String getCarMap(String carVin){
        return carMap.get(carVin);
    }

    public static Map<String, String> getCarMap() {
        return carMap;
    }
}
