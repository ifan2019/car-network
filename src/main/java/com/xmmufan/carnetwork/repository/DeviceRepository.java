package com.xmmufan.carnetwork.repository;

import com.xmmufan.carnetwork.entity.monitor.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/8/13
 */
@Repository
public interface DeviceRepository extends JpaRepository<Device,Long> {

}
