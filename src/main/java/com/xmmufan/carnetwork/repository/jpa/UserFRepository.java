package com.xmmufan.carnetwork.repository.jpa;

import com.xmmufan.carnetwork.entity.monitor.UserF;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserFRepository extends JpaRepository<UserF,Long> {

    /**
     * 根据username获取F段用户
     * @param username
     * @return
     */
    UserF findUserFByUserName(String username);
}
