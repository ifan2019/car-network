package com.xmmufan.carnetwork.repository.jpa;

import com.xmmufan.carnetwork.entity.monitor.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface CarInfoRepository extends JpaRepository<CarInfo,Long>, JpaSpecificationExecutor<CarInfo> {



    List<CarInfo> findAllByCarStatusAndUserF(String status,UserF userF);

    int countByCarCarTypeAndUserFAndCarStatus(String carType,UserF userF,String carStatus);


    List<CarInfo> getCarInfoByUserBId(Long userBId);

     int countAllByUserFIdAndCarStatus(long userfId,String carStatus);

    List<CarInfo> getCarInfoByCarStatusAndCarVin(String carStatus,String carVin);

    Set<CarInfo> getCarInfoByBrandAndCarCarTypeAndDeviceListIn(String brand, String carType, List<Device> deviceList);


    List<CarInfo> getCarInfoByCarStatusAndLicentsePlate(String carStatus,String license);

    @Query(nativeQuery = true,value = "select *\n" +
            "            from\n" +
            "              car_info\n" +
            "            join\n" +
            "            (\n" +
            "                select\n" +
            "                  GROUP_CONCAT(car_device.device_id) devices,car_info.id\n" +
            "                from car_device\n" +
            "                join car_info\n" +
            "                on car_device.car_info_id = car_info.id\n" +
            "                GROUP BY car_info.id\n" +
            "            ) as temp\n" +
            "            on car_info.id = temp.id\n" +
            "            join car_type_price\n" +
            "            on car_type_price.car_device_ids like concat('%',temp.devices,'%')\n" +
            "            join\n" +
            "            car\n" +
            "            on\n" +
            "            car_info.car_id = car.id\n" +
            "            join\n" +
            "            user_f\n" +
            "            on\n" +
            "            car_info.userf_id = user_f.id\n" +
            "            join\n" +
            "            car_device\n" +
            "            on\n" +
            "            car_device.car_info_id =car_info.id\n" +
            "            join\n" +
            "            store\n" +
            "            on\n" +
            "            car_info.store_id = store.id\n" +
            "            join\n" +
            "            city\n" +
            "            on\n" +
            "            city.city_id = store.city_id\n" +
            "            where\n" +
            "            user_f.id = ?1\n" +
            "            and car_status = 'true'\n" +
            "            and\n" +
            "            licentse_plate\n" +
            "            LIKE concat('%',?2,'%')\n" +
            "            OR car_vin LIKE concat('%',?2,'%')\n" +
            "            OR car_type_price.type_name LIKE concat('%',?2,'%')\n" +
            "            OR chassis_info LIKE concat('%',?2,'%')\n" +
            "            OR car_status LIKE concat('%',?2,'%')")
    List<CarInfo> selectByCondition(Long id, String message);
}
