package com.xmmufan.carnetwork.repository.jpa;

import com.xmmufan.carnetwork.entity.monitor.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car,Long> {

    /**
     * 根据车辆类型查询车辆数量
     * @param carType 车辆类型
     * @return int
     */
    int countByCarType(String carType);
}
