package com.xmmufan.carnetwork.repository.jpa;

import com.xmmufan.carnetwork.entity.monitor.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mr.ifan/詹奕凡
 * @date 2019/6/13
 * @version 1.0
 */
@Repository
public interface RentRepository extends JpaRepository<Order,Integer> {


    Order getRentByUserCUserTel(String tel);

    List<Order> findAllByCarInfoUserFIdAndCarInfoLeaseStatusAndOrderStatusIn(Long id,String carStatus,List<String> orderStatus);
}
