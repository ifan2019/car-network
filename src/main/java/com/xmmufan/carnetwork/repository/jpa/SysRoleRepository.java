package com.xmmufan.carnetwork.repository.jpa;



import java.util.List;


/**
 * @author Mr.ifan | 詹奕凡
 * @version 1.0
 * @date 2019/5/19
 * <p>
 *     SysRole 对应的Jpa操作仓库
 * </p>
 */
//@Repository
//public interface SysRoleRepository extends JpaRepository<SysRole,Integer> {
//    List<SysRole> findByRole(String role);
//}
