package com.xmmufan.carnetwork.repository.jpa;

import com.xmmufan.carnetwork.entity.monitor.UserB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mr.ifan/詹奕凡
 * @date 2019/6/13
 * @version 1.0
 */
@Repository
public interface UserBRepository extends JpaRepository<UserB,Long> {

    /**
     * 根据username获取UserB
     * @param username userB username
     * @return UserB
     */
    UserB findUserBByUserName(String username);

    List<UserB> getUserBByState(int state);

    UserB findUserBByBrand(String brand);

}
