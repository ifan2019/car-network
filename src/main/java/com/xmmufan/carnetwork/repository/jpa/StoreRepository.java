package com.xmmufan.carnetwork.repository.jpa;

import com.xmmufan.carnetwork.entity.monitor.City;
import com.xmmufan.carnetwork.entity.monitor.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepository extends JpaRepository<Store,Long> {
    Store getStoreByCityCityIdAndUserFId(long cityId,Long userFId);
}
