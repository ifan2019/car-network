package com.xmmufan.carnetwork.repository.jpa;

import com.xmmufan.carnetwork.entity.monitor.CarTypePrice;
import com.xmmufan.carnetwork.entity.monitor.City;
import com.xmmufan.carnetwork.entity.monitor.UserF;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarTypePriceRepository extends JpaRepository<CarTypePrice, Long> {

    CarTypePrice getCarTypePriceByCarDeviceIdsAndCarTypeIdAndBrand(String carDeviceId, Long carTypeId,String brand);
    List<CarTypePrice> getAllByUserF(UserF userF);
    CarTypePrice getCarTypePriceByCarDeviceIdsAndCarTypeId(String carDeviceId,Long carTypeId);
    List<CarTypePrice> getAllByUserFAndCity(UserF user, City city);
}
