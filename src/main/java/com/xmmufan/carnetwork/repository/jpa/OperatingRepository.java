package com.xmmufan.carnetwork.repository.jpa;

import com.xmmufan.carnetwork.entity.monitor.Operating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author ifan
 * @version 1.0
 * @date 2019/8/12
 */
@Repository
public interface OperatingRepository extends JpaRepository<Operating,Integer> {

}
