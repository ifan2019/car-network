package com.xmmufan.carnetwork.repository.jpa;

import com.xmmufan.carnetwork.entity.monitor.CarDevice;
import com.xmmufan.carnetwork.entity.monitor.CarInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarDeviceRepository extends JpaRepository<CarDevice,Long> {

    CarDevice getCarDeviceByDeviceDeviceIdAndCarInfoId(Long deviceId,Long carInfoId);

    Integer deleteByCarInfo(CarInfo carInfo);
}
