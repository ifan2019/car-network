package com.xmmufan.carnetwork.repository.jpa;

import com.xmmufan.carnetwork.entity.monitor.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * carAdmin repository
 * @author ifan
 * @version 1.0
 * @date 2019/8/13
 */
@Repository
public interface AdminRepository extends JpaRepository<Admin,Long> {

    /**
     * 根据用户名唯一查询出车辆管理员
     * @param userName
     * @return
     */
    List<Admin> findByUserName(String userName);

}
