package com.xmmufan.carnetwork.repository.dao;

import com.xmmufan.carnetwork.commons.BaseDao;
import com.xmmufan.carnetwork.entity.monitor.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface RentDao extends BaseDao<Order> {


    List<Order> getRentInfoList();

    List<Order> selectByCondition(@Param("id") Long id, @Param("message") String message);
}
