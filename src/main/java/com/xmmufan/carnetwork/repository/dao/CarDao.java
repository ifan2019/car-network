package com.xmmufan.carnetwork.repository.dao;

import com.xmmufan.carnetwork.commons.BaseDao;
import com.xmmufan.carnetwork.entity.monitor.Car;

/**
 * @author  陈丽
 * @date    2019-5-21
 */
public interface CarDao extends BaseDao<Car> {
    int getCountByCarType(String type);

}
