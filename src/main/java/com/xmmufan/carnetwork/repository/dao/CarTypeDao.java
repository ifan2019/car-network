package com.xmmufan.carnetwork.repository.dao;

import com.xmmufan.carnetwork.commons.BaseDao;
import com.xmmufan.carnetwork.entity.monitor.CarType;

public interface CarTypeDao extends BaseDao<CarType> {
}
