package com.xmmufan.carnetwork.repository.dao;

import com.xmmufan.carnetwork.commons.BaseDao;
import org.apache.ibatis.annotations.Mapper;

/**
 * Mybatis SysPermission 表的dao层
 * @date 2019/5/19
 * @version 1.0
 * @author Mr ifan/詹奕凡
 */
//@Mapper
//public interface SysPermissionDao extends BaseDao<SysPermission> {
//}
