package com.xmmufan.carnetwork.repository.dao;


import com.xmmufan.carnetwork.commons.BaseDao;
import com.xmmufan.carnetwork.entity.monitor.CarInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * @author 陈丽
 * @version 1.0f
 * @date 2019/5/23
 */
public interface CarInfoDao extends BaseDao<CarInfo> {
    List<CarInfo> getCarInfoList();

    int insertCarInfo(CarInfo carInfo);

    CarInfo selectCarInfoById(Long id);

    CarInfo getCarInfoByVin(String vin);

    List<CarInfo> selectAllByVin(String carVin);


    List<CarInfo> selectByCondition(@Param("id") Long id,@Param("message") String message);

    List<CarInfo> selectWeb_bCarInfoByCondition(@Param("id")int id,@Param("message") String message);

    List<CarInfo> selectByUserbId(Long id);

    List<Map<String, String>> getCountByChassis(Long userfId);

    List<Map<String, Object>> getCountByLeaseStatus(Long userfId);

    List<CarInfo> selectCarInfo();

    String getCarSequenceByCarVin(String carVin);
}
