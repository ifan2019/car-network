package com.xmmufan.carnetwork.repository.dao;

import com.xmmufan.carnetwork.commons.BaseDao;
import org.apache.ibatis.annotations.Mapper;

/**
 * SySRole 对应的dao层
 * @date 2019/5/19
 * @version 1.0
 * @author Mr ifan/詹奕凡
 */
//@Mapper
//public interface SysRoleDao extends BaseDao<SysRole> {
//}
