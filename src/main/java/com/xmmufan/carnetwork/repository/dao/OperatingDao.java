package com.xmmufan.carnetwork.repository.dao;


import com.xmmufan.carnetwork.commons.BaseDao;
import com.xmmufan.carnetwork.entity.monitor.Operating;

import java.util.List;

/**
 * @author   陈丽
 * @date     2019-5-25
 */
public interface OperatingDao extends BaseDao<Operating> {
    List<Operating> selectOperatingByCarId(int carId);
}
